<?php

return [
    'global' => [
        'site_title'     => '网站标题',
        'title_singular' => '全球的',
        'name'           => '名字 / 关键词',
        'value'          => '值',
        'values' =>[
            'site_title' => '你好世界'
        ]
    ],
    'condition' => [
        'title' => '条件',
        'packages' => '配套名称',
        'min_deposit_amount' => '最低存款金额',
        'max_refundable_amount' => '最大可退还金额',
        'refund_period' => '退款期限',
        'target_downline' => '目标下线（人）',
        'refund_target' => '最低退款目标（下线人员）',
    ],
    'condition_values' => [
        'dealer_commission_level' => '经销商小组显示佣金高达 ',
        'franchiser_commission_level' => '特许经营小组显示佣金高达 ',
        'maximum_franchise_group' => '最大分行组',
        'maximum_franchiser_downline' => '最大经营商的下线',
        'trainner_student_kpi' => '实习生的关键绩效指标',
    ],
    'general' => [
        'title' => '通用'
    ],
    'api' => [
        'title' => '接口',
    ],
    'clear_cache' => '清除所有缓存',
    'clear_route_cache' => '刷新路径缓存',
    'clear_config_cache' => '刷新配置缓存',
    'clear_view_cache' => '刷新视图缓存',
    'clear_cache_cache' => '刷新 JavaScript/CSS 缓存',
];