<?php

return [
    'global' => [
        'site_title'     => 'Tajuk Tapak',
        'title_singular' => 'Global',
        'name'           => 'Nama / Kata Kunci',
        'value'          => 'Nilai',
        'values' =>[
            'site_title' => 'Hai Dunia'
        ]
    ],
    'condition' => [
        'title'                 => 'Keadaan',
        'packages'              => 'Nama Pakej',
        'min_deposit_amount'    => 'Jumlah Deposit Minimum',
        'max_refundable_amount' => 'Amaun Maksimum Boleh Dipulangkan',
        'refund_period'         => 'Tempoh bayaran balik',
        'target_downline'       => 'Sasaran Downline (Orang)',
        'refund_target'         => 'Sasaran Bayaran Balik Minimum (Orang Downline)',
    ],
    'condition_values' => [
        'dealer_commission_level'       => 'Panel Peniaga menunjukkan komisen sehingga ',
        'franchiser_commission_level'   => 'Panel Francais menunjukkan komisen sehingga ',
        'maximum_franchise_group'       => 'Kumpulan Francais Maksimum',
        'maximum_franchiser_downline'   => 'Downline Francais Maksimum',
        'trainner_student_kpi'          => 'KPI Pelajar Pelatih ',
    ],
    'general' => [
        'title' => 'Umum'
    ],
    'api' => [
        'title' => 'Api',
    ],
    'clear_cache'           => 'Kosongkan Semua Cache',
    'clear_route_cache'     => 'Segarkan Semula Cache Laluan',
    'clear_config_cache'    => 'Segarkan Semula Cache Konfigurasi',
    'clear_view_cache'      => 'Segarkan Semula Cache Pandangan',
    'clear_cache_cache'     => 'Segarkan Semula Cache JavaScript/CSS',
];