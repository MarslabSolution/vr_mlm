<?php

return [
    'global' => [
        'site_title'     => 'Site Title',
        'title_singular' => 'Global',
        'name'           => 'Name / Key',
        'value'          => 'Value',
        'values' =>[
            'site_title' => 'Hello World'
        ]
    ],
    'condition' => [
        'title' => 'Condition',
        'packages' => 'Packages Name',
        'min_deposit_amount' => 'Minimum Deposit Amount',
        'max_refundable_amount' => 'Maximum Refundable Amount',
        'refund_period' => 'Refund period',
        'target_downline' => 'Target Downline (Persons)',
        'refund_target' => 'Minimum Refund Target (Downline Persons)',
    ],
    'condition_values' => [
        'dealer_commission_level' => 'Dealer Panel show commission up to ',
        'franchiser_commission_level' => 'Frachise Panel show commission up to ',
        'maximum_franchiser_group' => 'Maximum Franchise Group',
        'maximum_franchiser_downline' => 'Maximum Franchise Downline',
        'trainner_student_kpi' => 'Trainer Student KPI ',
    ],
    'general' => [
        'title' => 'General'
    ],
    'api' => [
        'title' => 'Api',
    ],
    'clear_cache' => 'Clear All Cache',
    'clear_route_cache' => 'Flush Route Cache',
    'clear_config_cache' => 'Flush Config Cache',
    'clear_view_cache' => 'Flush View Cache',
    'clear_cache_cache' => 'Flush JavaScript/CSS Cache',
];