@extends('layouts.admin')
@include('layouts.styles')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card mt-4">
                <div class="card-header font-semibold text-lg">{{ trans('global.general_report')}}</div>
                <div class="card-body">
                    @if(session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif  
                    <div class="intro-y report-box mt-12 sm:mt-4 sm:mb-1 xs:mt-4 xs:mb-1">
                        <div class="box py-0 xl:py-5 grid grid-cols-12 gap-0 divide-y xl:divide-y-0 divide-x divide-dashed divide-slate-200 dark:divide-white/5">
                            <div class="report-box__item py-5 xl:py-0 px-5 col-span-12 sm:col-span-6 xl:col-span-3">
                                <div class="report-box__content">
                                    <div class="flex">
                                        <div class="report-box__item__icon text-primary bg-pending/20 border border-pending/20 flex items-center justify-center rounded-full">
                                            <span class="material-symbols-outlined">emoji_people</span>
                                        </div>
                                    </div>
                                    <a href="{{ route("admin.agents.index", ['type' => 'dealer']) }}" class="text-decoration-none">
                                        <div class="text-lg font-medium leading-7 mt-6">{{trans('global.total_dealer')}}</div>
                                        <div class="text-slate-500 mt-1" id="total_dealers">{{ $total_dealers }}</div>
                                    </a>
                                </div>
                            </div>
                            <div class="report-box__item py-5 xl:py-0 px-5 sm:!border-t-0 col-span-12 sm:col-span-6 xl:col-span-3">
                                <div class="report-box__content">
                                    <div class="flex">
                                        <div class="report-box__item__icon text-pending bg-pending/20 border border-pending/20 flex items-center justify-center rounded-full">
                                            <span class="material-symbols-outlined">escalator_warning</span>
                                        </div>
                                    </div>
                                    <a href="{{ route("admin.agents.index", ['type' => 'franchiser']) }}" class="text-decoration-none">
                                        <div class="text-lg font-medium leading-7 mt-6">{{trans('global.total_franchiser')}}</div>
                                        <div class="text-slate-500 mt-1" id="total_franchiser">{{ $total_franchiser }}</div>
                                    </a>
                                </div>
                            </div>
                            <div class="report-box__item py-5 xl:py-0 px-5 col-span-12 sm:col-span-6 xl:col-span-3">
                                <div class="report-box__content">
                                    <div class="flex">
                                        <div class="report-box__item__icon text-success bg-pending/20 border border-pending/20 flex items-center justify-center rounded-full">
                                            <i class="fa fa-rocket "></i>
                                        </div>
                                    </div>
                                    <a href="{{ route("admin.agent-plans.index") }}" class="text-decoration-none">
                                        <div class="text-lg font-medium leading-7 mt-6">{{trans('global.total_dealer_plan')}}</div>
                                        <div class="text-slate-500 mt-1" id="total_dealer_price">{{ $total_dealer_price }}</div>
                                    </a>
                                </div>
                            </div>
                            <div class="report-box__item py-5 xl:py-0 px-5 col-span-12 sm:col-span-6 xl:col-span-3">
                                <div class="report-box__content">
                                    <div class="flex">
                                        <div class="report-box__item__icon text-warning bg-pending/20 border border-pending/20 flex items-center justify-center rounded-full">
                                            <i class="fa fa-paper-plane"></i>
                                        </div>
                                    </div>
                                    <a href="{{ route("admin.agent-plans.index") }}" class="text-decoration-none">
                                        <div class="text-lg font-medium leading-7 mt-6">{{trans('global.total_franchiser_plan')}}</div>
                                        <div class="text-slate-500 mt-1" id="total_franchiser_price">{{ $total_franchiser_price }}</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@parent
<script>
var speed = 50;

/* Call this function with a string containing the ID name to
 * the element containing the number you want to do a count animation on.*/
function incEltNbr(id, num) {
  elt = document.getElementById(id);
  endNbr = Number(document.getElementById(id).innerHTML);
  console.log(num);
  incNbrRec(0, num, endNbr, elt);
}

/*A recursive function to increase the number.*/
function incNbrRec(i, num, endNbr, elt) {
    i+= num ?? 1;
    if (i <= endNbr) {
        elt.innerHTML = i;
        setTimeout(function() {
            incNbrRec(i, num, endNbr, elt);
        }, speed);
    } else if(i > endNbr) {
        elt.innerHTML = endNbr;
    }
}

incEltNbr("total_dealers");
incEltNbr("total_franchiser"); 
incEltNbr("total_franchiser_price", 150); 
incEltNbr("total_dealer_price", 10);
</script>
@endsection
