@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('global.create') }} {{ trans('cruds.agentPlan.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.agent-plans.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                <div class="invalid-feedback">
                    {{ $errors->first('name') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.fields.name_helper') }}</span>
            </div>
            <div class="form-group row">
                <div class="col">
                    <label class="required" for="price">{{ trans('cruds.agentPlan.fields.price') }}</label>
                    <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price" value="{{ old('price', '0.00') }}" step="0.01" required>
                    @if($errors->has('price'))
                    <div class="invalid-feedback">
                        {{ $errors->first('price') }}
                    </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.agentPlan.fields.price_helper') }}</span>
                </div>
                <div class="col-auto">
                    <label class="row justify-content-center">{{ trans('cruds.agentPlan.fields.refundable') }}</label>
                    <div class="custom-control custom-switch custom-switch-lg" data-toggle="toggle">
                        <input type="checkbox" class="custom-control-input" id="refundable" name="refundable" type="checkbox" data-toggle="toggle" value="{{ old('refundable', 0) }}" {{ old('refundable', 0) === 1 ? 'checked' : '' }}>
                        <label class="custom-control-label" for="refundable"></label>
                    </div>
                    @if($errors->has('refundable'))
                        <div class="invalid-feedback">
                            {{ $errors->first('refundable') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.agentPlan.fields.refundable_helper') }}</span>
                </div>
            </div>
            <div class="card-body refundable-form" id="refundable-form" style="display:none">
                <div class="form-group">
                    <label class="required" for="max_refundable_amount[]">{{ trans('settings.condition.max_refundable_amount') }} {{ trans('global.rm') }}</label>
                    <input class="form-control {{ $errors->has('max_refundable_amount[]') ? 'is-invalid' : '' }}" type="number" name="max_refundable_amount[]" id="max_refundable_amount[]" value="{{ old('max_refundable_amount_') }}">
                </div>
                <div class="form-group">
                    <label class="required" for="package_refund_period[]">{{ trans('settings.condition.refund_period') }} {{ trans('global.months') }}</label>
                    <input class="form-control {{ $errors->has('package_refund_period[]') ? 'is-invalid' : '' }}" min="1" max="99" type="number" name="package_refund_period[]" id="package_refund_period[]" value="{{ old('package_refund_period[]') }}">
                </div>
                <div class="form-group">
                    <label class="required" for="package_refund_target[]">{{ trans('settings.condition.target_downline') }}</label>
                    <input class="form-control {{ $errors->has('package_refund_target[]') ? 'is-invalid' : '' }}" min="0" max="99" type="number" name="package_refund_target[]" id="package_refund_target[]" value="{{ old('package_refund_target[]') }}">
                </div>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.agentPlan.fields.description') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{!! old('description') !!}</textarea>
                @if($errors->has('description'))
                <div class="invalid-feedback">
                    {{ $errors->first('description') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.agentPlan.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="roles_id">{{ trans('cruds.agentPlan.fields.roles') }}</label>
                <select class="form-control select2 {{ $errors->has('roles') ? 'is-invalid' : '' }}" name="roles_id" id="roles_id">
                    @foreach($roles as $id => $entry)
                    <option value="{{ $id }}" {{ $id == '' ? 'disabled' : '' }} {{ old('roles_id') == $id ? 'selected' : '' }}>{{ $entry }}</option>
                    @endforeach
                </select>
                @if($errors->has('roles'))
                <div class="invalid-feedback">
                    {{ $errors->first('roles') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.agentPlan.fields.roles_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="commissionable_level">{{ trans('cruds.agentPlan.fields.commissionable_level') }}</label>
                <input class="form-control {{ $errors->has('commissionable_level') ? 'is-invalid' : '' }}" type="number" name="commissionable_level" id="commissionable_level" value="{{ old('commissionable_level', '2') }}" step="1" required>
                @if($errors->has('commissionable_level'))
                <div class="invalid-feedback">
                    {{ $errors->first('commissionable_level') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.agentPlan.fields.commissionable_level_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        function SimpleUploadAdapter(editor) {
            editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
                return {
                    upload: function() {
                        return loader.file
                            .then(function(file) {
                                return new Promise(function(resolve, reject) {
                                    // Init request
                                    var xhr = new XMLHttpRequest();
                                    xhr.open('POST', '{{ route("admin.agent-plans.storeCKEditorImages") }}', true);
                                    xhr.setRequestHeader('x-csrf-token', window._token);
                                    xhr.setRequestHeader('Accept', 'application/json');
                                    xhr.responseType = 'json';

                                    // Init listeners
                                    var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                                    xhr.addEventListener('error', function() {
                                        reject(genericErrorText)
                                    });
                                    xhr.addEventListener('abort', function() {
                                        reject()
                                    });
                                    xhr.addEventListener('load', function() {
                                        var response = xhr.response;

                                        if (!response || xhr.status !== 201) {
                                            return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                                        }

                                        $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                                        resolve({
                                            default: response.url
                                        });
                                    });

                                    if (xhr.upload) {
                                        xhr.upload.addEventListener('progress', function(e) {
                                            if (e.lengthComputable) {
                                                loader.uploadTotal = e.total;
                                                loader.uploaded = e.loaded;
                                            }
                                        });
                                    }

                                    // Send request
                                    var data = new FormData();
                                    data.append('upload', file);
                                    data.append('crud_id', '{{ $agentPlan->id ?? 0 }}');
                                    xhr.send(data);
                                });
                            })
                    }
                };
            }
        }

        var allEditors = document.querySelectorAll('.ckeditor');
        for (var i = 0; i < allEditors.length; ++i) {
            ClassicEditor.create(
                allEditors[i], {
                    extraPlugins: [SimpleUploadAdapter]
                }
            );
        }

        $('#refundable').click(function () {
            $('#refundable-form').toggle($('#refundable').is(':checked'));
            $('#refundable-form :input').prop('required',$('#refundable').is(':checked'));
        });
    });

</script>

@endsection
