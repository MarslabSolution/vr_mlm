@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('global.edit') }} {{ trans('cruds.franchiser.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.agents.update", ['type'=> 'dealer', $user->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="role">{{ trans('cruds.franchiser.fields.roles') }}</label>
                <input class="form-control" type="text" name="role" id="role" value="{{ trans('cruds.franchiser.title_singular') }}" readonly>
                <span class="help-block">{{ trans('cruds.franchiser.fields.roles_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="upline">{{ trans('cruds.franchiser.fields.upline') }}</label>
                <input class="form-control" type="text" name="upline" id="upline" value="{{ $agent_upline ? $agent_upline->user_id . ' - ' . $agent_upline->user->name : trans('cruds.franchiser.fields.no_upline') }}" readonly>
                <span class="help-block">{{ trans('cruds.franchiser.fields.upline_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $user->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="email">{{ trans('cruds.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" value="{{ old('email', $user->email) }}" required>
                @if($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('approved') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="approved" value="0">
                    <input class="form-check-input" type="checkbox" name="approved" id="approved" value="1" {{  old('approved', $user->approved) === 0 ? 'checked' : '' }}>
                    <label class="form-check-label" for="approved">{{ trans('cruds.franchiser.fields.account_disabled') }}</label>
                </div>
                @if($errors->has('approved'))
                    <div class="invalid-feedback">
                        {{ $errors->first('approved') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.franchiser.fields.approved_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection