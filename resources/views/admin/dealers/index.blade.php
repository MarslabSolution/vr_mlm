@extends('layouts.admin')
@section('content')
@can('user_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success add_condition" href="{{ route('admin.agents.create', ['type'=> 'dealer']) }}">
                {{ trans('global.add') }} {{ trans('cruds.dealer.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('cruds.dealer.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Dealer">
            <thead>
                <tr>
                    <th width="10"></th>
                    <th width="15">
                        {{ trans('cruds.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.fields.name') }}
                    </th>
                    <th>
                        {{ trans('cruds.fields.email') }}
                    </th>
                    <th>
                        {{ trans('cruds.dealer.fields.total_referral_student') }}
                    </th>
                    <th>
                        {{ trans('cruds.dealer.fields.account_disabled') }}
                    </th>
                    {{-- <th>
                        {{ trans('cruds.dealer.fields.email_verified_at') }}
                    </th>
                    <th>
                        {{ trans('cruds.dealer.fields.approved') }}
                    </th>
                    <th>
                        {{ trans('cruds.dealer.fields.verified') }}
                    </th>
                    <th>
                        {{ trans('cruds.dealer.fields.two_factor') }}
                    </th> --}}
                    <th>
                        &nbsp;
                    </th>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td>
                        <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                    </td>
                    <td></td>
                    <td>
                        <select class="search">
                            <option value>{{ trans('global.all') }}</option>
                            <option value="0">{{ trans('global.enabled') }}</option>
                            <option value="1">{{ trans('global.disabled') }}</option>
                        </select>
                    </td>
                    <td></td>
                    {{-- <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td> --}}
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.agents.index', ['type'=> 'dealer']) }}",
    columns: [
{ data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'name', name: 'name' },
{ data: 'email', name: 'email' },
{ data: 'total_referral_student', name: 'total_referral_student' },
{ data: 'users.approved', name: 'users.approved' },
//{ data: 'email_verified_at', name: 'email_verified_at' },
//{ data: 'approved', name: 'approved' },
//{ data: 'verified', name: 'verified' },
//{ data: 'two_factor', name: 'two_factor' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  let table = $('.datatable-Dealer').DataTable(dtOverrideGlobals);
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
let visibleColumnsIndexes = null;
$('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value

      let index = $(this).parent().index()
      if (visibleColumnsIndexes !== null) {
        index = visibleColumnsIndexes[index]
      }

      table
        .column(index)
        .search(value, strict)
        .draw()
  });
table.on('column-visibility.dt', function(e, settings, column, state) {
      visibleColumnsIndexes = []
      table.columns(":visible").every(function(colIdx) {
          visibleColumnsIndexes.push(colIdx);
      });
  })
});

</script>
@endsection
