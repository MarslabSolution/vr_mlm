@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('global.create') }} {{ trans('cruds.dealer.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.agents.store", ['type'=> 'dealer']) }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="role">{{ trans('cruds.dealer.fields.roles') }}</label>
                <input class="form-control" type="text" name="role" id="role" value="{{ trans('cruds.dealer.title_singular') }}" readonly>
                <span class="help-block">{{ trans('cruds.dealer.fields.roles_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="upline">{{ trans('cruds.dealer.fields.upline') }}</label>
                <select class="form-control select2 {{ $errors->has('upline') ? 'is-invalid' : '' }}" name="upline" id="upline" required>
                    @foreach($uplines as $id => $level)
                        <option value="{{ $id }}" {{ $id == '' ? 'disabled' : '' }}>{{ $level }}</option>
                    @endforeach
                </select>
                @if($errors->has('upline'))
                    <div class="invalid-feedback">
                        {{ $errors->first('upline') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.dealer.fields.upline_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="email">{{ trans('cruds.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" value="{{ old('email') }}" required>
                @if($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('approved') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="approved" value="0">
                    <input class="form-check-input" type="checkbox" name="approved" id="approved" value="1" {{ old('approved', 1) == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="approved">{{ trans('cruds.dealer.fields.approved') }}</label>
                </div>
                @if($errors->has('approved'))
                    <div class="invalid-feedback">
                        {{ $errors->first('approved') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.dealer.fields.approved_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="password">{{ trans('cruds.dealer.fields.password') }}</label>
                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" id="password" required>
                @if($errors->has('password'))
                    <div class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.dealer.fields.password_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection