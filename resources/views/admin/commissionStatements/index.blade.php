@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        <p class="font-semibold text-lg mt-2 mb-3">{{ trans('cruds.commissionStatement.title_singular') }} {{ trans('global.list') }}</p>
        <div class="row">
            <div class="col-auto align-self-center">@include('partials.datatablesFilter.createdAt')</div>
            <!-- <div class="col-auto align-self-center">@include('partials.datatablesFilter.minTotal')</div> -->
            <div class="col-auto">
                <button class="btn btn-secondary" id="datatable-filter" onclick="filterData()">
                    {{ trans('global.filter') }}
                </button>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-CommissionStatement">
            <thead>
                <tr>
                    <th width="10"></th>
                    <th>
                        {{ trans('cruds.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.commissionStatement.fields.agent') }}
                    </th>
                    <th>
                        {{ trans('cruds.fields.email') }}
                    </th>
                    <th>
                        {{ trans('cruds.commissionStatement.fields.total') }}
                    </th>
                    <th>
                        {{ trans('cruds.commissionStatement.fields.statement_month') }}
                    </th>
                    {{-- <th>{{ trans('cruds.commissionStatement.fields.commission_group') }}</th> --}}
                    <th>
                        &nbsp;
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    var filterData = function() {}
    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);
        let dtOverrideGlobals = {
        buttons: dtButtons,
        processing: true,
        serverSide: true,
        retrieve: true,
        aaSorting: [],
        ajax: "{{ route('admin.commission-statements.index', ['type' => $type]) }}",
        columns: [
            { data: 'placeholder', name: 'placeholder' },
            { data: 'id', name: 'id' },
            { data: 'user.name', name: 'user.name' },
            { data: 'user.email', name: 'user.email' },
            { data: 'total', name: 'total' },
            { data: 'created_at', name: 'created_at', visible : $('#filter-date').val() ? true : false },
            //{ data: 'commission_group', name: 'commission_groups.type' },
            { data: 'actions', name: '{{ trans('global.actions') }}' }
        ],
        orderCellsTop: true,
        order: [[ 1, 'desc' ]],
        pageLength: 100,
    };
    let table = $('.datatable-CommissionStatement').DataTable(dtOverrideGlobals);
    $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
    filterData = function() {
        table.column(5).search($('#filter-date').val())
        /*table.column(4).search.push(function (settings, data, dataIndex) {
            var min = $('#min-total').val();
            var total = parseFloat(data[4]) || 0;
            if (min <= age || min == '') {
                return true;
            }
            return false;
        });*/
        table.draw();
    }
});
</script>
@endsection