@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('cruds.agentStudent.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-AgentStudent">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.agentStudent.fields.tuition_package_efk') }}
                    </th>
                    <th>
                        {{ trans('cruds.agentStudent.fields.student_id') }}
                    </th>
                    <th>
                        {{ trans('cruds.agentStudent.fields.referral_id') }}
                    </th>
                    <th>
                        {{ trans('cruds.agentStudent.fields.trainer_id') }}
                    </th>
                    <th>
                        &nbsp;
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
  
  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.agent-students.index') }}",
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'tuition_package_efk', name: 'tuition_package_efk' },
{ data: 'student_id', name: 'student_id' },
{ data: 'referral_id', name: 'referral_id' },
{ data: 'trainer_id', name: 'trainer_id' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  let table = $('.datatable-AgentStudent').DataTable(dtOverrideGlobals);
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
});

</script>
@endsection