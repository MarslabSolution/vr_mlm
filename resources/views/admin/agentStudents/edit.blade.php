@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('global.edit') }} {{ trans('cruds.agentStudent.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.agent-students.update", [$agentStudent->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="tuition_package_efk">{{ trans('cruds.agentStudent.fields.tuition_package_efk') }}</label>
                <input class="form-control {{ $errors->has('tuition_package_efk') ? 'is-invalid' : '' }}" type="number" name="tuition_package_efk" id="tuition_package_efk" value="{{ old('tuition_package_efk', $agentStudent->tuition_package_efk) }}" step="1" required>
                @if($errors->has('tuition_package_efk'))
                    <div class="invalid-feedback">
                        {{ $errors->first('tuition_package_efk') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.agentStudent.fields.tuition_package_efk_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="student_id">{{ trans('cruds.agentStudent.fields.student_id') }}</label>
                <input class="form-control {{ $errors->has('student_id') ? 'is-invalid' : '' }}" type="number" name="student_id" id="student_id" value="{{ old('student_id', $agentStudent->student_id) }}" step="1" required>
                @if($errors->has('student_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('student_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.agentStudent.fields.student_id_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="referral_id">{{ trans('cruds.agentStudent.fields.referral_id') }}</label>
                <input class="form-control {{ $errors->has('referral_id') ? 'is-invalid' : '' }}" type="number" name="referral_id" id="referral_id" value="{{ old('referral_id', $agentStudent->referral_id) }}" step="1" required>
                @if($errors->has('referral_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('referral_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.agentStudent.fields.referral_id_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="trainer_id">{{ trans('cruds.agentStudent.fields.trainer_id') }}</label>
                <input class="form-control {{ $errors->has('trainer_id') ? 'is-invalid' : '' }}" type="number" name="trainer_id" id="trainer_id" value="{{ old('trainer_id', $agentStudent->trainer_id) }}" step="1" required>
                @if($errors->has('trainer_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('trainer_id') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.agentStudent.fields.trainer_id_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection