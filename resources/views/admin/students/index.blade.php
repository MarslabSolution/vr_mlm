@extends('layouts.admin')
@section('content')
@can('student_detail_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('trainer.students.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.studentDetail.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('cruds.studentDetail.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-StudentDetail">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.studentDetail.fields.full_name') }}
                    </th>
                    @can(['user_show', 'user_edit'])
                    <th>
                        {{ trans('cruds.studentDetail.fields.student_efk') }}
                    </th>
                    @endcan
                    <th>
                        {{ trans('cruds.studentDetail.fields.parent_name') }}
                    </th>
                    <th>
                        {{ trans('cruds.studentDetail.fields.parent_phone') }}
                    </th>
                    {{-- <th>
                        {{ trans('cruds.studentDetail.fields.lesson_categories') }}
                    </th>
                    <th>
                        {{ trans('cruds.studentDetail.fields.lesson_group') }}
                    </th> --}}
                    <th>
                        {{ trans('cruds.studentDetail.fields.is_handicapped') }}
                    </th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('student_detail_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ settings('main_system_domain', '','api').settings('api_version', '','api').settings('student_details_massDestroy', '','api') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: {
        url : "{{ (settings('student_details_index', '', 'api') ? settings('main_system_domain', '', 'api').settings('api_version', '', 'api').settings('student_details_index', '', 'api') : '') }}",
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
            'X-VRDRUM-USER': '{{ auth()->user()->email }}',
            'Authorization': '{{ access_token() }}',
            /*'Access-Control-Allow-Credentials': true,
            'Access-Control-Allow-Origin': '*',
			"Access-Control-Allow-Methods": "*",
			"Access-Control-Allow-Headers": "*"*/
        },
        //crossDomain: true,
        //credentials: 'same-origin',
        //xhrFields: {withCredentials: true},
    },
    columns: [
        { data: 'placeholder', name: 'placeholder' },
        { data: 'id', name: 'id' },
        { data: 'full_name', name: 'full_name' },
        @can(['user_show', 'user_edit'])
        { data: 'student_efk', name: 'student_efk', sortable: false, searchable: false },
        @endcan
        { data: 'parent_name', name: 'parent_name' },
        { data: 'parent_phone', name: 'parent_phone' },
        //{ data: 'lesson_categories', name: 'lesson_categories' },
        //{ data: 'lesson_group', name: 'lesson_group' },
        { data: 'is_disabled', name: 'is_disabled' },
        { data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 10,
  };
  let table = $('.datatable-StudentDetail').DataTable(
      dtOverrideGlobals);
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
});
</script>
@endsection