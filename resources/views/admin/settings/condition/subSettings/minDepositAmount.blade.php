
<div class="card">
    <form action="{{ route('admin.settings.save-settings', ['condition' => 'min_deposit_amount']) }}" method="POST">
    @csrf
    <div class="card-header font-semibold text-lg">
        <div class="row align-items-center">
            <div class="px-3">{{ trans('settings.condition.min_deposit_amount').' '.trans('settings.condition.title') }}</div>
            <div class="col ml-auto text-right">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }} {{ trans('cruds.setting.title') }}
                </button>
            </div>
        </div>
    </div>

    <div class="card-body">
        <table class="w-100">
            <thead>
                <tr>
                    <th>{{ trans('settings.condition.packages') }}</th>
                    <th>{{ trans('settings.condition.min_deposit_amount') }} {{ trans('global.rm') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($agentPlans as $key => $agentPlan)
                <tr>
                    <td><a href="{{ route('admin.agent-plans.show', $agentPlan->id) }}" class="required" for="min_deposit_amount">{{$agentPlan->name}}</a></td>
                    <td><input class="form-control {{ $errors->has('min_deposit_amount') ? 'is-invalid' : '' }}" type="number"
                    name="min_deposit_amount[{{$agentPlan->id}}]" value="{{ old('min_deposit_amount', settings('package_min_deposit_amount_'.$agentPlan->id, '', 'condition')) }}"></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </form>
</div>