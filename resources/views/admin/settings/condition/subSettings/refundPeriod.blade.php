
<div class="card">
    <form action="{{ route('admin.settings.save-settings', ['condition' => 'refund_period']) }}" method="POST">
    @csrf
    <div class="card-header font-semibold text-lg">
        <div class="row align-items-center">
            <div class="px-3">{{ trans('settings.condition.refund_period').' '.trans('settings.condition.title') }}</div>
            <div class="col ml-auto text-right">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }} {{ trans('cruds.setting.title') }}
                </button>
            </div>
        </div>
    </div>

    <div class="card-body">
        <table class="w-100">
            <thead>
                <tr>
                    <th>{{ trans('settings.condition.packages') }}</th>
                    <th>{{ trans('settings.condition.refund_period') }} {{ sprintf(trans('global.months'), 6) }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($agentPlans as $key => $agentPlan)
                <tr>
                    <td><a href="{{ route('admin.agent-plans.show', $agentPlan->id) }}" class="required" for="refund_period">{{$agentPlan->name}}</a></td>
                    <td><input class="form-control {{ $errors->has('refund_period') ? 'is-invalid' : '' }}" type="number"
                    name="refund_period[{{$agentPlan->id}}]" value="{{ old('refund_period', settings('package_refund_period_'.$agentPlan->id, '', 'condition')) }}"></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </form>
</div>