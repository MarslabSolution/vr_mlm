
<div class="card">
    <form action="{{ route('admin.settings.save-settings', ['condition' => 'refund_target']) }}" method="POST">
    @csrf
    <div class="card-header font-semibold text-lg">
        <div class="row align-items-center">
            <div class="px-3">{{ trans('settings.condition.refund_target') }}</div>
            <div class="col ml-auto text-right">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }} {{ trans('cruds.setting.title') }}
                </button>
            </div>
        </div>
    </div>

    <div class="card-body">
        <table class="w-100">
            <thead>
                <tr>
                    <th>{{ trans('settings.condition.packages') }}</th>
                    <th>{{ trans('settings.condition.target_downline') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($agentPlans as $key => $agentPlan)
                <tr>
                    <td><a href="{{ route('admin.agent-plans.show', $agentPlan->id) }}" class="required" for="refund_target">{{$agentPlan->name}}</a></td>
                    <td><input class="form-control {{ $errors->has('refund_target') ? 'is-invalid' : '' }}" type="number"
                    name="refund_target[{{$agentPlan->id}}]" value="{{ old('refund_target', settings('package_refund_target_'.$agentPlan->id, '', 'condition')) }}"></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    </form>
</div>