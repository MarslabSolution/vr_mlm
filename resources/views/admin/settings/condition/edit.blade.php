@extends('layouts.admin')
@section('content')
@component('components.settings.edit',[
    'group' => 'condition',
    'setting' => $setting,
]) @endcomponent
@endsection