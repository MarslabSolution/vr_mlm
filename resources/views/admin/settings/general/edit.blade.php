@extends('layouts.admin')
@section('content')
@component('components.settings.edit',[
    'group' => 'general',
    'setting' => $setting,
]) @endcomponent
@endsection