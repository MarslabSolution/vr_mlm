@include('layouts.styles')
<div class="rounded-full h-12 px-5 flex items-center justify-left z-50 mr-52">
    <div class="mr-4 sm:block text-slate-600 dark:text-slate-200">{{ trans('global.colorScheme')}}</div>
    <a href="{{ route('color-scheme-switcher', ['color_scheme' => 'default']) }}" class="block w-9 h-8 cursor-pointer bg-[#ee3325] rounded-full border-4 mr-1 hover:border-slate-200 {{ $color_scheme =='default' ? 'border-slate-300 dark:border-darkmode-800/80' : 'border-white dark:border-darkmode-600' }}"></a>

    <a href="{{ route('color-scheme-switcher', ['color_scheme' => 'theme-1']) }}" class="block w-9 h-8 cursor-pointer bg-[#be1a0e] rounded-full border-4 mr-1 hover:border-slate-200 {{ $color_scheme =='theme-1' ? 'border-slate-300 dark:border-darkmode-800/80' : 'border-white dark:border-darkmode-600' }}"></a>
</div>