@extends('layouts.admin')
@section('styles')
<style>
    .settings-btn{
        display: none;
    }
    .settings-btn a {
        margin: 0 5px;
    }
</style>
@endsection
@section('content')
@can('setting_create')
<div style="margin-bottom: 10px;" class="row no-gutters">
    <div class="col-4 setting_btn mb-3">
        <a class="btn btn-success add_condition" href="{{ route('admin.settings.create', ['group' => 'api']) }}">
            {{ trans('global.add') }} {{ trans('cruds.setting.title') }}
        </a>
    </div>
</div>
@endcan
<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('cruds.setting.title') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="datatable-settings" class="table table-bordered table-striped table-hover datatable datatable-Settings">
                <thead>
                    <tr>
                        <th width="10"></th>
                        <th>
                            {{ trans('settings.global.name') }}
                        </th>
                        <th>
                            {{ trans('settings.global.value') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($settings as $key => $setting)
                    <tr data-entry-id="{{ $setting->id }}">
                        <td></td>
                        <td>
                            @if (trans()->has('settings.condition_values.'.$setting->name))
                            {{ trans('settings.condition_values.'.$setting->name) ?? '' }}
                            @else {{ $setting->name ?? '' }}
                            @endif
                        </td>
                        <td>
                            {{ json_decode($setting->payload) ?? '' }}
                        </td>
                        <td>
                            @can('setting_edit')
                            <a class="btn btn-xs btn-info" href="{{ route('admin.settings.edit', [ $setting->id, 'group' => 'api']) }}">
                                {{ trans('global.edit') }}
                            </a>
                            @endcan
                            @can('setting_delete')
                            <form action="{{ route('admin.settings.destroy', $setting->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                <input type="hidden" name="_method" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}" {{ strlen($setting->id) < 3 ? 'disabled' : '' }}>
                            </form>
                            @endcan
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection
@section('scripts')
@parent
<script src="https://cdn.datatables.net/datetime/1.1.2/js/dataTables.dateTime.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables.net-editor/2.0.5/dataTables.editor.min.js"></script>
<script>
    var editor; // use a global for the submit and return data rendering in the examples
    $(function() {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        @can('setting_delete')
        let deleteButtonTrans = "{{ trans('global.datatables.delete') }}"
        let deleteButton = {
            text: deleteButtonTrans,
            url: "{{ route('admin.settings.massDestroy') }}",
            className: 'btn-danger',
            action: function(e, dt, node, config) {
                var ids = $.map(dt.rows({
                    selected: true
                }).nodes(), function(entry) {
                    return $(entry).data('entry-id')
                });

                if (ids.length === 0) {
                    alert("{{ trans('global.datatables.zero_selected ') }}");
                    return
                } else if (Math.max(ids.map(word => word.length)) < 3) {
                    alert("Selected item is protected, cannot be deleted.");
                    return
                }

                if (confirm("{{ trans('global.areYouSure') }}")) {
                    $.ajax({
                            headers: {
                                'x-csrf-token': _token
                            },
                            method: 'POST',
                            url: config.url,
                            data: {
                                ids: ids,
                                _method: 'DELETE'
                            }
                        })
                        .done(function() {
                            location.reload()
                        })
                }
            }
        }
        dtButtons.push(deleteButton)
        @endcan
        $.extend(true, $.fn.dataTable.defaults, {
            orderCellsTop: true,
            order: [
                //[1, 'desc']
            ],
            pageLength: 100,
        });
        /*  need license for datatables editor. https://editor.datatables.net/purchase/index
            editor = new $.fn.dataTable.Editor({
            ajax: "route('admin.settings.update')",
            table: ".datatable-settings",
            fields: [ {
                label: "First name:",
                name: "first_name"
            }, {
                label: "Value:",
                name: "value"
            }]
        });
        $('.datatable-Settings').on( 'click', 'tbody td div.row-edit', function (e) {
            editor.inline( table.cells(this.parentNode, '*').nodes(), {
                onBlur: 'submit'
            });
        }); */
        let table = $('.datatable-Settings:not(.ajaxTable)').DataTable({
            buttons: dtButtons
        })
        $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e) {
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });

    })
</script>
@endsection