@extends('layouts.admin')
@section('content')
@component('components.settings.edit',[
    'group' => 'api',
    'setting' => $setting,
]) @endcomponent
@endsection