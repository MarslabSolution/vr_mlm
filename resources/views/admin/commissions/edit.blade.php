@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('global.edit') }} {{ trans('cruds.commission.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.commissions.update", [$commission->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="tuition_package_efk">{{ trans('cruds.commission.fields.tuition_package_efk') }}</label>
                @include('partials.commissions.tuitionPackageEfk')
                <span class="help-block">{{ trans('cruds.commission.fields.tuition_package_efk_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="agent_plan_id">{{ trans('cruds.commission.fields.agent_plan') }}</label>
                <select required class="form-control select2 {{ $errors->has('agent_plan') ? 'is-invalid' : '' }}" name="agent_plan_id" id="agent_plan_id">
                    <option value="" disabled>{{  trans('global.pleaseSelect') }}</option>
                    @foreach($agent_plans as $agent_plan)
                    <option value="{{ $agent_plan->id }}" {{ (old('agent_plan_id') ?? $commission->agent_plan->id ?? '') == $agent_plan->id ? 'selected' : '' }}>{{ $agent_plan->name }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.commission.fields.type_helper') }}</span>
            </div>
            <div class="form-group commissionable-level" style="{{ isset($commission->agent_plan->id) ? '' : 'display: none;' }}">
                <label class="required" for="commission">{{ trans('cruds.commission.fields.commission') }}</label>
                <p class="commissionable-level"></p>
                <span class="help-block">{{ trans('cruds.commission.fields.commission_helper') }}</span>
                <div class="card-body"></div>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@include('partials.commissions.customScripts')
@endsection