@if(isset($detailCommissions))
<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('cruds.eachLevelCommission.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-EachLevelCommission">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.eachLevelCommission.fields.commission') }}
                        </th>
                        <th>
                            {{ trans('cruds.eachLevelCommission.fields.level') }}
                        </th>
                        <th>
                            {{ trans('cruds.fields.updated_at') }}
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($detailCommissions as $key => $detailCommission)
                    <tr data-entry-id="{{ $detailCommission->id }}">
                        <td></td>
                        <td>{{ $detailCommission->id ?? '' }}</td>
                        <td>{{ $detailCommission->commission ?? '' }}</td>
                        <td>
                            {{ $detailCommission->level ? (
                                $detailCommission->level > 0 ?
                                sprintf(trans('cruds.commission.fields.commission_down_x_level'), $detailCommission->level)  : 
                                 trans('cruds.commission.fields.commission_current_level') 
                            ): trans('cruds.commission.fields.commission_current_level')  }}
                        </td>
                        <td>{{ $detailCommission->updated_at ?? '' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function() {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        $.extend(true, $.fn.dataTable.defaults, {
            orderCellsTop: true,
            order: [
                [3, 'asc']
            ],
            pageLength: 10,
        });
        let table = $('.datatable-EachLevelCommission:not(.ajaxTable)').DataTable({
            buttons: dtButtons
        })
        $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e) {
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    })
</script>
@endsection
@endif