@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('global.edit') }} {{ trans('cruds.commission.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.commissions.update", [$commission->id, 'type'=>'affiliate']) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            @csrf
            <input type="hidden" name="type" value="affiliate">
            <div class="form-group agent_plan_id">
                <label class="required form-labell" for="agent_plan_id">{{ trans('cruds.commission.fields.agent_plan') }}</label>
                <select required class="form-control select2 {{ $errors->has('agent_plan') ? 'is-invalid' : '' }}" name="agent_plan_id" id="agent_plan_id">
                    <option value="" disabled>{{ trans('global.pleaseSelect') }}</option>
                    @foreach($agent_plans as $agent_plan)
                    <option value="{{ $agent_plan->id }}" {{ (old('agent_plan_id') ?? $commission->agent_plan->id ?? '') == $agent_plan->id ? 'selected' : '' }}>{{ $agent_plan->name }}</option>
                    @endforeach
                </select>
                @if($errors->has('agent_plan'))
                <div class="invalid-feedback">
                    {{ $errors->first('agent_plan') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.commission.fields.type_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="commissionable_level">{{ trans('cruds.commission.fields.commission_for_up_line') }}</label>
                <div class="container row">
                <input class="col-auto" type="number" name="commissionable_level" id="commissionable_level" step="1" value="{{ old('commissionable_level') ?? $commissionableLevel }}"> 
                <div class="col-auto">
                    <button class="btn btn-info btn-add" onclick="getCommissionFields($('#commissionable_level').val(), $('#agent_plan_id').val())" type="button">
                        {{ trans('global.add') }}
                    </button>
                    <button class="btn btn-secondary btn-reset" style="display: none;" type="button" onclick="$('#commissionable_level').val(''); $(this).hide(); $('div.commission').hide();  $('.btn-add').html('{{ trans('global.add') }}')">
                        {{ trans('global.reset') }}
                    </button>
                </div>
                </div>
            </div>
            <div class="form-group commission" style="display: none;">
                <div class="card-body"></div>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
@include('partials.commissions.affiliateScripts')
@endsection