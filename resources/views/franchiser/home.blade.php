@extends('franchiser.layout')
@section('body')
<div class="content ml-0">
    <div class="p-1">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="alert text-center mobile-spacing-bottom">
                    <h5 class="h5-heading-style bottom-5">{{ trans('cruds.franchiserHome.fields.downline') }}</h5>
                    <div class="basic-progressbar">
                        <div class="progress">
                            <div class="progress-bar w-{{ round(100 / (int)settings('maximum_franchiser_group', config('constants.franchiser.group_franchiser_maximum'), 'condition') * count($franchiser_8k)) }}" role="progressbar">
                                
                            </div>
                        </div>
                        <div class="d-flex justify-content-center pt-1">
                            <h5>{{ count($franchiser_8k) . ' / ' . (int)settings('maximum_franchiser_group', config('constants.franchiser.group_franchiser_maximum'), 'condition') }}</h5>
                            <h5 class="ml-2">({{ number_format(count($franchiser_8k) / (int)settings('maximum_franchiser_group', config('constants.franchiser.group_franchiser_maximum'), 'condition') * 100, 1) . ' %' }})</h5>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Filter part -->
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 alert text-center">
                <h5 class="h5-heading-style bottom-5">{{ trans('cruds.franchiserHome.fields.month_select') }}</h5>
                <select class="form-control select2" name="month_select" id="month_select">
                    @foreach($months as $id => $value)
                        <option value="{{ $value }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 alert text-center">
                <h5 class="h5-heading-style bottom-5">{{ trans('cruds.franchiserHome.fields.level_select') }}</h5>
                <select class="form-control select2" name="level_select" id="level_select">
                    @foreach($levels as $id => $value)
                        <option value="{{ $value }}">{{ $value }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-xl-3 col-lg-3 col-md-4 col-sm-4">
                <!-- Commission (RM) part -->
                <div class="col alert text-center spacing-bottom-0">
                    <h5 class="h5-heading-style bottom-5">{{ trans('cruds.franchiserHome.fields.commission') }} {{ trans('global.rm') }}</h5>
                </div>
                <div class="card alert text-center align-items-center mobile-spacing zoom-in">
                    <h6 class="bottom-5">{{ trans('cruds.franchiserHome.fields.user_student') }}</h6>
                    <h6 id="user-student-commission" class="commission-text"></h6>
                    <div class="spinner-border text-primary commission-loading" role="status">
                    </div>
                </div>
                <div class="card alert text-center align-items-center mobile-spacing zoom-in">
                    <h6 class="bottom-5">{{ trans('cruds.franchiserHome.fields.downline_student') }}</h6>
                    <h6 id="downline-student-commission" class="commission-text"></h6>
                    <div class="spinner-border text-primary commission-loading" role="status">
                    </div>
                </div>
                <div class="card alert text-center align-items-center mobile-spacing zoom-in">
                    <h6 class="bottom-5">{{ trans('cruds.franchiserHome.fields.downline_join') }}</h6>
                    <h6 id="join-commission" class="commission-text"></h6>
                    <div class="spinner-border text-primary commission-loading" role="status">
                    </div>
                </div>

                <!-- Number of student (qty) part -->
                <div class="col alert text-center spacing-bottom-0">
                    <h5 class="h5-heading-style bottom-5">{{ trans('cruds.franchiserHome.fields.quantity_student') }}</h5>
                </div>
                <div class="card alert text-center align-items-center zoom-in">
                    <h6 class="bottom-5">{{ trans('cruds.franchiserHome.fields.downline_student') }}</h6>
                    <h6 id="downline-student-number" class="commission-text"></h6>
                    <div class="spinner-border text-primary commission-loading" role="status">
                    </div>
                </div>
                <div class="card alert text-center align-items-center zoom-in">
                    <h6 class="bottom-5">{{ trans('cruds.franchiserHome.fields.user_student') }}</h6>
                    <h6 id="user-student-number" class="commission-text"></h6>
                    <div class="spinner-border text-primary commission-loading" role="status">
                    </div>
                </div>
            </div>

            <!-- Chart part -->
            <div class="col-lg-9 col-md-8 col-sm-8">
                <div class="card card-spacing">
                    <div class="card-body">
                        <h5 id="commissionChartTitle" class="card-title h5-heading-style">{{ trans('cruds.franchiserHome.fields.commission_chart') }}</h5>
                        <div class="spinner-border text-primary commission-loading" role="status">
                        </div>
                        <div style="height: 500px">
                            <canvas id="commissionChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@endsection
@section('scripts')
@parent
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/3.8.0/chart.min.js"></script>
<script>
    $(function () {
    let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    var $month_select = $("#month_select")
    var $level_select = $("#level_select")
    var $loadingDivs = $(".commission-loading")
    var $commissionTexts = $(".commission-text")
    var $commissionChart = $("#commissionChart")[0].getContext("2d")
    var $previousChart = null

    $month_select.on('change', function(){
        getCommission($month_select.val(), $level_select.val())
        renderCommissionChart($month_select.val(), $level_select.val())

        var $commissionChartTitle = "{{ trans('cruds.franchiserHome.fields.commission_chart') }}"
        $("#commissionChartTitle").html($commissionChartTitle.replace("%s", $("#month_select").val()))
    }).trigger('change');

    $level_select.on('change', function(){
        getCommission($month_select.val(), $level_select.val())
        renderCommissionChart($month_select.val(), $level_select.val())
    }).trigger('change');

    function getCommission($month, $level){
        $loadingDivs.show()
        $commissionTexts.hide()

        $.ajax({
            headers: {
                'x-csrf-token': _token
            },
            method: 'POST',
            url: "{{ route('franchiser.home.getCommission') }}",
            data: {
                month: $month,
                level: $level,
            }
        })
        .done(function(data) {
            var $downline_commission = data['downline_commission']
            var $user_commission = data['user_commission']
            var $join_commission = data['join_commission']
            var $downline_student = data['downline_student']
            var $user_student = data['user_student']

            $('#user-student-commission').text($user_commission ?? '-')
            $('#downline-student-commission').text($downline_commission ?? '-')
            $('#join-commission').text($join_commission ?? '-')
            $('#downline-student-number').text($downline_student ?? '-')
            $('#user-student-number').text($user_student ?? '-')

            $loadingDivs.hide()
            $commissionTexts.show()
        });
    }

    function renderCommissionChart($month, $level){
        $loadingDivs.show()

        $.ajax({
            headers: {
                'x-csrf-token': _token
            },
            method: 'POST',
            url: "{{ route('franchiser.home.getLatestMonthCommission') }}",
            data: {
                month: $month,
                level: $level,
            }
        })
        .done(function(chartData) {
            if($previousChart != null){
                $previousChart.destroy()
            }

            if ($("#commissionChart").length) {
                $previousChart = new Chart(commissionChart, {
                    type: "bar",
                    data: {
                        labels: chartData.map((dataValue) => {
                            return dataValue['date'].substring(0, dataValue['date'].lastIndexOf('-'));
                        }),
                        datasets: [
                            {
                                label: "{{ trans('cruds.franchiserHome.fields.label_you_own') }}",
                                barPercentage: 0.3,
                                borderWidth: 1,
                                data: chartData.map((dataValue) => {
                                    return dataValue['user']
                                }),
                                borderColor: '#ffb366',
                                backgroundColor: '#ffb366'
                            },
                            {
                                label: "{{ trans('cruds.franchiserHome.fields.label_downlne') }}",
                                barPercentage: 0.3,
                                borderWidth: 1,
                                data: chartData.map((dataValue) => {
                                    return dataValue['downline']
                                }),
                                borderColor: '#009efb',
                                backgroundColor: '#009efb'
                            },
                            {
                                label: "{{ trans('cruds.franchiserHome.fields.label_join') }}",
                                barPercentage: 0.3,
                                borderWidth: 1,
                                data: chartData.map((dataValue) => {
                                    return dataValue['join']
                                }),
                                borderColor: '#cc33ff',
                                backgroundColor: '#cc33ff'
                            }
                        ]
                    },
                    options: {
                        indexAxis: "y",
                        maintainAspectRatio: false,
                        plugins: {
                        legend: {
                            labels: {
                                color: '#b4becb',
                            }
                        }
                        },
                        scales: {
                            x: {
                                ticks: {
                                    font: {
                                        size: 12
                                    },
                                    color: '#b4becb',
                                    callback: function callback(value, index, values) {
                                        return "{{ trans('global.rm') }} " + value;
                                    }
                                },
                                grid: {
                                display: false,
                                drawBorder: false
                                }
                            },
                            y: {
                                ticks: {
                                    font: {
                                        size: 12
                                    },
                                    color: '#b4becb',
                                },
                                grid: {
                                    color: $("html").hasClass("dark") ? '#b4becb' : '#b4becb',
                                    borderDash: [2, 2],
                                    drawBorder: false
                                }
                            }
                        }
                    }
                });
            }

            $loadingDivs.hide()
        });
    }
});

</script>
@endsection
