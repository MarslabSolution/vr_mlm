@extends('franchiser.layout')
@section('body')
<div class="content ml-0">
    <div class="intro-y box mt-4 mb-6">
        <div class="flex flex-col sm:flex-row items-center p-01 border-b border-slate-200/60">
            <h2 class="font-semibold text-base mr-4 mb-01">{{ trans('global.create') }}{{ trans('cruds.franchiserAgent.agent') }}</h2>
        </div>

        <div class="card-body">
            <form class="spacing-bottom-0" method="POST" action="{{ route("franchiser.franchiser-agent.store") }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group spacing-top-0">
                    <label class="required" for="agent_plan">{{ trans('cruds.franchiserAgent.fields.agent_plan') }}</label>
                    <select class="form-control select2 {{ $errors->has('agent_plan') ? 'is-invalid' : '' }}" name="agent_plan" id="agent_plan" required>
                        @foreach($agent_plans as $index => $plan)
                            <option value="{{ $plan->id }}">{{ $plan->name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('agent_plan'))
                        <div class="invalid-feedback">
                            {{ $errors->first('agent_plan') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.franchiserAgent.fields.agent_plan_helper') }}</span>
                </div>
                <div class="form-group spacing-top-0">
                    <label class="required" for="upline">{{ trans('cruds.franchiserAgent.fields.upline') }}</label>
                    <select class="form-control select2 {{ $errors->has('upline') ? 'is-invalid' : '' }}" name="upline" id="upline" required>
                        @foreach($downlines as $level_index => $level_donwlines)
                            @foreach($level_donwlines as $downline_index => $downline)
                                <option value="{{ $downline->id }}" agentPlan="{{ $level_index }}">{{ $downline->user->id . ' - ' . $downline->user->name }}</option>
                            @endforeach
                        @endforeach
                    </select>
                    @if($errors->has('upline'))
                        <div class="invalid-feedback">
                            {{ $errors->first('upline') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.franchiserAgent.fields.agent_plan_helper') }}</span>
                </div>
                <div class="form-group">
                    <label class="required" for="name">{{ trans('cruds.fields.name') }}</label>
                    <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                    @if($errors->has('name'))
                        <div class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.fields.name_helper') }}</span>
                </div>
                <div class="form-group">
                    <label class="required" for="email">{{ trans('cruds.fields.email') }}</label>
                    <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="email" name="email" id="email" value="{{ old('email') }}" required>
                    @if($errors->has('email'))
                        <div class="invalid-feedback">
                            {{ $errors->first('email') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.fields.email_helper') }}</span>
                </div>
                <div class="form-group">
                    <label class="required" for="password">{{ trans('cruds.dealer.fields.password') }}</label>
                    <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" id="password" required>
                    @if($errors->has('password'))
                        <div class="invalid-feedback">
                            {{ $errors->first('password') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.dealer.fields.password_helper') }}</span>
                </div>
                <div class="form-group spacing-bottom-0">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection

@section('scripts')
@parent
<script>
$(function () {
    var $agent_select = $('#agent_plan')
    var $upline_select = $('#upline')
    var $upline_options = $upline_select.find('option')
    
    $agent_select.on('change', function(){
        agentOptionController()
    }).trigger('change');


    function agentOptionController(){
        var $options = $upline_options.filter('[agentPlan="'+ $agent_select.val() + '"]')

        if($options.length == 0){
            $upline_select.val("")
            $upline_select.html($upline_options.filter('[agentPlan=""]'))
        }else{
            $upline_select.val("")
            $upline_select.html($options)
        }
    }
});
</script>
@endsection