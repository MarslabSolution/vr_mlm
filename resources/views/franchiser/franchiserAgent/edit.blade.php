@extends('franchiser.layout')
@section('body')
<div class="content ml-0">
    <div class="intro-y box mt-4 mb-6">
        <div class="flex flex-col sm:flex-row items-center p-01 border-b border-slate-200/60">
            <h2 class="font-semibold text-base mr-4 mb-01">{{ trans('global.edit') }} {{ trans('cruds.dealer.title_singular') }}</h2>
        </div>

        <div class="card-body">
            <form class="spacing-bottom-0" method="POST" action="{{ route("franchiser.franchiser-agent.update") }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="form-group spacing-top-0">
                    <label class="required" for="downline">{{ trans('cruds.franchiserAgent.fields.downline') }} {{ trans('cruds.franchiserAgent.fields.email') }}</label>
                    <select class="form-control select2 {{ $errors->has('downline') ? 'is-invalid' : '' }}" name="downline" id="downline" required>
                    </select>
                    @if($errors->has('downline'))
                        <div class="invalid-feedback">
                            {{ $errors->first('downline') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.franchiserAgent.fields.downline_helper') }}</span>
                </div>
                <div class="form-group">
                    <label for="id">{{ trans('cruds.franchiserAgent.fields.selected_downline') }} {{ trans('cruds.fields.id') }}</label>
                    <input class="form-control" type="text" name="downline_id" id="downline_id" readonly>
                </div>
                <div class="form-group">
                    <label for="name">{{ trans('cruds.franchiserAgent.fields.selected_downline') }} {{ trans('cruds.fields.name') }}</label>
                    <input class="form-control" type="text" name="downline_name" id="downline_name" readonly>
                </div>
                <div class="form-group">
                    <label class="required" for="upgrade_plan">{{ trans('cruds.franchiserAgent.fields.upgrade_plan') }}</label>
                    <select class="form-control select2 {{ $errors->has('upgrade_plan') ? 'is-invalid' : '' }}" name="upgrade_plan" id="upgrade_plan" required>
                        @foreach($upgrade_plans as $index => $value)
                            <option value="{{ $value->id }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                    @if($errors->has('upgrade_plan'))
                        <div class="invalid-feedback">
                            {{ $errors->first('upgrade_plan') }}
                        </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.franchiserAgent.fields.upgrade_plan_helper') }}</span>
                </div>
                <div class="form-group spacing-bottom-0">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('cruds.franchiserAgent.fields.upgrade') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
        var $downline_select = $("#downline")

        $downline_select.select2({
            ajax: {
                url: "{{ route('franchiser.franchiser-agent.searchDownline') }}",
                method: "POST",
                headers: {
                    'x-csrf-token': _token
                },
                dataType: 'json',
                cache: true,
                delay: 250,
                data: function (params) {
                    return {
                        search: params.term,
                        page: params.page || 1,
                    }
                },
                processResults: function (data, params) {
                    return {
                        results: data.results,
                        pagination: params.page || 1,
                    };
                }
            },
            minimumInputLength: 1,
            placeholder: "{{ trans('cruds.franchiserAgent.fields.select_downline') }}",
        });

        $downline_select.on('change', function(){
            var $downline_detail = $downline_select.val().split("|")
            var $downline_id = $downline_detail[0];
            var $downline_name = $downline_detail[1];

            $('#downline_id').val($downline_id);
            $('#downline_name').val($downline_name);
        })
    });
</script>
@endsection