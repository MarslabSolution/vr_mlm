@if(!isset($isExpires) || $isExpires == null)
<?php $isExpires = false ?>
@endif
@if(!isset($errors) || $errors == null)
<?php $errors = [] ?>
@endif

@extends('layouts.master')
<div class="c-wrapper mr-0">
    <header class="top-bar px-8">
        <img src="{{ asset('images/VrWhite.png') }}" alt="" class="logo-svg left-logo">
    </header>
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid px-8 mb-16">
                <div class="content ml-0">
                    <div class="intro-y box mt-4 mb-6">
                        <div class="flex flex-col sm:flex-row items-center p-01 border-b border-slate-200/60">
                            <h2 class="font-semibold text-base mr-4 mb-01">{{ trans('cruds.franchiserAgent.message.agent_verify') }}</h2>
                        </div>
                        
                        <div class="card-body">
                            @if($isExpires)
                                {{ trans('validation.link_expired') }}
                            @elseif(!empty($errors))
                                @foreach($errors as $index => $error)
                                    {{ $error }}
                                @endforeach
                            @else
                                {{ trans('cruds.franchiserAgent.message.verify_success') }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>
