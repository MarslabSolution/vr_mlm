<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Verification Email</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <style type="text/css">
            body, table, td, a { 
                -webkit-text-size-adjust: 100%;
                -ms-text-size-adjust: 100%;
            }
            table, td { 
                mso-table-lspace: 0pt; 
                mso-table-rspace: 0pt; 
            }
            img { 
                border: 0; 
                height: auto; 
                line-height: 100%; 
                outline: none; 
                text-decoration: none; 
            }
            table { 
                border-collapse: collapse !important;
            }
            body { 
                height: 100% !important; 
                margin: 0 !important; 
                padding: 0 !important; 
                width: 100% !important; 
            }
            .body-bg {
                background-color: #FFFFFF;
            }
            .container {
                padding: 20px;
            }
            .center {
                text-align: center;
            }
            .solid {
                border-top: 2px solid #F1F5F9;
            }
            .text-style {
                color: #000000 !important;
                font-family: sans-serif !important;
                font-size: 14px !important;
            }
            .white-text {
                color: #FFFFFF !important;
                font-family: sans-serif !important;
                font-size: 10px !important;
            }
        </style>
    </head>
    <body class="body-bg">
        <table cellspacing="0" cellpadding="0" role="presentation" width="600" bgColor="#EE3325">
        <tr>
            <td class="center" style="padding: 20px 0 20px 0;">
                <img src="https://www.vrculturetech.com.my/images/VrWhite.png" width="100px" alt="logo">
            </td>
        </tr>
        <tr>
            <td class="container text-style" style="background-color: #FFFFFF; border: 1px solid #EE3325;" width="100%">
                <h2 style="margin: 14px 0 !important;">{{ trans('cruds.franchiserAgent.message.agent_verify') }}</h2>
                <p>{{ trans('cruds.franchiserAgent.message.click_here') }} <a href="{{ $url }}" style="color: #EE3325"><strong>{{ trans('cruds.franchiserAgent.message.verify_link') }}</strong></a> {{ trans('cruds.franchiserAgent.message.verify_account') }}</p>
                <p>{{ trans('cruds.franchiserAgent.message.click_to_verify') }}</p>
                <p>{{ $url }}</p>
            </td>
        </tr>
        <tr>
            <td class="center" style="background-color: #EE3325; padding: 30px 0 10px;">
                <table cellspacing="0" cellpadding="0" width="150" align="center">
                    <tr>
                        <td width="50">
                            <a href="https://www.facebook.com/VRDrummingAcademy">
                                <img src="https://vrculturetech.com.my/wp-content/uploads/2022/08/fb.png" width="40" alt="facebook icon" />
                            </a>
                        </td>
                        <td width="50">
                            <a href="https://www.instagram.com/vrdrummingacademy/">
                                <img src="https://vrculturetech.com.my/wp-content/uploads/2022/08/insta.png" width="40" alt="instagram icon" />
                            </a>
                        </td>
                        <td width="50">
                            <a href="https://www.youtube.com/channel/UCNmapAxFW4GDM5b6xF4iBhQ">
                                <img src="https://vrculturetech.com.my/wp-content/uploads/2022/08/yt.png" width="40" alt="youtube icon" />
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="center white-text" style="background-color: #EE3325; padding: 0 20px 10px;">
                <span>{{ trans('cruds.franchiserAgent.message.do_not_reply_email') }}</span><br>
                <span>{!! trans('cruds.franchiserAgent.message.add_address') !!}</span><br>
            </td>
        </tr>
        <tr>
            <td class="container center white-text" style="background-color: #EE3325; padding: 0 20px 30px;">
                <hr class="solid">
                <span>{{ trans('cruds.franchiserAgent.message.vr_copyright') }}</span><br>
                <span>{{ trans('cruds.franchiserAgent.message.vr_address') }}</span>
            </td>
            </tr>
        </table>
    </body>
</html>