@extends('franchiser.layout')
@section('body')
<div class="content ml-0">
    <div class="intro-y flex items-center mt-4">
        <h2 class="text-xl font-semibold truncate mr-5">{{ trans('cruds.agentDownline.title') }}</h2>
    </div>

    <div class="intro-y row">
        <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12 alert spacing-mb-0">
            <ul class="nav nav-pills" id="formTab" role="tablist">
                @foreach($level_downlines as $index => $level_data)
                    <li class="nav-item">
                        <a class="nav-link {{ $index == 0 ? 'active' : '' }}" id="downline-{{ $index }}Btn" data-toggle="pill" href="#downline-{{ $index }}Tab" role="tab" aria-controls="downline-{{ $index }}Tab" aria-selected="true">
                            {{ $index == 0 ? trans('cruds.agentDownline.fields.all') . ' ' . trans('cruds.agentDownline.fields.level') : trans('cruds.agentDownline.fields.level') . $index }}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 alert mt-auto mobile-date-filter-40">
            <label id="date-filter" class="font-medium text-base mb-01 date-filter-right date-filter-left">{{ trans('cruds.agentDownline.fields.date_filter') }}</label>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4 alert mt-auto spacing-bottom-1 mobile-date-filter-60">
            <select class="form-control select2" name="month_select" id="month_select">
                @foreach($month_select as $index => $date_range)
                    <option value="{{ $date_range['from'] . '|' . $date_range['to'] }}">{{ $index == 0 ? trans('cruds.agentDownline.fields.all') : substr($date_range['from'], 0, 10) . ' - ' . substr($date_range['to'], 0, 10) }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="tab-content" id="formTabContent">
        @foreach($level_downlines as $level_index => $level_data)
            <div class="tab-pane fade active" style="display: none" id="downline-{{ $level_index }}Tab" role="tabpanel" aria-labelledby="downline-{{ $level_index }}Btn">
                <div class="intro-y box mt-4 mb-4">
                    <div class="flex flex-row justify-content-between px-6 border-b border-slate-200/60 p-01">
                        <div class="flex flex-col sm:flex-row items-center">
                            <h2 class="font-medium text-base mr-auto mb-01">{{ $level_index == 0 ? trans('cruds.agentDownline.fields.all') . ' ' . trans('cruds.agentDownline.fields.level') : trans('cruds.agentDownline.fields.level') . ' ' . $level_index }} {{ trans('cruds.agentDownline.title_singular') }} {{ trans('global.list') }}</h2>
                        </div>
                        <div class="flex items-center text-end" id="total-commission-{{ $level_index }}">{{ trans('cruds.agentDownline.fields.total_commission') }} {{ trans('global.rm') }} {{ $level_data['commission'] }}</div>
                    </div>

                    <div class="card-body">
                        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-AgentDownline-{{ $level_index }} text-14-size">
                            <thead>
                                <tr>
                                    <th width="10">
                                    </th>
                                    <th>
                                        {{ trans('cruds.fields.id') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.agentDownline.fields.user') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.agentDownline.fields.current_plan') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.agentDownline.fields.total_student') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.agentDownline.fields.register_at') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.agentDownline.fields.cumulative_commission') }} {{ trans('global.rm') }}
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($level_data['downlines'] as $index => $downline)
                                    <tr data-entry-id="{{ $downline->id }}">
                                        <td>
                                        </td>
                                        <td>
                                            {{ $downline->id }}
                                        </td>
                                        <td>
                                            {{ $downline->user_name }}
                                        </td>
                                        <td>
                                            {{ $downline->current_plan_name }}
                                        </td>
                                        <td id="level-{{ $level_index }}-downlineId-{{ $downline->id }}-student">
                                            {{ $downline->student_count }}
                                        </td>
                                        <td>
                                            {{ $downline->register_at }}
                                        </td>
                                        <td id="level-{{ $level_index }}-downlineId-{{ $downline->id }}-commission">
                                            {{ $downline->cumulative_commission }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection
@section('scripts')
@parent
<script>
    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

        let dtOverrideGlobals = {
            buttons: dtButtons,
            processing: true,
            // serverSide: true,
            retrieve: true,
            aaSorting: [],
            scrollX: true,
            columns: [
                { data: 'placeholder', name: 'placeholder' },
                { data: 'id', name: 'id' },
                { data: 'user_name', name: 'user.name' },
                { data: 'current_plan_name', name: 'current_plan.name' },
                { data: 'student_count', name: 'student_count' },
                { data: 'register_at', name: 'register_at' },
                { data: 'cumulative_commission', name: 'cumulative_commission' },
            ],
            orderCellsTop: true,
            order: [[ 1, 'desc' ]],
            pageLength: 100,
        };

        var $tol_table = "{{ count($level_downlines) }}"

        for(var $i = 0; $i <= $tol_table; $i++){
            let table = $('.datatable-AgentDownline-' + $i).DataTable(dtOverrideGlobals);

            if($i == 0) $('#downline-'+$i+'Tab').show();
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });

        for(var $i = 0; $i <= $tol_table; $i++){
            $('#downline-'+$i+'Btn').on('click', function(){
                $level_index = $(this).attr('id').replace('downline-', '').replace('Btn', '');

                if($level_index != 0){
                    $('#downline-'+$level_index+'Tab').show();
                    $('.datatable-AgentDownline-' + $level_index).DataTable().columns.adjust()
                }

                $(this).off('click');
            });
        }

        var $month_select = $("#month_select")

        $month_select.on('change', function(){
            $data_range = $month_select.val().split("|")
            $data_from = $data_range[0]
            $data_to = $data_range[1]

            $.ajax({
                headers: {
                    'x-csrf-token': _token
                },
                method: 'POST',
                url: "{{ route('franchiser.franchiser-downline.update') }}",
                data: {
                    from: $data_from,
                    to: $data_to,
                }
            })
            .done(function($data) {
                $data.forEach(($value, $level_index) => {
                    $commission = $value['commission']
                    $downlines = $value['downlines']
                    $("#total-commission-"+$level_index).html("{{ trans('cruds.agentDownline.fields.total_commission') }} {{ trans('global.rm') }} " + $commission)

                    $downlines.forEach(($downline) => {
                        $("#level-"+ $level_index +"-downlineId-"+ $downline['id'] +"-student").html($downline['student_count'])
                        $("#level-"+ $level_index +"-downlineId-"+ $downline['id'] +"-commission").html($downline['cumulative_commission'])
                    })
                })
            });
        }).trigger('change');
    });
</script>
@endsection
