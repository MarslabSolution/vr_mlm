@extends('franchiser.layout')
@section('body')
<div class="content ml-0">
    <div class="intro-y flex items-center mt-4">
        <h2 class="text-xl font-semibold truncate mr-5">{{ trans('cruds.agentStudent.nav_title') }}</h2>
    </div>

    <div class="grid grid-cols-12 gap-6">
        <div class="col-span-12 lg:col-span-6">
            <div class="flex flex-col sm:flex-row items-center spacing-pt-5 justify-center sm:justify-start">
                <h2 class="font-medium text-lg mr-auto dis-contents" >{{ trans('cruds.agentStudent.target') }}</h2>
            </div>
            <div class="pt-4">
                <div class="basic-progressbar">
                    <div class="progress">
                        <div class="progress-bar w-{{ 100 / config('constants.dealer.minimum_temp') * $total_student }}" role="progressbar">
                            {{ $total_student . ' / ' . config('constants.dealer.minimum_temp') }}
                        </div>
                    </div>
                    @if($total_student == 0)
                    <div class="d-flex justify-content-center">{{ $total_student . ' / ' . config('constants.dealer.minimum_temp') }}</div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box mt-10">
        <div class="flex flex-col sm:flex-row items-center p-01 border-b border-slate-200/60">
            <h2 class="font-medium text-lg mr-auto mb-01">{{ trans('cruds.agentStudent.in_month_title') }} {{ trans('global.list') }}</h2>
        </div>

        <div class="card-body">
            <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-DealerStudentInMonth text-14-size">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.agentStudent.fields.tuition_package_efk') }}
                        </th>
                        <th>
                            {{ trans('cruds.agentStudent.fields.student_id') }}
                        </th>
                        <th>
                            {{ trans('cruds.agentStudent.fields.register_at') }}
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="intro-y box mt-10 mb-6">
        <div class="row p-01 border-b border-slate-200/60">
            <div class="col-lg-6 col-md-6 flex items-center mb-1">
                <h2 class="font-medium text-lg mb-01">{{ trans('cruds.agentStudent.title_singular') }} {{ trans('global.list') }}</h2>
            </div>
            <div class="col-lg-3 col-md-3 flex items-center mt-2 md:justify-end mb-1 md:mt-0">
                <label class="mb-0">{{ trans('cruds.agentDownline.fields.date_filter') }}</label>
            </div>
            <div class="col-md-3 col-lg-3 mt-2 md:mt-0">
                <select class="form-control select2" name="month_select" id="month_select">
                    @foreach($month_select as $index => $date_range)
                        <option value="{{ $date_range['from'] . '|' . $date_range['to'] }}">{{ $index == 0 ? trans('cruds.agentDownline.fields.all') : substr($date_range['from'], 0, 10) . '-' . substr($date_range['to'], 0, 10) }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="card-body">
            <table id="datatable-AgentStudent" class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-AgentStudent text-14-size">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.agentStudent.fields.tuition_package_efk') }}
                        </th>
                        <th>
                            {{ trans('cruds.agentStudent.fields.student_id') }}
                        </th>
                        <th>
                            {{ trans('cruds.agentStudent.fields.register_at') }}
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

        let dtDealerStudentInMonth = {
            buttons: dtButtons,
            processing: true,
            serverSide: true,
            retrieve: true,
            searching: false,
            lengthChange: false,
            info: false,
            scrollX: true,
            aaSorting: [],
            ajax: "{{ route('franchiser.franchiser-students.agentStudentInMonth') }}",
            columns: [
                { data: 'placeholder', name: 'placeholder' },
                { data: 'id', name: 'id' },
                { data: 'tuition_package_efk', name: 'tuition_package_efk' },
                { data: 'student_id', name: 'student_id' },
                { data: 'register_at', name: 'register_at' },
            ],
            orderCellsTop: true,
            order: [[ 1, 'desc' ]],
            pageLength: "{{ config('constants.dealer.minimum_temp') }}",
        };
        let tableInMonth = $('.datatable-DealerStudentInMonth').DataTable(dtDealerStudentInMonth);
        $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });

        var $month_select = $("#month_select")

        $month_select.on('change', function(){
            $data_range = $month_select.val().split("|")
            $data_from = $data_range[0]
            $data_to = $data_range[1]

            let dtOverrideGlobals = {
                buttons: dtButtons,
                processing: true,
                serverSide: true,
                retrieve: true,
                aaSorting: [],
                scrollX: true,
                ajax: {
                    headers: {
                        'x-csrf-token': _token
                    },
                    url: "{{ route('franchiser.franchiser-students.agentStudentByDate') }}",
                    type: 'POST',
                    data: {
                        from: $data_from,
                        to: $data_to,
                    },
                },
                columns: [
                    { data: 'placeholder', name: 'placeholder' },
                    { data: 'id', name: 'id' },
                    { data: 'tuition_package_efk', name: 'tuition_package_efk' },
                    { data: 'student_id', name: 'student_id' },
                    { data: 'register_at', name: 'register_at' },
                ],
                orderCellsTop: true,
                order: [[ 1, 'desc' ]],
                pageLength: 100,
            };

            if ($.fn.dataTable.isDataTable('#datatable-AgentStudent')) {
                $('#datatable-AgentStudent').DataTable().rows().remove();
                $('#datatable-AgentStudent').DataTable().destroy();
            }

            let table = $('#datatable-AgentStudent').DataTable(dtOverrideGlobals);
        }).trigger('change');
});

</script>
@endsection
