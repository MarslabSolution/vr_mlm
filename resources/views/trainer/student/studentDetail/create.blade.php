@extends('trainer.layout')
@section('body')
@if(!isset($student_detail) || $student_detail == null)
<?php $student_detail = [
    'student_name' => '',
    'chinese_name' => '',
    'student_email' => '',
    'nric_no' => '',
    'age' => '',
    'school_name' => '',
    'class_name' => '',
    'gender' => '',
    'is_handicapped' => '',
    'is_special' => '',
    'student_password' => '',
    'student_con_password' => '',
]; ?>
@endif
@if(!isset($home_address) || $home_address == null)
<?php $home_address = new App\Models\Address(); ?>
@endif
@if(!isset($mail_address) || $mail_address == null)
<?php $mail_address = new App\Models\Address(); ?>
@endif
<div class="content no-margin-left m-height" id="content-box">
    <div class="content-title">
        <div class="content-title-text">
            {{ trans('global.create') }} {{ trans('cruds.studentDetail.title_singular') }}
        </div>
    </div>

    <hr class="mb-0">

    <nav aria-label="breadcrumb" class="-intro-x xl:flex">
        <ol id="detailBreadcrumb" class="breadcrumb mb-0">
            <li id="mlmDetailBCBtn" class="breadcrumb-item"><a href="#">{{ trans('cruds.student.fields.step1') }}</a></li>
            <li id="studentDetailBCBtn" class="breadcrumb-item"><a href="#">{{ trans('cruds.student.fields.step2') }}</a></li>
        </ol>
    </nav>

    <div class="pl-2 pr-2">
        <div id="newStudentForm" style="display: none">
            <div class="form-group">
                <label class="required" for="student_name">{{ trans('cruds.studentDetail.fields.english_name') }}</label>
                <input class="form-control {{ $errors->has('student_name') ? 'is-invalid' : '' }}" type="text" name="student_name" id="student_name" value="{{ old('student_name', $student_detail['student_name']) }}" required>
                @if($errors->has('student_name'))
                <div class="invalid-feedback">
                    {{ $errors->first('student_name') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.studentDetail.fields.english_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="chinese_name">{{ trans('cruds.studentDetail.fields.chinese_name') }}</label>
                <input class="form-control {{ $errors->has('chinese_name') ? 'is-invalid' : '' }}" type="text" name="chinese_name" id="chinese_name" value="{{ old('chinese_name', $student_detail['chinese_name']) }}" required>
                @if($errors->has('chinese_name'))
                <div class="invalid-feedback">
                    {{ $errors->first('chinese_name') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.studentDetail.fields.chinese_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="student_email">{{ trans('cruds.studentDetail.fields.student_email') }}</label>
                <input class="form-control {{ $errors->has('student_email') ? 'is-invalid' : '' }}" type="text" name="student_email" id="student_email" value="{{ old('student_email', $student_detail['student_email']) }}" required>
                @if($errors->has('student_email'))
                <div class="invalid-feedback">
                    {{ $errors->first('student_email') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.studentDetail.fields.student_email_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="nric_no">{{ trans('cruds.studentDetail.fields.nric_no') }}</label>
                <input class="form-control {{ $errors->has('nric_no') ? 'is-invalid' : '' }}" type="text" name="nric_no" id="nric_no" value="{{ old('nric_no', $student_detail['nric_no']) }}" step="1" placeholder="{{trans('cruds.studentDetail.fields.nric_no_helper')}}" required>
                @if($errors->has('nric_no'))
                <div class="invalid-feedback">
                    {{ $errors->first('nric_no') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="required" for="age">{{ trans('cruds.studentDetail.fields.age') }}</label>
                <input class="form-control {{ $errors->has('age') ? 'is-invalid' : '' }}" type="number" name="age" id="age" value="{{ old('age', $student_detail['age']) }}" step="1" placeholder="{{trans('cruds.studentDetail.fields.age_helper')}}" required>
                @if($errors->has('age'))
                <div class="invalid-feedback">
                    {{ $errors->first('age') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="required" for="school_name">{{ trans('cruds.studentDetail.fields.school_name') }}</label>
                <input class="form-control {{ $errors->has('school_name') ? 'is-invalid' : '' }}" type="text" name="school_name" id="school_name" value="{{ old('school_name', $student_detail['school_name']) }}" step="1" placeholder="{{trans('cruds.studentDetail.fields.school_name_helper')}}" required>
                @if($errors->has('school_name'))
                <div class="invalid-feedback">
                    {{ $errors->first('school_name') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="required" for="class_name">{{ trans('cruds.studentDetail.fields.class_name') }}</label>
                <input class="form-control {{ $errors->has('class_name') ? 'is-invalid' : '' }}" type="text" name="class_name" id="class_name" value="{{ old('class_name', $student_detail['class_name']) }}" step="1" placeholder="{{trans('cruds.studentDetail.fields.class_name_helper')}}" required>
                @if($errors->has('class_name'))
                <div class="invalid-feedback">
                    {{ $errors->first('class_name') }}
                </div>
                @endif
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.studentDetail.fields.gender') }}</label>
                <select class="form-control {{ $errors->has('gender') ? 'is-invalid' : '' }}" name="gender" id="gender" required>
                    <option value disabled {{ old('gender', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(config('constants.gender_select') as $key => $label)
                    <option value="{{ $key }}" {{ old('gender', $student_detail['gender']) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('gender'))
                <div class="invalid-feedback">
                    {{ $errors->first('gender') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.studentDetail.fields.gender_helper') }}</span>
            </div>

            <div class="form-group">
                <div class="form-check {{ $errors->has('is_handicapped') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="is_handicapped" value="0">
                    <input class="form-check-input" type="checkbox" name="is_handicapped" id="is_handicapped" value="1" {{ old('is_handicapped', $student_detail['is_handicapped']) == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="is_handicapped">{{ trans('cruds.studentDetail.fields.is_handicapped') }}</label>
                </div>
                @if($errors->has('is_handicapped'))
                <div class="invalid-feedback">
                    {{ $errors->first('is_handicapped') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.studentDetail.fields.is_handicapped_helper') }}</span>
            </div>

            <div class="form-group">
                <div class="form-check {{ $errors->has('is_special') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="is_special" value="0">
                    <input class="form-check-input" type="checkbox" name="is_special" id="is_special" value="1" {{ old('is_special', $student_detail['is_special']) == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="is_special">{{ trans('cruds.studentDetail.fields.is_special') }}</label>
                </div>
                @if($errors->has('is_special'))
                <div class="invalid-feedback">
                    {{ $errors->first('is_special') }}
                </div>
                @endif
                <span class="help-block">{{ trans('cruds.studentDetail.fields.is_special_helper') }}</span>
            </div>
            
            <div class="form-group">
                <label class="required" for="password">{{ trans('cruds.studentDetail.fields.password') }}</label>
                <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password" name="password" id="password" value="{{ old('password', $student_detail['student_password']) }}" placeholder="{{trans('cruds.studentDetail.fields.password_helper')}}" required>
                @if($errors->has('password'))
                    <div class="invalid-feedback">
                        {{ $errors->first('password') }}
                    </div>
                @endif
            </div>

            <div class="form-group">
                <label class="required" for="con_password">{{ trans('cruds.studentDetail.fields.con_password') }}</label>
                <input class="form-control {{ $errors->has('con_password') ? 'is-invalid' : '' }}" type="password" name="con_password" id="con_password" value="{{ old('con_password', $student_detail['student_con_password']) }}" step="1" placeholder="{{trans('cruds.studentDetail.fields.con_password_helper')}}" required>
                @if($errors->has('con_password'))
                    <div class="invalid-feedback">
                        {{ $errors->first('con_password') }}
                    </div>
                @endif
            </div>
            
            <div class="form-group">
                <label for="home_address_id">{{ trans('cruds.studentDetail.fields.home_address') }}</label>
                <div class="card card-body ">
                    @component('components.addresses.fields',[
                    'prefix' => 'home_',
                    'required' => true,
                    'address' => $home_address,
                    'errors' => $errors,
                    ]) @endcomponent
                </div>
                <span class="help-block">{{ trans('cruds.studentDetail.fields.home_address_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="mail_address_id">{{ trans('cruds.studentDetail.fields.mail_address') }}</label>
                <div class="card card-body">
                    <span class="input-group ">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="checkbox" class="" name="is_same_with_home" id="is_same_with_home" value="1" {{ $mail_address == null ? 'checked' : '' }} aria-label="Radio button for following text input">
                            </div>
                        </div>
                        <div class="input-group-append">
                            <label class="input-group-text" for="is_same_with_home">{{ trans('cruds.studentDetail.fields.same_with_home_address') }}</label>
                        </div>
                    </span>
                    <div class="">
                        @component('components.addresses.fields',[
                        'prefix' => 'mail_',
                        'required' => true,
                        'address' => $mail_address,
                        'errors' => $errors,
                        ]) @endcomponent
                    </div>
                </div>

                <span class="help-block">{{ trans('cruds.studentDetail.fields.mail_address_helper') }}</span>
            </div>

            <div class="form-group">
                <button type="submit" id="submitBtn" class="btn btn-danger">
                    {{ trans('cruds.studentDetail.fields.next') }}
                </button>
            </div>
        </div>

        <div id="oldStudentForm" style="display: none">
            <form method="POST" action="{{ route('trainer.students.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="student_type" value="{{ $student_type }}">
                <input type="hidden" name="mlm_detail" value="{{ $mlm_detail }}">
                <div class="form-group">
                    <label class="required" for="student_detail_id">{{ trans('cruds.studentDetail.fields.student') }}</label>
                    <select class="form-control select2 {{ $errors->has('student_detail_id') ? 'is-invalid' : '' }}" name="student_detail_id" id="student_detail_id" required>
                    </select>
                    @if($errors->has('student_detail_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('student_detail_id') }}
                    </div>
                    @endif
                    <span class="help-block">{{ trans('cruds.studentDetail.fields.student_helper') }}</span>
                </div>

                <div class="form-group">
                    <button type="submit" id="submitOldBtn" class="btn btn-danger">
                        {{ trans('cruds.studentDetail.fields.next') }}
                    </button>
                </div>
            </form>
        </div>

    </div>
</div>
@endsection

@section('scripts')
@parent
<!-- use for control form -->
<script>
    const $student_type = "{{ $student_type }}";

    if ($student_type == "newStudent") {
        $("#newStudentForm").show();
        $("#oldStudentForm").hide();
    } else {
        $("#newStudentForm").hide();
        $("#oldStudentForm").show();
    }
</script>

<!-- use for breakcrum -->
<script>
    $("#mlmDetailBCBtn").on('click', function(e) {
        let $urlPath = window.location.pathname.split("/")

        let $subStrIndex = $urlPath.indexOf('students') + 1;

        for (let $i = $subStrIndex; $i < $urlPath.length; $i++) {
            delete $urlPath[$i];
        }

        $urlPath.length = $subStrIndex

        window.location = $urlPath.join('/');
    });
</script>

<!-- use for mail address -->
<script>
    let $isSameCbk = $('#is_same_with_home');

    if ($isSameCbk.prop('checked')) {
        $('#mail_address_id').prop('disabled', true);
        whenChecked();
    } else {
        whenRemoveChecked();
    }

    $isSameCbk.on('change', (e) => {
        if (e.target.checked) {
            whenChecked();
            $isSameCbk.attr('checked', true);
        } else {
            whenRemoveChecked();
            $isSameCbk.attr('checked', false);
        }
    });

    function whenChecked() {
        $('#mail_address_line_1').attr('disabled', true);
        $('#mail_address_line_2').attr('disabled', true);
        $('#mail_city').attr('disabled', true);
        $('#mail_state').attr('disabled', true);
        $('#mail_postal_code').attr('disabled', true);
        $('#mail_country').attr('disabled', true);
        $('#mail_phone').attr('disabled', true);

        $('#mail_address_line_1').attr('required', false);
        $('#mail_city').attr('required', false);
        $('#mail_state').attr('required', false);
        $('#mail_postal_code').attr('required', false);
        $('#mail_country').attr('required', false);
        $('#mail_phone').attr('required', false);

        $('#mail_address_line_1_lbl').removeClass('required');
        $('#mail_city_lbl').removeClass('required');
        $('#mail_state_lbl').removeClass('required');
        $('#mail_postal_code_lbl').removeClass('required');
        $('#mail_country_lbl').removeClass('required');
        $('#mail_phone_lbl').removeClass('required');
    }

    function whenRemoveChecked() {
        $('#mail_address_line_1').attr('disabled', false);
        $('#mail_address_line_2').attr('disabled', false);
        $('#mail_city').attr('disabled', false);
        $('#mail_state').attr('disabled', false);
        $('#mail_postal_code').attr('disabled', false);
        $('#mail_country').attr('disabled', false);
        $('#mail_phone').attr('disabled', false);

        $('#mail_address_line_1').attr('required', true);
        $('#mail_city').attr('required', true);
        $('#mail_state').attr('required', true);
        $('#mail_postal_code').attr('required', true);
        $('#mail_country').attr('required', true);
        $('#mail_phone').attr('required', true);

        $('#mail_address_line_1_lbl').addClass('required');
        $('#mail_city_lbl').addClass('required');
        $('#mail_state_lbl').addClass('required');
        $('#mail_postal_code_lbl').addClass('required');
        $('#mail_country_lbl').addClass('required');
        $('#mail_phone_lbl').addClass('required');
    }
</script>

<!-- use for old student selection -->
<script>
    $(function() {
        var $student_select = $("#student_detail_id")

        $student_select.select2({
            ajax: {
                url: "{{ route('trainer.students.search-student-detail') }}",
                method: "POST",
                headers: {
                    'x-csrf-token': _token
                },
                dataType: 'json',
                cache: true,
                delay: 250,
                data: function(params) {
                    return {
                        search: params.term,
                        page: params.page || 1,
                    }
                },
                processResults: function(data, params) {
                    return {
                        results: data.results,
                        pagination: params.page || 1,
                    };
                }
            },
            minimumInputLength: 1,
            placeholder: "{{ trans('cruds.studentDetail.fields.select_student') }}",
        });
    });
</script>

<!-- use for submit to next form -->
<script>
    $("#submitBtn").on('click', function(e) {
        $home_address = {};

        $home_address.address_line_1 = $('#home_address_line_1').val();
        $home_address.address_line_2 = $('#home_address_line_2').val();
        $home_address.city = $('#home_city').val();
        $home_address.state = $('#home_state').val();
        $home_address.postal_code = $('#home_postal_code').val();
        $home_address.country = $('#home_country').val();
        $home_address.phone = $('#home_phone').val();

        $mail_address = null;

        if (!$isSameCbk.is(':checked')) {
            $mail_address = {};

            $mail_address.address_line_1 = $('#mail_address_line_1').val();
            $mail_address.address_line_2 = $('#mail_address_line_2').val();
            $mail_address.city = $('#mail_city').val();
            $mail_address.state = $('#mail_state').val();
            $mail_address.postal_code = $('#mail_postal_code').val();
            $mail_address.country = $('#mail_country').val();
            $mail_address.phone = $('#mail_phone').val();
        }

        // create user student details
        $student_detail = {};

        $student_detail.student_name = $('#student_name').val();
        $student_detail.chinese_name = $('#chinese_name').val();
        $student_detail.student_email = $('#student_email').val();
        $student_detail.nric_no = $('#nric_no').val();
        $student_detail.age = $('#age').val();
        $student_detail.school_name = $('#school_name').val();
        $student_detail.class_name = $('#class_name').val();
        $student_detail.gender = $('#gender').val();
        $student_detail.is_handicapped = $('#is_handicapped').val();
        $student_detail.is_special = $('#is_special').val();
        $student_detail.student_password = $('#password').val();
        $student_detail.student_con_password = $('#con_password').val();

        let $data_resource = JSON.stringify({
            'student_detail': $student_detail,
            'home_address': $home_address,
            'mail_address': $mail_address,
        });

        let $urlPath = window.location.pathname.split("/")

        let $subStrIndex = $urlPath.indexOf('students') + 3

        $urlPath[$subStrIndex] = $data_resource

        window.location = $urlPath.join("/");
    });
</script>
@endsection