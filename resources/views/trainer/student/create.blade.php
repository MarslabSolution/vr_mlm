@extends('trainer.layout')
@section('body')
<div class="content no-margin-left m-height" id="content-box">
    <div class="content-title">
        <div class="content-title-text">
            {{ trans('global.create') }} {{ trans('cruds.student.title_singular') }}
        </div>
    </div>

    <hr class="mb-0">

    <nav aria-label="breadcrumb" class="-intro-x xl:flex">
        <ol id="detailBreadcrumb" class="breadcrumb mb-0">
            <li id="mlmDetailBCBtn" class="breadcrumb-item"><a href="#">{{ trans('cruds.student.fields.step1') }}</a></li>
        </ol>
    </nav>

    <div class="pl-2 pr-2">
        <div class="form-group">
            <label class="required" for="tuition_package_efk">
                {{ trans('cruds.packagesCommission.fields.tuition_package_efk') }}
            </label>
            <div class="progress">
                <div class="progress-bar progress-bar-animated progress-bar-primary progress-bar-striped w-90"
                    role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
                    {{ trans('global.loading') }}
                </div>
            </div>
            <select style="display: none;"
                class="form-control select2 {{ $errors->has('tuition_package_efk') ? 'is-invalid' : '' }}"
                name="tuition_package_efk" id="tuition_package_efk" step="1">
                {{-- JS populate the package options --}}
            </select>

            @if($errors->has('tuition_package_efk'))
            <div class="invalid-feedback">
                {{ $errors->first('tuition_package_efk') }}
            </div>
            @else
            <div id="invalid-feedback-tuition_package_efk" style="display: none;" for="tuition_package_efk">
            </div>
            @endif
            <span class="help-block">
                {{ trans('cruds.packagesCommission.fields.tuition_package_efk_helper') }}
            </span>
        </div>
        
        <div class="form-group">
            <label class="required" for="referral_id">
                {{ trans('cruds.student.fields.referral_id') }}
            </label>
            <select class="form-control select2 {{ $errors->has('referral_id') ? 'is-invalid' : '' }}"
                name="referral_id" id="referral_id" data-tags="true" required>
                <option value='' disabled>{{ trans('global.pleaseSelect') }}</option>
                @foreach($agents as $id => $agent)
                <option value="{{ $id }}" {{ old('referral_id') == $id ? 'selected' : '' }}>
                    {{ $agent }}
                </option>
                @endforeach
            </select>
            @if($errors->has('referral_id'))
            <div class="invalid-feedback">
                {{ $errors->first('referral_id') }}
            </div>
            @endif
            <span class="help-block">
                {{ trans('cruds.student.fields.referral_id_helper') }}
            </span>
        </div>

        <div>
            <button id="nextToNewStudentBtn" class="btn btn-danger mr-2 js-studentSelectBtn">
                {{ trans('cruds.student.fields.new_student') }}
            </button>

            @if(false)
            {{ trans('cruds.student.fields.or') }}
            
            <button id="nextToOldStudentBtn" class="btn btn-danger mx-2 js-studentSelectBtn">
                {{ trans('cruds.student.fields.existing_student') }}
            </button>
            @endif
        </div>
    </div>
</div>
@endsection

@section('footer-scripts')
@parent
<!-- use for fetch tuition package -->
<script>
    const tuitionPackageEfk = 'tuition_package_efk';
    $.ajax({
        method: 'GET',
        headers: {
            'Authorization': 'Bearer {{ access_token() }}',
            'X-VRDRUM-USER': '{{ auth()->user()->email }}',
            'X-CSRF-TOKEN': '{{ csrf_token() }}',
            'Accept': 'application/json',
        },
        url: "{{ settings('main_system_domain', '', 'api') . settings('api_version', '', 'api') . settings('tuition_packages', '', 'api') }} ",
        success: (response) => {
            const tuitionPackages = response.data;
            let options = '';
            if (tuitionPackages != null && tuitionPackages.length > 0) {
                isDisabled =  tuitionPackages.length > 0 ? 'disabled' : '';
                options += "<option value='' disabled>{{ trans('global.pleaseSelect') }}</option>";
                for (const package of tuitionPackages) {
                    var selected = tuitionPackages.length == 1 ? 'selected' : '';
                    if(selected) {
                        $('#tuition_package_price').val(package.price);
                    }
                    options += `<option value="${package.id}" ${selected} title="{{ trans("cruds.agentPlan.fields.price") }}: ${package.price}"> ${package.name} </option>`;
                }
                if(isDisabled) {
                    $('#' + tuitionPackageEfk).prop('required',true);
                }
                $(`#${tuitionPackageEfk}`).html(options);
            }
        },
        error: (xhr, response, thrownError) =>{
            console.error(xhr);
            $('#invalid-feedback-tuition_package_efk').html(
                `
                <div class="row"><div class="col">{{ trans("global.status_code") }}</div><div class="col">:${xhr.status} </div></div>
                <div class="row"><div class="col">{{ trans("global.messages") }}</div><div class="col"> : ${xhr.statusText} </div></div>
                `
            ).show();
            $('.progress').slideUp();
        }
    }).done(() => {
        $('.progress').slideUp();
        $(`#${tuitionPackageEfk}`).select2({
            templateResult: (option) => {
                return $(
                    `<div><strong>${option.text}</strong></div><div>${option.title}</div>`
                );
            },
            //allowClear: true,
        });
    });
    $(`#${tuitionPackageEfk}`).on('select2:select', function(e) {
        $('#tuition_package_price').val($(`#${tuitionPackageEfk}`).find(':selected').attr('title').split(':')[1].trim());
    });
</script>

<!-- use for controler breadcrum step -->
<script>
    $(".js-studentSelectBtn").on('click', function(e) {
        showStudentStep($(this).attr('id'))
    })

    function showStudentStep(btnId){
        $tuition_package_efk = $("#tuition_package_efk").val();
        $referral_id = $("#referral_id").val();

        $data_resource = JSON.stringify({
            'tuition_package_id': $tuition_package_efk,
            'referral_id': $referral_id,
        });

        let $urlPath = window.location.pathname.replace('#', '').split("/")

        let $subStrIndex = $urlPath.indexOf('students') + 1

        $urlPath[$subStrIndex] = "studentType=" 
        $urlPath[$subStrIndex + 1] = $data_resource

        if(btnId == "nextToNewStudentBtn"){
            $urlPath[$subStrIndex] += "newStudent";
        }else{
            $urlPath[$subStrIndex] += "oldStudent";
        }

        window.location = $urlPath.join("/");
    }

</script>
@endsection 
