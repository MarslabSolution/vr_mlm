@extends('trainer.layout')
@section('body')
@if(!isset($guardian_detail))
<?php $guardian_detail = null ?>
@endif
<div class="content no-margin-left m-height" id="content-box">
    <div class="content-title">
        <div class="content-title-text">
        {{ trans('global.create') }} {{ trans('cruds.studentDetail.title_singular') }}
        </div>
    </div>

    <hr class="mb-0"/>
    
    <nav aria-label="breadcrumb" class="-intro-x xl:flex">
        <ol id="detailBreadcrumb" class="breadcrumb">
            <li id="mlmDetailBCBtn" class="breadcrumb-item"><a href="#">{{ trans('cruds.student.fields.step1') }}</a></li>
            <li id="studentDetailBCBtn" class="breadcrumb-item"><a href="#">{{ trans('cruds.student.fields.step2') }}</a></li>
            <li id="guardianDetailBCBtn" class="breadcrumb-item"><a href="#">{{ trans('cruds.student.fields.step3') }}</a></li>
        </ol>
    </nav>

    <div class="pl-2 pr-2">
        <form method="POST" action="{{ route('trainer.students.store') }}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="student_type" value="{{ $student_type }}">
            <input type="hidden" name="mlm_detail" value="{{ $mlm_detail }}">
            <input type="hidden" name="student_detail" value="{{ $student_detail }}">
            @for($i = 0; $i < 2; $i++)

                @if($i != 0)
                <hr />
                @endif
                <h5 class="my-2">{{ trans('cruds.studentDetail.fields.guardian') }} {{ $i + 1 }}</h5>
                <hr class="mb-0 pb-0"/>

                <div id="guardianForm-{{ $i }}" class="my-4">
                    <input type="hidden" name="total_guardian[]" value="{{ $i }}">
                    <div class="form-group">
                        <label class="required" for="parent_name_{{ $i }}">{{ trans('cruds.studentDetail.fields.english_name') }}</label>
                        <input class="form-control {{ $errors->has('parent_name_'. $i) ? 'is-invalid' : '' }}" type="text" name="parent_name_{{ $i }}" id="parent_name_{{ $i }}" value="{{ old('parent_name_'. $i, $guardian_detail['parent_name_'. $i] ?? '') }}" step="1" placeholder="{{trans('cruds.studentDetail.fields.english_name_helper')}}">
                        @if($errors->has('parent_name_'. $i))
                            <div class="invalid-feedback">
                                {{ $errors->first('parent_name_'. $i) }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="required" for="parent_chinese_name_{{ $i }}">{{ trans('cruds.studentDetail.fields.chinese_name') }}</label>
                        <input class="form-control {{ $errors->has('parent_chinese_name_'. $i) ? 'is-invalid' : '' }}" type="text" name="parent_chinese_name_{{ $i }}" id="parent_chinese_name_{{ $i }}" value="{{ old('parent_chinese_name_'. $i, $guardian_detail['parent_chinese_name_'. $i] ?? '') }}" step="1" placeholder="{{trans('cruds.studentDetail.fields.chinese_name_helper')}}">
                        @if($errors->has('parent_chinese_name_'. $i))
                            <div class="invalid-feedback">
                                {{ $errors->first('parent_chinese_name_'. $i) }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="required" for="parent_email_{{ $i }}">{{ trans('cruds.studentDetail.fields.guardian_email') }}</label>
                        <input class="form-control {{ $errors->has('parent_email_'. $i) ? 'is-invalid' : '' }}" type="text" name="parent_email_{{ $i }}" id="parent_email_{{ $i }}" value="{{ old('parent_email_'. $i, $guardian_detail['parent_email_'. $i] ?? '') }}" step="1" placeholder="{{trans('cruds.studentDetail.fields.guardian_email_helper')}}">
                        @if($errors->has('parent_email_'. $i))
                            <div class="invalid-feedback">
                                {{ $errors->first('parent_email_'. $i) }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="required" for="parent_nric_no_{{ $i }}">{{ trans('cruds.studentDetail.fields.nric_no') }}</label>
                        <input class="form-control {{ $errors->has('parent_nric_no_'. $i) ? 'is-invalid' : '' }}" type="text" name="parent_nric_no_{{ $i }}" id="parent_nric_no_{{ $i }}" value="{{ old('parent_nric_no_'. $i, $guardian_detail['parent_nric_no_'. $i] ?? '') }}" step="1" placeholder="{{trans('cruds.studentDetail.fields.nric_no_helper')}}">
                        @if($errors->has('parent_nric_no_'. $i))
                            <div class="invalid-feedback">
                                {{ $errors->first('parent_nric_no_'. $i) }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="required" for="parent_occupation_{{ $i }}">{{ trans('cruds.studentDetail.fields.occupation') }}</label>
                        <input class="form-control {{ $errors->has('parent_occupation_'. $i) ? 'is-invalid' : '' }}" type="text" name="parent_occupation_{{ $i }}" id="parent_occupation_{{ $i }}" value="{{ old('parent_occupation_'. $i, $guardian_detail['parent_occupation_'. $i] ?? '') }}" step="1" placeholder="{{trans('cruds.studentDetail.fields.occupation_helper')}}">
                        @if($errors->has('parent_occupation_'. $i))
                            <div class="invalid-feedback">
                                {{ $errors->first('parent_occupation_'. $i) }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="required" for="parent_relationship_{{ $i }}">{{ trans('cruds.studentDetail.fields.relationship') }}</label>
                        <select class="form-control relationship {{ $errors->has('parent_relationship_'. $i) ? 'is-invalid' : '' }}" name="parent_relationship_{{ $i }}" id="parent_relationship_{{ $i }}">
                            @foreach(config('constants.gradient_relstionship_select') as $key => $label)
                            <option value="{{ $key }}" {{ old('parent_relationship_'. $i, $guardian_detail['parent_relationship_'. $i] ?? '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="home_address_id">{{ trans('cruds.studentDetail.fields.address') }}</label>
                        <div class=" card card-body">
                        @component('components.addresses.fields',[
                            'prefix' => 'parent_'. $i .'_',
                            'required' => true,
                            'address' => $guardian_detail ? $guardian_detail['parent_'. $i .'_address'] : null,
                            'errors' => $errors,
                        ]) @endcomponent
                        </div>
                        <span class="help-block">{{ trans('cruds.studentDetail.fields.address_helper') }}</span>
                    </div>
                </div>
            @endfor

            <div class="form-group">
                <button type="submit" class="btn btn-danger">
                    {{ trans('cruds.studentDetail.fields.submit_student_detail_form') }}
                </button>
            </div>  
        </form>
    </div>
</div>
@endsection

@section('scripts')
@parent
<script>
    $("#mlmDetailBCBtn").on('click', function(e){
        let $urlPath = window.location.pathname.split("/")

        let $subStrIndex = $urlPath.indexOf('students')+1

        for(let $i = $subStrIndex; $i < $urlPath.length; $i++){
            delete $urlPath[$i];
        }

        $urlPath.length = $subStrIndex;

        window.location = $urlPath.join("/");
    })

    $("#studentDetailBCBtn").on('click', function(e){
        history.back()
    })

    $('#is_same_with_home').on('change', (e) => {
        if(e.target.checked){
            $('#mail_address_line_1').attr('disabled', true);
            $('#mail_address_line_2').attr('disabled', true);
            $('#mail_city').attr('disabled', true);
            $('#mail_state').attr('disabled', true);
            $('#mail_postal_code').attr('disabled', true);
            $('#mail_country').attr('disabled', true);
            $('#mail_phone').attr('disabled', true);

            $('#mail_address_line_1').attr('required', false);
            $('#mail_city').attr('required', false);
            $('#mail_state').attr('required', false);
            $('#mail_postal_code').attr('required', false);
            $('#mail_country').attr('required', false);
            $('#mail_phone').attr('required', false);

            $('#mail_address_line_1_lbl').removeClass('required');
            $('#mail_city_lbl').removeClass('required');
            $('#mail_state_lbl').removeClass('required');
            $('#mail_postal_code_lbl').removeClass('required');
            $('#mail_country_lbl').removeClass('required');
            $('#mail_phone_lbl').removeClass('required');
        }else{
            $('#mail_address_line_1').attr('disabled', false);
            $('#mail_address_line_2').attr('disabled', false);
            $('#mail_city').attr('disabled', false);
            $('#mail_state').attr('disabled', false);
            $('#mail_postal_code').attr('disabled', false);
            $('#mail_country').attr('disabled', false);
            $('#mail_phone').attr('disabled', false);

            $('#mail_address_line_1').attr('required', true);
            $('#mail_city').attr('required', true);
            $('#mail_state').attr('required', true);
            $('#mail_postal_code').attr('required', true);
            $('#mail_country').attr('required', true);
            $('#mail_phone').attr('required', true);

            $('#mail_address_line_1_lbl').addClass('required');
            $('#mail_city_lbl').addClass('required');
            $('#mail_state_lbl').addClass('required');
            $('#mail_postal_code_lbl').addClass('required');
            $('#mail_country_lbl').addClass('required');
            $('#mail_phone_lbl').addClass('required');
        }
    })
</script>
@endsection