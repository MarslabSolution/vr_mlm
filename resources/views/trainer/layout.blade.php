@extends('layouts.master')

@section('styles')
<style>
    .progress-bar.active,
    .progress.active .progress-bar {
        -webkit-animation: progress-bar-stripes 1s linear infinite;
        -o-animation: progress-bar-stripes 1s linear infinite;
        animation: progress-bar-stripes 1s linear infinite;
    }

    .w-90 {
        width: 90% !important;
    }

    .progress {
        height: 20px;
        margin-bottom: 20px;
        overflow: hidden;
        background-color: #f5f5f5;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 2px rgb(0 0 0 / 10%);
        box-shadow: inset 0 1px 2px rgb(0 0 0 / 10%);
    }

    #invalid-feedback-tuition_package_efk {
        color: var(--ck-color-base-error);
    }
</style>
@endsection

@section('content')


<head>
<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/icons/android-chrome-192x192.png') }}">
<link rel="icon" type="image/png" sizes="512x512"  href="{{ asset('images/icons/android-chrome-512x512.png') }}">
<link rel="icon" href="{{ asset('images/icons/favicon.ico') }}" sizes="any">
<link rel="icon" href="{{ asset('images/icons/Logo.svg') }}" type="image/svg+xml">
</head>

<div class="c-wrapper no-margin-right">
    <header class="top-bar top-bar-padding">
        <a href="#">
            <img src="{{ asset('images/VrWhite.png') }}" alt="" class="logo-svg  left-logo">
        </a>

        <ul class="c-header-nav ml-auto text-slate-500">
            @if(count(config('panel.available_languages', [])) > 1)
            <li class="c-header-nav-item dropdown ">
                <a class="c-header-nav-link color-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    {{ strtoupper(app()->getLocale()) }}
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    @foreach(config('panel.available_languages') as $langLocale => $langName)
                    <a class="dropdown-item" href="{{ url()->current() }}?change_language={{ $langLocale }}">{{
                            strtoupper($langLocale) }} ({{ $langName }})</a>
                    @endforeach
                </div>
            </li>
            @endif

            @include('ssoclient::partials.notification')

            <li class="c-header-nav-item dropdown">
                <div class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="h-10">
                        <span class="material-symbols-outlined icon-user color-white">account_circle</span>
                    </div>
                    <div class="hidden md:block ml-3">
                        <div class="max-w-[7rem] truncate font-medium color-white">{{auth()->user()->name }}</div>
                        <div class="text-xs color-white">{{auth()->user()->roles()->first()->title }}</div>
                    </div>
                </div>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="{{ route('trainer.profile.edit') }}">
                        <i class="fas fa-key"></i>
                        <span class="pl-2">{{ trans('global.change_password') }}</span>
                    </a>
                    <a href="#" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <i class="fas fa-sign-out-alt"></i>
                        <span class="logout-text pl-2">Log Out</span>
                    </a>
                </div>
            </li>

        </ul>
    </header>


    @include('layouts.cBody')

    <div class="fixed-bottom pt-2 px-2">
        <ul class="c-bottom-nav w-100 justify-content-around">
            <li class="c-bottom-nav-item {{ request()->is('trainer') ? 'c-active' : '' }}">
                <a class="c-bottom-nav-link" href="{{ route('trainer.home') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="d-flex flex-column justify-content-center align-items-center text-center">
                        <i class="fas fa-home fa-2x"></i>
                        <span class="c-bottom-nav-item-text">Dashboard</span>
                    </div>
                </a>
            </li>

            <li class="c-bottom-nav-item {{ request()->is('trainer/students') || request()->is('trainer/students/*') ? 'c-active' : '' }}">
                <a class="c-bottom-nav-link" href="{{ route('trainer.students.create') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="d-flex flex-column justify-content-center align-items-center text-center">
                        <i class="fas fa-address-book fa-2x"></i>
                        <span class="c-bottom-nav-item-text">{{ trans('global.register') }}</span>
                    </div>
                </a>
            </li>

            <li class="c-bottom-nav-item {{ request()->is('trainer/profile') || request()->is('trainer/profile/*') ? 'c-active' : '' }}">
                <a class="c-bottom-nav-link" href="{{ route('trainer.profile.edit') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="d-flex flex-column justify-content-center align-items-center text-center">
                        <i class="fas fa-user-circle fa-2x"></i>
                        <span class="c-bottom-nav-item-text">Profile</span>
                    </div>
                </a>
            </li>
        </ul>
    </div>

    <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

</div>
@endsection

@section('footer-scripts')
@endsection