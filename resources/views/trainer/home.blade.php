@extends('trainer.layout')

@section('body')
<div class="content ml-0" id="content-box">
<div class="content-title ">
    <div class="content-title-text">
         {{ trans('cruds.trainer.title_singular') }}
        </div>
    </div>

    <hr>

    <div class="card-body">
            <div class="alert text-center mobile-spacing-bottom">
                <h2 class="text-xl font-semibold truncate mr-5">{{ trans('cruds.trainer.kpi_current_month') }}</h2>
                <div class="basic-progressbar">
                    <div class="progress">
                        <div class="progress-bar w-{{ ($current_month_api/settings('trainner_student_kpi', 1, 'condition')) * 100 }}" role="progressbar">

                        </div>
                    </div>
                    <div class="d-flex justify-content-center pt-1">
                        <h5>{{ $current_month_api }}</h5>
                        <h5 class="ml-2">/ {{ settings('trainner_student_kpi', '', 'condition') }}</h5>
                    </div>
                </div>
            </div>

    </div>
</div>
@endsection