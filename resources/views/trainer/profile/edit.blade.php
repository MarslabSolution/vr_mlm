@extends('trainer.layout')

@section('styles')
<style>
    @media screen and (min-width: 770px) {
        .mb-3{
            padding-top:30px;
        }
        .btn-layout {
            margin:auto;
            display:block;
            width: 300px !important;
        }
    }
    .px-5{
        padding-left: 0px !important;
        padding-right: 0px !important;
    }
    .mr-52{
        margin-right: 0rem !important;
        margin-left: 1.25rem;
    }
    .w-9{
        width: 2rem !important;
    }
    .sm\:block{
        display: none;
    }

</style>
@endsection

@section('body')
<div class="content ml-0" id="content-box">
    <div class="row">
        <div class="col-md-6">
            <div class="intro-y box mt-4">
                <div class="flex flex-col sm:flex-row p-01 border-b border-slate-200/60">
                    <h2 class="font-semibold text-xl mb-0">{{ trans('global.my_profile') }}</h2>
                </div>
                <div class="card-body">
                    <form class="spacing-bottom-0" method="POST" action="{{ route('trainer.password.updateProfile') }}">
                        @csrf
                        <div class="form-group spacing-top-0">
                            <label class="required" for="name">{{ trans('cruds.fields.name') }}</label>
                            <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text"
                                name="name" id="name" value="{{ old('name', auth()->user()->name) }}" required>
                            @if($errors->has('name'))
                            <div class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="required" for="title">{{ trans('cruds.fields.email') }}</label>
                            <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text"
                                name="email" id="email" value="{{ old('email', auth()->user()->email) }}" required>
                            @if($errors->has('email'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group spacing-bottom-0">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="intro-y box mt-4">
                <div class="flex flex-col sm:flex-row p-01 border-b border-slate-200/60">
                    <h2 class="font-semibold text-xl mb-0">{{ trans('global.change_password') }}</h2>
                </div>

                <div class="card-body">
                    <form class="spacing-bottom-0" method="POST" action="{{ route('trainer.password.update') }}">
                        @csrf
                        <div class="form-group spacing-top-0">
                            <label class="required" for="title">New {{ trans('cruds.user.fields.password')
                                }}</label>
                            <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password"
                                name="password" id="password" required>
                            @if($errors->has('password'))
                            <div class="invalid-feedback">
                                {{ $errors->first('password') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="required" for="title">Repeat New {{ trans('cruds.user.fields.password')
                                }}</label>
                            <input class="form-control" type="password" name="password_confirmation"
                                id="password_confirmation" required>
                        </div>
                        <div class="form-group spacing-bottom-0">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!--name and password-->

    <div class="row">
        <div class="intro-y col-md-6 xl:mt-10 lg:mt-10 md:mt-10 sm:mt-6 xs:mt-6 xl:mb-0 lg:mb-0 md:mb-0 sm:mb-0 xs:mb-0">
            <div class="box">
                <div class="flex flex-col sm:flex-row p-01 border-b border-slate-200/60">
                    <h2 class="font-semibold text-xl mb-0">{{ trans('global.delete_account') }}</h2>
                </div>

                <div class="card-body">
                    <form class="spacing-bottom-0" method="POST" action="{{ route('trainer.password.destroyProfile') }}"
                        onsubmit="return prompt('{{ __('global.delete_account_warning') }}') == '{{ auth()->user()->email }}'">
                        @csrf
                        <div class="form-group spacing-top-0 spacing-bottom-0">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.delete') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @if(Route::has('trainer.password.toggleTwoFactor'))
        <div class="intro-y col-md-6 xl:mt-10 lg:mt-10 md:mt-10 sm:mt-6 xs:mt-6 sm:mb-0 xs:mb-0">
            <div class="box">
                <div class="flex flex-col sm:flex-row p-01 border-b border-slate-200/60">
                    <h2 class="font-semibold text-xl mb-0">{{ trans('global.two_factor.title') }}</h2>
                </div>

                <div class="card-body">
                    <form class="spacing-bottom-0" method="POST" action="{{ route('trainer.password.toggleTwoFactor') }}">
                        @csrf
                        <div class="form-group spacing-top-0 spacing-bottom-0">
                            <button class="btn btn-danger" type="submit">
                                {{ auth()->user()->two_factor ? trans('global.two_factor.disable') :
                                trans('global.two_factor.enable') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
    </div><!--delete acc and Two factor authentication -->

    <div class="row">
        <div class="intro-y col-md-6 xl:mt-10 lg:mt-10 md:mt-10 sm:mt-6 xs:mt-6 xl:mb-6 lg:mb-6 md:mb-6 sm:mb-6 xs:mb-6">
            <div class="box">
                <div class="flex flex-col sm:flex-row p-01">
                    <h2 class="font-semibold text-xl flex items-center mb-0">{{ trans('global.colorScheme')}}</h2>
                    @include('admin.settings.general.mainSwitcher')
                </div>
            </div>
        </div>
    </div>

    <div clas="row ">
        <div class="col">
            <form id="logoutform" action="{{ route('logout') }}" method="POST" class="mb-3 mt-3">
                @csrf
                <button type="submit" class="btn btn-danger btn-layout  m-b-10 m-l-5 w-100 ">{{ trans('global.logout') }}</button>
            </form>
        </div>
    </div><!--log out-->

</div>
@endsection

