<!DOCTYPE html>
<html class="{{ $color_scheme != 'default' ? ' ' . $color_scheme : '' }}">

<head>
    @include('layouts.meta')
    <title>
      @if(View::hasSection('page_title'))
      @yield('page_title') - {{ settings('site_name', trans('panel.site_title')) }}
      @elseif(View::hasSection('title'))
      @yield('title')
      @else {{ settings('site_name',trans('panel.site_title')) }}
      @endif
    </title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Symbols+Outlined"rel="stylesheet">
    @if(View::hasSection('canonical_url'))
        <link rel="canonical" href="@yield('canonical_url')" />
    @endif
    @yield('dns_prefetch')
    @include('layouts.styles')
    @include('layouts.headScripts')
    @yield('meta-schema')
</head>

<body class="c-app">
    @yield('content')
    @include('layouts.footer')
</body>
</html>
