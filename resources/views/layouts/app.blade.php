<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
      @if(View::hasSection('page_title'))
      @yield('page_title') - {{ settings('site_name', trans('panel.site_title')) }}
      @elseif(View::hasSection('title'))
      @yield('title')
      @else {{ settings('site_name',trans('panel.site_title')) }}
      @endif
    </title>
    <meta name="description" content="@yield('page_description')" />
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/icons/android-chrome-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="512x512"  href="{{ asset('images/icons/android-chrome-512x512.png') }}">
    <link rel="icon" href="{{ asset('images/icons/favicon.ico') }}" sizes="any">
    <link rel="icon" href="{{ asset('images/icons/Logo.svg') }}" type="image/svg+xml">
    <link rel="manifest" href="{{ asset('manifest.webmanifest.json') }}">
    @yield('canonical')
    @yield('dns_prefetch')
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://unpkg.com/@coreui/coreui@3.2/dist/css/coreui.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/theme_app.css') }}" rel="stylesheet" />
    @yield('styles')
</head>

<body class="header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden login-page">
    <div class="c-app flex-row align-items-center login_bg">
        <div id="overlay"></div>
        <div class="container">
            <div class="login_img mx-4 mb-4">
                <a class="" href="{{ route('login') }}"> 
                <img src="../images/VrWhite.png" alt="logo_white" class="ml-auto mr-auto" width="150"></a>
            </div>
            @yield("content")
        </div>
    </div>
    @yield('scripts')
</body>

</html>
