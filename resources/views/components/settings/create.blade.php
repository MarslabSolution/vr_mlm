<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('global.create').' '.trans('settings.'.$group.'.title').' '.trans('cruds.setting.title') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.settings.store", ['group' => $group]) }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans("cruds.fields.name") }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <label for="value">{{ trans('settings.global.value') }}</label>
                <textarea class="form-control {{ $errors->has('value') ? 'is-invalid' : '' }}" type="text" name="value" id="value" required>{{ old('value', '') }}</textarea>
                @if($errors->has('value'))
                    <div class="invalid-feedback">
                        {{ $errors->first('value') }}
                    </div>
                @endif
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>