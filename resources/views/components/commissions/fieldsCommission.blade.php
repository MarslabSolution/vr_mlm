<div class="form-group">
    <label class="required" for="commissions[{{$level}}]">
    @if($level < 1)
    {{ trans('cruds.commission.fields.commission_current_level'); }}
    @elseif(isset($upline) && $upline == true)
    {{  sprintf(trans('cruds.commission.fields.commission_up_x_level'), $level); }}
    @else
    {{  sprintf(trans('cruds.commission.fields.commission_down_x_level'), $level); }}
    @endif
    </label>
    @if(isset($commissionsIds) && array_key_exists($level, $commissionsIds))
    <input type="hidden" name="commissionsIds[{{$level}}]" value="{{ $commissionsIds[$level] }}">
    @endif
    <input class="form-control {{ $errors->has('commissions[$level]') ? 'is-invalid' : '' }}" type="text" name="commissions[{{$level}}]" id="commissions[{{$level}}]" value="{{ old('commissions[$level]', $commissionValue) }}" placeholder="e.g. 100.00, 10%">
    @if($errors->has('commissions[{{$level}}]'))
    <div class="invalid-feedback">
        {{ $errors->first('commissions[$level]') }}
    </div>
    @endif
    <span class="help-block">{{ trans('cruds.commission.fields.commission_helper') }}</span>
</div>