@if(!isset($address))
<?php $address = new App\Models\Address(); ?>
@endif
@if(!isset($prefix))
<?php $prefix = ''; ?>
@endif
@if(!isset($disabled))
<?php $disabled = false; ?>
@endif
@if(!isset($required))
<?php $required = false; ?>
@endif
@if(!isset($array))
<?php $array = ''; ?>
@else
<?php $array = '[]'; ?>
@endif
@if(!isset($errors))
<?php $errors = []; ?>
@endif
<div class="form-group">
    <label class="{{ $required ? 'required' : '' }}" id="{{$prefix}}address_line_1_lbl" for="{{$prefix}}address_line_1">{{ trans('cruds.address.fields.address_line_1') }}</label>
    <input class="form-control {{ $errors->has($prefix.'address_line_1') ? 'is-invalid' : '' }}" type="text" name="{{$prefix}}address_line_1{{$array}}" id="{{$prefix}}address_line_1" value="{{ old($prefix.'address_line_1', $address->address_line_1) }}" {{ $disabled ? 'disabled' : ' ' }} {{ $required ? 'required' : ' ' }}>
    @if($errors->has($prefix.'address_line_1'))
        <div class="invalid-feedback">
            {{ $errors->first($prefix.'address_line_1') }}
        </div>
    @endif
    <span class="help-block">{{ trans('cruds.address.fields.address_line_1_helper') }}</span>
</div>
<div class="form-group">
    <label for="{{$prefix}}address_line_2">{{ trans('cruds.address.fields.address_line_2') }}</label>
    <input class="form-control {{ $errors->has($prefix.'address_line_2') ? 'is-invalid' : '' }}" type="text" name="{{$prefix}}address_line_2{{$array}}" id="{{$prefix}}address_line_2" value="{{ old($prefix.'address_line_2', $address->address_line_2) }}" {{ $disabled ? 'disabled' : ' ' }}>
    @if($errors->has($prefix.'address_line_2'))
        <div class="invalid-feedback">
            {{ $errors->first($prefix.'address_line_2') }}
        </div>
    @endif
    <span class="help-block">{{ trans('cruds.address.fields.address_line_2_helper') }}</span>
</div>
<div class="form-group">
    <label class="{{ $required ? 'required' : '' }}" id="{{$prefix}}city_lbl" for="{{$prefix}}city">{{ trans('cruds.address.fields.city') }}</label>
    <input class="form-control {{ $errors->has($prefix.'city') ? 'is-invalid' : '' }}" type="text" name="{{$prefix}}city{{$array}}" id="{{$prefix}}city" value="{{ old($prefix.'city', $address->city) }}" {{ $disabled ? 'disabled' : ' ' }} {{ $required ? 'required' : ' ' }}>
    @if($errors->has($prefix.'city'))
        <div class="invalid-feedback">
            {{ $errors->first($prefix.'city') }}
        </div>
    @endif
    <span class="help-block">{{ trans('cruds.address.fields.city_helper') }}</span>
</div>
<div class="form-group">
    <label class="{{ $required ? 'required' : '' }}" id="{{$prefix}}state_lbl" for="{{$prefix}}state">{{ trans('cruds.address.fields.state') }}</label>
    <input class="form-control {{ $errors->has($prefix.'state') ? 'is-invalid' : '' }}" type="text" name="{{$prefix}}state{{$array}}" id="{{$prefix}}state" value="{{ old($prefix.'state', $address->state) }}" {{ $disabled ? 'disabled' : ' ' }} {{ $required ? 'required' : ' ' }}>
    @if($errors->has($prefix.'state'))
        <div class="invalid-feedback">
            {{ $errors->first($prefix.'state') }}
        </div>
    @endif
    <span class="help-block">{{ trans('cruds.address.fields.state_helper') }}</span>
</div>
<div class="form-group">
    <label class="{{ $required ? 'required' : '' }}" id="{{$prefix}}postal_code_lbl" for="p{{$prefix}}ostal_code">{{ trans('cruds.address.fields.postal_code') }}</label>
    <input class="form-control {{ $errors->has($prefix.'postal_code') ? 'is-invalid' : '' }}" type="number" name="{{$prefix}}postal_code{{$array}}" id="{{$prefix}}postal_code" value="{{ old($prefix.'postal_code', $address->postal_code) }}" step="1" {{ $disabled ? 'disabled' : ' ' }} {{ $required ? 'required' : ' ' }}>
    @if($errors->has($prefix.'postal_code'))
        <div class="invalid-feedback">
            {{ $errors->first($prefix.'postal_code') }}
        </div>
    @endif
    <span class="help-block">{{ trans('cruds.address.fields.postal_code_helper') }}</span>
</div>
<div class="form-group">
    <label class="{{ $required ? 'required' : '' }}" id="{{$prefix}}country_lbl" for="{{$prefix}}country">{{ trans('cruds.address.fields.country') }}</label>
    <input class="form-control {{ $errors->has($prefix.'country') ? 'is-invalid' : '' }}" type="text" name="{{$prefix}}country{{$array}}" id="{{$prefix}}country" value="{{ old($prefix.'country', $address->country) }}" {{ $disabled ? 'disabled' : ' ' }} {{ $required ? 'required' : ' ' }}>
    @if($errors->has($prefix.'country'))
        <div class="invalid-feedback">
            {{ $errors->first($prefix.'country') }}
        </div>
    @endif
    <span class="help-block">{{ trans('cruds.address.fields.country_helper') }}</span>
</div>
<div class="form-group">
    <label class="{{ $required ? 'required' : '' }}" id="{{$prefix}}phone_lbl" for="{{$prefix}}phone">{{ trans('cruds.address.fields.phone') }}</label>
    <input class="form-control {{ $errors->has($prefix.'phone') ? 'is-invalid' : '' }}" type="text" name="{{$prefix}}phone{{$array}}" id="{{$prefix}}phone" value="{{ old($prefix.'phone', $address->phone) }}" {{ $disabled ? 'disabled' : ' ' }} {{ $required ? 'required' : ' ' }} placeholder="{{ trans('cruds.address.fields.phone_helper') }}">
    @if($errors->has($prefix.'phone'))
        <div class="invalid-feedback">
            {{ $errors->first($prefix.'phone') }}
        </div>
    @endif
</div>