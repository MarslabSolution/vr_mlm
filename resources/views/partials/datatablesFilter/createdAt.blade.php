<select class="form-control select2" name="filter-date" id="filter-date">
<option value="">{{ trans('global.all_months') }}</option>
@foreach($dates as $key => $date)
    <option value="{{ $key }}">{{ $date }}</option>
@endforeach
</select>