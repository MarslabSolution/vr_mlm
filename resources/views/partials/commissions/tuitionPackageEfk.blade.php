@section('styles')
<style>
    .progress-bar.active,
    .progress.active .progress-bar {
        -webkit-animation: progress-bar-stripes 1s linear infinite;
        -o-animation: progress-bar-stripes 1s linear infinite;
        animation: progress-bar-stripes 1s linear infinite;
    }

    .w-90 {
        width: 90% !important;
    }

    .progress {
        height: 20px;
        margin-bottom: 20px;
        overflow: hidden;
        background-color: #f5f5f5;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 2px rgb(0 0 0 / 10%);
        box-shadow: inset 0 1px 2px rgb(0 0 0 / 10%);
    }

    #invalid-feedback-tuition_package_efk {
        color: var(--ck-color-base-error);
    }
</style>
@endsection


<div class="progress">
    <div class="progress-bar progress-bar-animated progress-bar-primary progress-bar-striped w-90" role="progressbar"
        aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
        {{ trans('global.loading') }}
    </div>
</div>
<select style="display: none;"
    class="form-control select2 {{ $errors->has('tuition_package_efk') ? 'is-invalid' : '' }}"
    name="tuition_package_efk" id="tuition_package_efk" step="1">
    {{-- contents --}}
</select>

@if($errors->has('tuition_package_efk'))
<div class="invalid-feedback">
    {{ $errors->first('tuition_package_efk') }}
</div>
@else
<div id="invalid-feedback-tuition_package_efk" style="display: none;" for="tuition_package_efk">
</div>
@endif