<script>
    $(document).ready(function() {
        var agentPlans = @json($agent_plans ?? []);
        var tuitionCommissions = @json($tuition_commissions ?? []);
        var packagesCommissions = @json(isset($commission) ? json_decode($commission->packagesCommissions->pluck('tuition_package_efk')->toJson()) : []);
        var tuitionPackageEfk = 'tuition_package_efk';
        var agentPlanId = 'agent_plan_id';
        var personal_access_token = "{{ access_token() }}";
        $.ajax({
            method: 'GET',
            headers: {
                'Authorization': 'Bearer ' + personal_access_token,
                'X-VRDRUM-USER': '{{ auth()->user()->email }}',
                'Accept': 'application/json',
            },
            url: "{{ (settings('main_system_domain', '', 'api').settings('api_version', '', 'api').settings('tuition_packages', '', 'api') ) ?? 'http://127.0.0.1:8001/api/v1/tuition-packages' }} ",
            data: {},
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success: function(data) {
                var tutionPackages = data.data;
                var options = '';
                if (tutionPackages.length > 0) {
                    isDisabled =  tutionPackages.length > 0 ? 'disabled' : '';
                    options += `<option value='' ${isDisabled}>{{ trans('global.pleaseSelect') }}</option>`;
                    for (var i = 0; i < tutionPackages.length; i++) {
                        var selected = packagesCommissions.includes(tutionPackages[i].id) ? 'selected' : '';
                        options += '<option ' + selected + ' value="' + tutionPackages[i].id + '" title="{{ trans("cruds.agentPlan.fields.price") }} : ' + tutionPackages[i].price + '">' + tutionPackages[i].name + '</option>';
                    }
                    if(isDisabled) {
                        $('#' + tuitionPackageEfk).prop('required',true);
                    }
                    $('#' + tuitionPackageEfk).html(options);
                }

            },
            error: function(xhr, response, thrownError) {
                console.log(xhr);
                $('#invalid-feedback-tuition_package_efk').html(
                    '<div class="row"><div class="col">{{ trans("global.status_code") }}</div><div class="col">: ' + xhr.status + '</div></div>' +
                    '<div class="row"><div class="col">{{ trans("global.messages") }}</div><div class="col"> : ' + xhr.statusText + '</div></div>'
                ).show();
                $('.progress').slideUp();
            }
        }).done(function() {
            $('.progress').slideUp();
            $('#' + tuitionPackageEfk).select2({
                templateResult: (option) => {
                    return $(
                        '<div><strong>' + option.text + '</strong></div><div>' + option.title + '</div>'
                    );
                },
                //placeholder: '{{ trans("cruds.commission.select_tution_package") }}',
                allowClear: true,
            });
        });
        @if(isset($commissionableLevel))
        getCommissionFields("{{$commissionableLevel}}", packagesCommissions[0], "{{$commission->agent_plan_id}}");
        @endif
        //tution package changed
        $('#' + tuitionPackageEfk).on('select2:select', function(e) {
            $('#' + agentPlanId).find('option:disabled').removeAttr('disabled').select2();
            $('#' + agentPlanId).val('').trigger('change');
            $('p.commissionable-level').html("{{  trans('global.pleaseSelect').' '.trans('cruds.commission.fields.agent_plan') }}");
            $('div.commissionable-level > div.card-body').hide();
            $('div.agent_plan_id > span.help-block').html("{{ trans('cruds.commission.fields.agent_plan_helper') }}");
            $selectedValue = $('#' + tuitionPackageEfk).select2('data')[0];
            if ($('#'+tuitionPackageEfk).select2('data')[0].id > 0 && Object.keys(tuitionCommissions).length > 0) {
                let selectAgentPlan = tuitionCommissions[$('#'+tuitionPackageEfk).select2('data')[0].id];
                let uniquesameTuitionPackage = [...new Set(selectAgentPlan)];
                uniquesameTuitionPackage.forEach(function(key) {
                    $('#' + agentPlanId).find("option[value='" + key + "']").attr('disabled', 'disabled');
                });
                if ($('#' + agentPlanId).find('option:disabled').length == $('#' + agentPlanId).find('option').length - 1) {
                    $('div.agent_plan_id > span.help-block').html("{{ trans('cruds.commission.all_agent_plan_be_assigned') }}");
                }
                $('#' + agentPlanId).select2();
            }
        });
        $('#' + agentPlanId).on('select2:select', function(e) {
            var commissionableLevel = agentPlans.filter(obj => obj.id == $('#' + agentPlanId).select2('data')[0].id)[0]['commissionable_level'] ?? 0;
            if($('#' + agentPlanId).select2('data')[0].id > 0) {
                $('div.commissionable-level').show();
            } else {
                $('div.commissionable-level').hide();
            }
            if($('#'+tuitionPackageEfk).val() != null) {
                getCommissionFields(commissionableLevel, $('#' + tuitionPackageEfk).select2('data')[0].id, $('#' + agentPlanId).select2('data')[0].id);
            }
        });
        function getCommissionFields(commissionableLevel, tuitionPackageEfk, id) {
            console.log('getCommissionFields : '+commissionableLevel+', '+tuitionPackageEfk+', '+id);
            $('p.commissionable-level').html('{{ trans("cruds.agentPlan.fields.commissionable_level") }} : '+commissionableLevel);
            var html = '';
            $.ajax({
                method: 'POST',
                headers: {
                    'x-csrf-token': "{{ csrf_token() }}",
                },
                url: ' {{ route("admin.commissions.returnCommissionFields") }} ',
                data: {
                    'number': commissionableLevel,
                    'tuitionPackageEfk': tuitionPackageEfk,
                    'commission': @json($commission ?? null),
                    'agentPlanId': id,
                    'type': 'package',
                    _token:  "{{ csrf_token() }}",
                },
                success: function(data) {
                    html += data.data;
                },
                error: function(xhr, response, thrownError) {
                    html += '<div class="row"><div class="col">{{ trans("global.status_code") }}</div><div class="col">: ' + xhr.status + '</div></div>';
                    html += '<div class="row"><div class="col">{{ trans("global.messages") }}</div><div class="col"> : ' + xhr.statusText + '</div></div>';
                    
                }
            }).done(function() {
                $('div.commissionable-level > div.card-body').html(html).show();
            });
        }
    });
</script>