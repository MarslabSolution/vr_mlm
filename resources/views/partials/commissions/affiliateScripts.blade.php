<script>
function getCommissionFields(commissionableLevel, agentPlanId) {
    var html = '';
    $('.btn-add').html('<i class="fa fa-spinner fa-spin"></i> Loading...');
    $.ajax({
        method: 'POST',
        headers: {
            'x-csrf-token': _token,
        },
        url: ' {{ route("admin.commissions.returnCommissionFields") }} ',
        data: {
            'number': commissionableLevel,
            'agentPlanId': agentPlanId,
            'type': 'affiliate',
            'upline':true
        },
        success: function(data) {
            html += data.data;
        },
        error: function(xhr, response, thrownError) {
            html += '<div class="row"><div class="col">{{ trans("global.status_code") }}</div><div class="col">: ' + xhr.status + '</div></div>';
            html += '<div class="row"><div class="col">{{ trans("global.messages") }}</div><div class="col"> : ' + xhr.statusText + '</div></div>';
            
        }
    }).done(function() {
        $('div.commission > div.card-body').html(html);
        $('.btn-add').html("{{ trans('global.update') }}");
        $('div.commission').show();
        $('.btn-reset').show();
    });
}
$(document).ready(function() {
    var selectedAgentId = "{{ old('agent_plan_id') ?? (isset($commission) ? $commission->agent_plan->id : '') ?? '' }}";
    var commissionableLevel = "{{ old('commissionable_level') ??  (isset($commissionableLevel) ? $commissionableLevel : '') ?? '' }}";
    if(commissionableLevel && selectedAgentId) {
        getCommissionFields(commissionableLevel, selectedAgentId);
    }
});

</script>