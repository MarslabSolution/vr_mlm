<!DOCTYPE html>
<html class="{{ $color_scheme != 'default' ? ' ' . $color_scheme : '' }}">

<head>
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('images/icons/android-chrome-192x192.png') }}">
    <link rel="icon" type="image/png" sizes="512x512"  href="{{ asset('images/icons/android-chrome-512x512.png') }}">
    <link rel="icon" href="{{ asset('images/icons/favicon.ico') }}" sizes="any">
    <link rel="icon" href="{{ asset('images/icons/Logo.svg') }}" type="image/svg+xml">
    <link rel="manifest" href="{{ asset('manifest.webmanifest.json') }}">
</head>

    <div class="fixed-bottom pt-2 px-2">
        <ul class="c-bottom-nav w-100 justify-content-around">
            <li class="c-bottom-nav-item {{ request()->is("dealer") ? "c-active" : "" }}">
                <a class="c-bottom-nav-link" href="{{ route('dealer.home') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="d-flex flex-column justify-content-center align-items-center text-center">
                        <i class="fas fa-home fa-2x"></i>
                        <span>{{ trans('global.dashboard') }}</span>
                    </div>
                </a>
            </li>

            <li class="c-bottom-nav-item {{ request()->is("dealer/dealer-students") ? "c-active" : "" }}">
                <a class="c-bottom-nav-link" href="{{ route('dealer.dealer-students.index') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="d-flex flex-column justify-content-center align-items-center text-center">
                        <i class="fas fa-history fa-2x"></i>
                        <span>{{ trans('cruds.agentStudent.nav_title') }}</span>
                    </div>
                </a>
            </li>

            <li class="c-bottom-nav-item {{ request()->is("dealer/dealer-downline") ? "c-active" : "" }}">
                <a class="c-bottom-nav-link" href="{{ route('dealer.dealer-downline.index') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="d-flex flex-column justify-content-center align-items-center text-center">
                        <i class="fas far fa-arrow-alt-circle-down fa-2x"></i>
                        <span>{{ trans('cruds.agentDownline.title') }}</span>
                    </div>
                </a>
            </li>

            <li class="c-bottom-nav-item {{ request()->is("dealer/dealer-profile") ? "c-active" : "" }}">
                <a class="c-bottom-nav-link" href="{{ route("dealer.dealer-profile.index") }}" role="button" aria-haspopup="true" aria-expanded="false">
                    <div class="d-flex flex-column justify-content-center align-items-center text-center">
                        <i class="fas fa-user-circle fa-2x"></i>
                        <span>{{ trans('global.my_profile') }}</span>
                    </div>
                </a>
            </li>
        </ul>
    </div>

</html>
