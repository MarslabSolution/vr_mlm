<div id="sidebar" class="bg-sidebar c-app c-sidebar c-sidebar-fixed c-sidebar-lg-show ">

    <div class="c-sidebar-brand d-md-down-none">
        <a class="c-sidebar-brand-full h4" href="">
        @if(View::hasSection('page_title'))
        @yield('page_title') - {{ settings('site_name', trans('panel.site_title')) }}
        @elseif(View::hasSection('title'))
        @yield('title')
        @else {{ settings('site_name',trans('panel.site_title')) }}
        @endif
        </a>
    </div>
    <ul class="c-sidebar-nav">
        <!-- <li>
            <select class="searchable-field form-control">
            </select>
        </li> -->
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.home") }}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fas fa-fw fa-tachometer-alt">

                </i>
                {{ trans('global.dashboard') }}
            </a>
        </li>
        @include('ssoclient::partials.userManagement')
        @can('student_accesssss')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.students.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/students") || request()->is("admin/students/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-user-graduate c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.student.title') }}
                </a>
            </li>
        @endcan
        @can('commission_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/commissions/*") || request()->is("admin.commissions.*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-hand-holding-usd c-sidebar-nav-icon"></i>
                    {{ trans('cruds.commission.title') }} @can('agent_plan_access') {{  ' & ' . trans('cruds.agentPlan.title') }} @endcan
                </a>
                 <ul class="c-sidebar-nav-dropdown-items">
                    @can('agent_plan_access')
                    <li class="c-sidebar-nav-item">
                        <a href="{{ route("admin.agent-plans.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/agent-plans") || request()->is("admin/agent-plans/*") ? "c-active" : "" }}">
                            <i class="fa-fw fas fa-cubes c-sidebar-nav-icon"></i>
                            {{ trans('cruds.agentPlan.title') }}
                        </a>
                    </li>
                    <li class="c-sidebar-nav-dropdown {{ request()->is("admin/commissions/*") || request()->is("admin.commissions.*") ? "c-show" : "" }}">
                        <a class="c-sidebar-nav-dropdown-toggle" href="#">
                            <i class="fa-fw fas fa-swatchbook c-sidebar-nav-icon"></i>
                            {{ trans('cruds.commission.title_cms') }}
                        </a>
                        <ul class="c-sidebar-nav-dropdown-items pl-4">
                            <li class="c-sidebar-nav-item third_menu">
                                <a href="{{ route("admin.commissions.index", ['type'=> 'package']) }}" class="c-sidebar-nav-link {{ request()->is("admin/commissions/package") || request()->is("admin/commissions/*/package") || request()->is("admin/commissions/package/*") ? "c-active" : "" }}">
                                    <i class="fa-fw fas fa-swatchbook c-sidebar-nav-icon"></i>
                                    {{ trans('cruds.commission.commission_for_tuition_package') }}
                                </a>
                            </li>
                            <li class="c-sidebar-nav-item third_menu">
                                <a href="{{ route("admin.commissions.index", ['type'=> 'affiliate']) }}" class="c-sidebar-nav-link {{ request()->is("admin/commissions/affiliate") || request()->is("admin/commissions/*/affiliate") || request()->is("admin/commissions/affiliate/*") ? "c-active" : "" }}">
                                    <i class="fas fa-layer-group fa-fw c-sidebar-nav-icon"></i>
                                    {{ trans('cruds.commission.commission_for_affiliate') }}
                                </a>
                            </li>
                        </ul>
                    </li>
                    @else
                    <li class="c-sidebar-nav-item">
                        <a href="{{ route("admin.commissions.index", ['type'=> 'package']) }}" class="c-sidebar-nav-link {{ request()->is("admin/commissions/package") || request()->is("admin/commissions/*/package") || request()->is("admin/commissions/package/*") ? "c-active" : "" }}">
                            <i class="fa-fw fas fa-swatchbook c-sidebar-nav-icon"></i>
                            {{ trans('cruds.commission.commission_for_tuition_package') }}
                        </a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a href="{{ route("admin.commissions.index", ['type'=> 'affiliate']) }}" class="c-sidebar-nav-link {{ request()->is("admin/commissions/affiliate") || request()->is("admin/commissions/*/affiliate") || request()->is("admin/commissions/affiliate/*") ? "c-active" : "" }}">
                            <i class="fas fa-layer-group fa-fw c-sidebar-nav-icon"></i>
                            {{ trans('cruds.commission.commission_for_affiliate') }}
                        </a>
                    </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can(['dealer_access','agent_plan_access'])
         <li class="c-sidebar-nav-dropdown {{ request()->is(["admin/dealer", "admin/franchiser", "admin/agents/*"]) ? "c-show" : "" }}">
            <a class="c-sidebar-nav-dropdown-toggle" href="#">
                <i class="fa-fw fas fa-user-tie c-sidebar-nav-icon"></i>
                {{ trans('cruds.agentManagement.title') }}
            </a>
            <ul class="c-sidebar-nav-dropdown-items">
                @can('dealer_access')
                <li class="c-sidebar-nav-item">
                    <a href="{{ route("admin.agents.index", ['type' => 'dealer']) }}" class="c-sidebar-nav-link {{ request()->is("admin/agents/dealer") || request()->is("admin/agents/dealer/*") ? "c-active" : "" }}">
                        <span class="material-symbols-outlined c-sidebar-nav-icon">emoji_people</span>
                        {{ trans('cruds.dealer.title') }}
                    </a>
                </li>
                @endcan
                @can('franchiser_access')
                <li class="c-sidebar-nav-item">
                    <a href="{{ route("admin.agents.index", ['type' => 'franchiser']) }}" class="c-sidebar-nav-link {{ request()->is("admin/agents/franchiser") || request()->is("admin/agents/franchiser/*") ? "c-active" : "" }}">
                        <span class="material-symbols-outlined c-sidebar-nav-icon">escalator_warning</span>
                        {{ trans('cruds.franchiser.title') }}
                    </a>
                </li>
                @endcan
            </ul>
        </li>
        @endcan
        @can('mlm_level_accesss')
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.mlm-levels.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/mlm-levels") || request()->is("admin/mlm-levels/*") ? "c-active" : "" }}">
                <i class="fa-fw fas fa-stream c-sidebar-nav-icon"></i>
                {{ trans('cruds.mlmLevel.title') }}
            </a>
        </li>
        @endcan
        @can('development')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.agent-students.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/dealer-students") || request()->is("admin/dealer-students/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.agentStudent.title') }}
                </a>
            </li>
        @endcan
        @can('commission_statement_access')
        <li class="c-sidebar-nav-dropdown {{ request()->is(["admin/commissions/", "admin/commissions.*", "admin/commission-statements/*"]) ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-file-invoice c-sidebar-nav-icon"></i>
                    {{ trans('cruds.statement.title') }}
                </a>
                 <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <a href="{{ route("admin.commission-statements.index", ['type' => 'dealer']) }}" class="c-sidebar-nav-link {{ request()->is("admin/commission-statements/dealer") || request()->is("admin/commission-statements/dealer/*") ? "c-active" : "" }}">
                            <span class="material-symbols-outlined c-sidebar-nav-icon">emoji_people</span>
                            {{ trans('cruds.commissionStatement.for_dealer') }}
                        </a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a href="{{ route("admin.commission-statements.index", ['type' => 'franchiser']) }}" class="c-sidebar-nav-link {{ request()->is("admin/commission-statements/frachiser") || request()->is("admin/commission-statements/frachiser/*") ? "c-active" : "" }}">
                            <span class="material-symbols-outlined c-sidebar-nav-icon">escalator_warning</span>
                            {{ trans('cruds.commissionStatement.for_frachiser') }}
                        </a>
                    </li>
                </ul>
            </li>
        @endcan
        @can('setting_access')
            <li class="c-sidebar-nav-dropdown {{ request()->is("admin/settings/*") || Request::is("admin.settings.*") ? "c-show" : "" }}">
                <a class="c-sidebar-nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-cog c-sidebar-nav-icon"></i>
                    {{ trans('cruds.setting.title') }}
                </a>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <a href="{{ route("admin.settings.index", ['group' => 'general']) }}" class="c-sidebar-nav-link {{ request()->is("admin/settings/general") || request()->is("admin/settings/*/general/*") || request()->is("admin/settings/general/*") ? "c-active" : "" }}">
                            <i class="fa-fw fas fa-bars c-sidebar-nav-icon"></i>
                            {{ trans('cruds.setting.general') }}
                        </a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a href="{{ route("admin.settings.index", ['group' => 'condition']) }}" class="c-sidebar-nav-link {{ request()->is("admin/settings/condition") || request()->is("admin/settings/*/condition/*") || request()->is("admin/settings/condition/*") ? "c-active" : "" }}">
                            <i class="fas fa-greater-than-equal fa-fw c-sidebar-nav-icon"></i>
                            {{ trans('settings.condition.title') }}
                        </a>
                    </li>
                    <li class="c-sidebar-nav-item">
                        <a href="{{ route("admin.settings.index", ['group' => 'api']) }}" class="c-sidebar-nav-link {{ request()->is("admin/settings/api") || request()->is("admin/settings/*/api/*") || request()->is("admin/settings/api/*") ? "c-active" : "" }}">
                            <i class="fas fa-bezier-curve fa-fw c-sidebar-nav-icon"></i>
                            {{ trans('settings.api.title') }}
                        </a>
                    </li>
                </ul>
            </li>
        @endcan
        @can('commission_statement_accesss')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.commission-statements.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/commission-statements") || request()->is("admin/commission-statements/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.commissionStatement.title') }}
                </a>
            </li>
        @endcan
        @can('commission_type_statement_accesss')
            <li class="c-sidebar-nav-item">
                <a href="{{ route("admin.commission-type-statements.index") }}" class="c-sidebar-nav-link {{ request()->is("admin/commission-type-statements") || request()->is("admin/commission-type-statements/*") ? "c-active" : "" }}">
                    <i class="fa-fw fas fa-cogs c-sidebar-nav-icon">

                    </i>
                    {{ trans('cruds.commissionTypeStatement.title') }}
                </a>
            </li>
        @endcan
        @can('development')
        <li class="c-sidebar-nav-item">
            <a href="{{ route("admin.systemCalendar") }}" class="c-sidebar-nav-link {{ request()->is("admin/system-calendar") || request()->is("admin/system-calendar/*") ? "c-active" : "" }}">
                <i class="c-sidebar-nav-icon fa-fw fas fa-calendar">

                </i>
                {{ trans('global.systemCalendar') }}
            </a>
        </li>
        @endcan
        @include('ssoclient::partials.passwordEdit')
        <li class="c-sidebar-nav-item">
            <a href="#" class="c-sidebar-nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                <i class="c-sidebar-nav-icon fas fa-fw fa-sign-out-alt">

                </i>
                {{ trans('global.logout') }}
            </a>
        </li>
    </ul>

</div>
