@extends('dealer.layout')
@section('body')
<div class="content no-margin-left">
    <div class="p-1">
        <h5>{{ trans('cruds.dealerUpgradePlan.fields.request') }}</h5>

        @if(count($plan_upline_users) > 0)


            <div class="intro-y box p-4 mt-4">
                <div class="mb-4">
                    <!-- <label for="agent_plan_select">{{ trans('cruds.dealerUpgradePlan.fields.agent_plan') }}</label> -->
                    <h2 class="font-medium text-lg mr-4 mb-01">{{ trans('cruds.dealerUpgradePlan.fields.agent_plan') }}</h2>
                    <div class="col-6 px-0 mt-2">
                        <select class="form-control select2" name="agent_plan_select" id="agent_plan_select">
                            @foreach($agent_plans as $id => $value)
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col">
                        <h2 class="font-medium text-lg mr-4 mb-01">{{ trans('cruds.dealerUpgradePlan.fields.upline_detail') }}</h2>
                    </div>
                </div>

                <div class="row mb-2">
                    <div class="col-3">
                        {{ trans('cruds.fields.name') }}
                    </div>

                    <div class="col">: <span id="upline_name"></span></div>
                </div>

                <div class="row mb-2">
                    <div class="col-3">
                        {{ trans('cruds.dealerUpgradePlan.fields.user_phone') }} :
                    </div>

                    <div class="col">: <span id="upline_phone"></span></div>
                </div>

                <div class="row mb-2">
                    <div class="col-3">
                        {{ trans('cruds.dealerUpgradePlan.fields.user_email') }} :
                    </div>

                    <div class="col">: <span id="upline_email"></span></div>
                </div>
            </div>
        @else
            <div class="card p-4">
                <h6>{{ trans('cruds.dealerUpgradePlan.fields.high_level') }}</h6>
            </div>
        @endif
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
        var $plan_select = $('#agent_plan_select')

        $plan_select.on('change', function(){
            var $plan_upline_users = @json($plan_upline_users);

            if($plan_upline_users.length > 0){
                var $plan_upline_user = $plan_upline_users[$plan_select.val()];

                $('#upline_name').text($plan_upline_user.name ?? '-');
                $('#upline_phone').text($plan_upline_user.phone ?? '-');
                $('#upline_email').text($plan_upline_user.email ?? '-');
            }

        }).trigger('change');
    });
</script>
@endsection
