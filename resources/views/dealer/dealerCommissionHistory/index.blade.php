@extends('dealer.layout')
@section('body')

<div>
    <ul class="nav nav-pills mb-3" id="formTab" role="tablist">
        @foreach($all_level_downlines as $index => $level_data)
            <li class="nav-item">
                <a class="nav-link {{ $index == 0 ? 'active' : '' }}" id="downline-{{ $index }}Btn" data-toggle="pill" href="#downline-{{ $index }}Tab" role="tab" aria-controls="downline-{{ $index }}Tab" aria-selected="true">
                    {{ $index == 0 ? trans('cruds.agentDownline.fields.all') . ' ' . trans('cruds.agentDownline.fields.level') : trans('cruds.agentDownline.fields.level') . $index }}
                </a>
            </li>
        @endforeach
    </ul>

    <div class="card alert mb-3">
        <h5>{{ trans('cruds.agentDownline.fields.date_filter') }}</h5>
        <select class="form-control select2" name="month_select" id="month_select">
            @foreach($month_select as $index => $date_range)
                <option value="{{ $date_range['from'] . '|' . $date_range['to'] }}">{{ $index == 0 ? trans('cruds.agentDownline.fields.all') : substr($date_range['from'], 0, 10) . ' - ' . substr($date_range['to'], 0, 10) }}</option>
            @endforeach
        </select>
    </div>

    <div class="tab-content" id="formTabContent">
        @foreach($all_level_downlines as $level_index => $level_data)
            <div class="tab-pane fade active {{ $level_index == 0 ? 'show' : '' }}" id="downline-{{ $level_index }}Tab" role="tabpanel" aria-labelledby="downline-{{ $level_index }}Btn">
                <div class="card">
                    <div class="card-header font-semibold text-lg">
                        <div class="row justify-content-between px-3">
                            <div>{{ $level_index == 0 ? trans('cruds.agentDownline.fields.all') . ' ' . trans('cruds.agentDownline.fields.level') : trans('cruds.agentDownline.fields.level') . ' ' . $level_index }} {{ trans('cruds.agentDownline.title_singular') }} {{ trans('global.list') }}</div>
                            <div id="total-commission-{{ $level_index }}">{{ trans('cruds.agentDownline.fields.total_commission') }} {{ trans('global.rm') }} {{ $level_data['commission'] }}</div>
                        </div>                        
                    </div>

                    <div class="card-body dataTables_scroll">
                        <table id="datatable-AgentDownline-{{ $level_index }}" class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-AgentDownline-{{ $level_index }}">
                            <thead>
                                <tr>
                                    <th width="10">
                                    </th>
                                    <th>
                                        {{ trans('cruds.fields.id') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.agentDownline.fields.user') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.agentDownline.fields.current_plan') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.agentDownline.fields.total_student') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.agentDownline.fields.register_at') }}
                                    </th>
                                    <th>
                                        {{ trans('cruds.agentDownline.fields.cumulative_commission') }} {{ trans('global.rm') }}
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>

@endsection
@section('scripts')
@parent
<script>
    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

        var $month_select = $("#month_select")

        $month_select.on('change', function(){
            $data_range = $month_select.val().split("|")
            $data_from = $data_range[0]
            $data_to = $data_range[1]

            $.ajax({
                headers: {
                    'x-csrf-token': _token
                },
                method: 'POST',
                url: "{{ route('dealer.dealer-commission-history.update') }}",
                data: { 
                    from: $data_from,
                    to: $data_to,
                }
            })
            .done(function($data) {    
                $data.forEach(($value, $level_index) => {
                    let $collection = $value['downlines']

                    let dtOverrideGlobals = {
                        buttons: dtButtons,
                        processing: true,
                        serverSide: true,
                        retrieve: true,
                        aaSorting: [],
                        ajax: {
                            headers: {
                                'x-csrf-token': _token
                            },
                            type: 'POST',
                            url: "{{ route('dealer.dealer-commission-history.generateTable') }}",
                            data: {
                                collection: JSON.stringify($collection)
                            },
                        },
                        columns: [
                            { data: 'placeholder', name: 'placeholder' },
                            { data: 'id', name: 'id' },
                            { data: 'user_name', name: 'user.name' },
                            { data: 'current_plan_name', name: 'current_plan.name' },
                            { data: 'student_count', name: 'student_count' },
                            { data: 'register_at', name: 'register_at' },
                            { data: 'cumulative_commission', name: 'cumulative_commission' },
                        ],
                        orderCellsTop: true,
                        order: [[ 1, 'desc' ]],
                        pageLength: 100,
                    };
                    
                    if ($.fn.dataTable.isDataTable('#datatable-AgentDownline-'+$level_index)) {
                        $('#datatable-AgentDownline-'+$level_index).DataTable().rows().remove();
                        $('#datatable-AgentDownline-'+$level_index).DataTable().destroy();
                    }

                    let table = $('#datatable-AgentDownline-'+$level_index).DataTable(dtOverrideGlobals);
                })
            });
        }).trigger('change');

        $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
    });
</script>
@endsection