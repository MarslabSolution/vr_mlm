@extends('layouts.master')
@section('content')
@include('partials.dealer.dealerNav')
<div class="c-wrapper mr-0">
<header class="top-bar px-8">
        <img src="{{ asset('images/VrWhite.png') }}" alt="" class="logo-svg left-logo">

        <ul class="c-header-nav ml-auto text-slate-500">
            @if(count(config('panel.available_languages', [])) > 1)
            <li class="c-header-nav-item dropdown">
                <a class="c-header-nav-link color-white" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                    aria-expanded="false">
                    {{ strtoupper(app()->getLocale()) }}
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    @foreach(config('panel.available_languages') as $langLocale => $langName)
                    <a class="dropdown-item" href="{{ url()->current() }}?change_language={{ $langLocale }}">{{
                        strtoupper($langLocale) }} ({{ $langName }})</a>
                    @endforeach
                </div>
            </li>
            @endif

            @include('ssoclient::partials.notification')

            <!-- <li class="c-header-nav-item">
                <a href="#" class="c-header-nav-link"
                    onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </li> -->

            <li class="c-header-nav-item dropdown ">
                <div class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                    aria-expanded="false">
                    <div class="w-40px h-10 image-fit">
                        <img class="rounded-full border-2 border-white border-opacity-10 shadow-lg" src="{{ asset('images/sample-user-profile.jpeg') }}">
                    </div>
                    <div class="hidden md:block ml-3">
                        <div class="max-w-[7rem] truncate font-medium color-white">{{auth()->user()->name }}</div>
                        <div class="text-xs color-white">{{auth()->user()->roles()->first()->title }}</div>
                    </div>
                </div>
                <div class="dropdown-menu dropdown-menu-right">
                    <a href="#" class="dropdown-item"
                        onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <i class="fas fa-sign-out-alt"></i>
                        <span class="logout-text">{{ trans('global.logout') }}</span>
                    </a>
                </div>
            </li>

        </ul>

        <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </header>

    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid mb-16">
                @yield('body')
            </div>
        </main>
    </div>
</div>
@endsection
