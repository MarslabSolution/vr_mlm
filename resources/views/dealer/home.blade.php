@extends('dealer.layout')
@section('body')

<div class="content ml-0">
    <div class="intro-y flex items-center mt-4">
        <h2 class="text-xl font-semibold truncate mr-5">General Report</h2>
    </div>

    <div class="intro-y report-box">
        <div class="box py-0 xl:py-5 grid grid-cols-12 gap-0 divide-y xl:divide-y-0 divide-x divide-dashed divide-slate-200 dark:divide-white/5">
            <div class="report-box__item p-01 col-span-12 sm:col-span-6 xl:col-span-3">
                <div class="report-box__content">
                    <div class="flex">
                        <div class="report-box__item__icon text-primary bg-primary/20 border border-primary/20 flex items-center justify-center rounded-full">
                            <i class="fa fa-sitemap"></i>
                        </div>
                    </div>
                    <div class="text-2xl font-medium leading-7 mt-6">{{ count($mlm_levels) > 0 ? count($mlm_levels) - 1 : 0 }}</div>
                    <div class="text-slate-500 mt-1">{{ trans('cruds.dealerHome.downline') }}</div>
                </div>
            </div>
            <div class="report-box__item p-01 sm:!border-t-0 col-span-12 sm:col-span-6 xl:col-span-3">
                <div class="report-box__content">
                    <div class="flex">
                        <div class="report-box__item__icon text-pending bg-pending/20 border border-pending/20 flex items-center justify-center rounded-full">
                            <i class="fa fa-usd"></i>
                        </div>
                    </div>
                    <div class="text-2xl font-medium leading-7 mt-6">{{ $cumulative_commission }}</div>
                    <div class="text-slate-500 mt-1">{{ trans('cruds.dealerHome.commission') }} {{ trans('global.rm') }}</div>
                </div>
            </div>
            <div class="report-box__item p-01 col-span-12 sm:col-span-6 xl:col-span-3">
                <div class="report-box__content">
                    <div class="flex">
                        <div class="report-box__item__icon text-warning bg-warning/20 border border-warning/20 flex items-center justify-center rounded-full">
                            <i class="fa fa-users"></i>
                        </div>
                    </div>
                    <div class="text-2xl font-medium leading-7 mt-6">{{ $downline_student }}</div>
                    <div class="text-slate-500 mt-1">{{ trans('cruds.dealerHome.downline_student') }}</div>
                </div>
            </div>
            <div class="report-box__item p-01 col-span-12 sm:col-span-6 xl:col-span-3">
                <div class="report-box__content">
                    <div class="flex">
                        <div class="report-box__item__icon text-success bg-success/20 border border-success/20 flex items-center justify-center rounded-full">
                            <i class="fa fa-users "></i>
                        </div>
                    </div>
                    <div class="text-2xl font-medium leading-7 mt-6">{{ $user_student }}</div>
                    <div class="text-slate-500 mt-1">{{ trans('cruds.dealerHome.user_student') }}</div>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box mt-10">
        <div class="flex flex-col sm:flex-row items-center p-01 border-b border-slate-200/60">
            <h2 class="font-medium text-lg mr-auto mb-01">{{ trans('cruds.agentStudent.in_month_title') }} {{ trans('global.list') }}</h2>
        </div>
        <div class="p-01" id="basic-table">
            <div class="preview">
                <div class="overflow-x-auto">
                    <table class="table table-bordered table-striped table-hover ajaxTable datatable datatable-DealerStudentInMonth">
                        <thead>
                            <tr>
                                <th width="10">

                                </th>
                                <th>
                                    {{ trans('cruds.fields.id') }}
                                </th>
                                <th>
                                    {{ trans('cruds.agentStudent.fields.tuition_package_efk') }}
                                </th>
                                <th>
                                    {{ trans('cruds.agentStudent.fields.student_id') }}
                                </th>
                                <th>
                                    {{ trans('cruds.agentStudent.fields.register_at') }}
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="intro-y box mt-10 mb-4">
        <div class="flex flex-col sm:flex-row items-center p-01 border-b border-slate-200/60">
            <h2 class="font-medium text-lg mr-auto mb-01">{{ trans('cruds.dealerHome.fields.commission_chart') }}</h2>
        </div>
        <div class="chart">
            <canvas id="commissionChart"></canvas>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/3.8.0/chart.min.js"></script>
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

    let dtDealerStudentInMonth = {
        buttons: dtButtons,
        processing: true,
        serverSide: true,
        retrieve: true,
        searching: false,
        lengthChange: false,
        info: false,
        scrollX: true,
        aaSorting: [],
        ajax: "{{ route('dealer.dealer-students.agentStudentInMonth') }}",
        columns: [
            { data: 'placeholder', name: 'placeholder' },
            { data: 'id', name: 'id' },
            { data: 'tuition_package_efk', name: 'tuition_package_efk' },
            { data: 'student_id', name: 'student_id' },
            { data: 'register_at', name: 'register_at' },
        ],
        orderCellsTop: true,
        order: [[ 1, 'desc' ]],
        pageLength: "{{ config('constants.dealer.minimum_temp') }}",
    };
    let tableInMonth = $('.datatable-DealerStudentInMonth').DataTable(dtDealerStudentInMonth);
    $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });

    $.ajax({
        headers: {
            'x-csrf-token': _token
        },
        method: 'GET',
        url: "{{ route('dealer.home.getLatestMonthCommission') }}",
    })
    .done(function(chartData) {
        if ($("#commissionChart").length) {
            var horizontalChart = $("#commissionChart")[0].getContext("2d");

            var myChart = new Chart(horizontalChart, {
                type: "bar",
                data: {
                    labels: chartData.map((dataValue) => {
                        return dataValue['date'].substring(0, dataValue['date'].lastIndexOf('-'));
                    }),
                    datasets: [
                        {
                            label: "{{ trans('cruds.dealerHome.fields.label_you_own') }}",
                            barPercentage: 0.4,
                            borderWidth: 1,
                            data: chartData.map((dataValue) => {
                                return dataValue['user']
                           }),
                            borderColor: '#ffb9bd',
                            backgroundColor: '#ffb9bd'
                        },
                        {
                            label: "{{ trans('cruds.dealerHome.fields.label_downlne') }}",
                            barPercentage: 0.4,
                            borderWidth: 1,
                            data: chartData.map((dataValue) => {
                                return dataValue['downline']
                            }),
                            borderColor: '#fa5661',
                            backgroundColor: '#fa5661'
                        }
                    ]
                },
                options: {
                    indexAxis: "y",
                    maintainAspectRatio: false,
                    plugins: {
                    legend: {
                        labels: {
                            color: '#b4becb',
                        }
                    }
                    },
                    scales: {
                        x: {
                            ticks: {
                                font: {
                                    size: 12
                                },
                                color: '#b4becb',
                                callback: function callback(value, index, values) {
                                    return "{{ trans('global.rm') }} " + value;
                                }
                            },
                            grid: {
                            display: false,
                            drawBorder: false
                            }
                        },
                        y: {
                            ticks: {
                                font: {
                                    size: 12
                                },
                                color: '#b4becb',
                            },
                            grid: {
                                color: $("html").hasClass("dark") ? '#b4becb' : '#b4becb',
                                borderDash: [2, 2],
                                drawBorder: false
                            }
                        }
                    }
                }
            });
        }
    });
});

</script>
@endsection
