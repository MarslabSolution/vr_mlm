@extends('dealer.layout')
@section('body')
@can('agent_downline_access')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12" class="row">
            <a id='dailyBtn' class="btn btn-success" href="{{ route("dealer.dealer-commission-report.daily") }}">
                Daily
            </a>
            <a id='weeklyBtn' class="btn btn-success" href="{{ route("dealer.dealer-commission-report.weekly", "") }}">
                Weekly
            </a>
            <a id='monthlyBtn' class="btn btn-success" href="{{ route("dealer.dealer-commission-report.monthly") }}">
                Monthly
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('cruds.dealerCommissionReport.title_singular') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-DealerCommissionReport">
            <thead>
                <tr>
                    <th width="10">

                    </th>
                    <th>
                        {{ trans('cruds.fields.id') }}
                    </th>
                    <th>
                        {{ trans('cruds.dealerCommissionReport.fields.user') }}
                    </th>
                    <th>
                        {{ trans('cruds.dealerCommissionReport.fields.current_plan') }}
                    </th>
                    <th>
                        {{ trans('cruds.dealerCommissionReport.fields.total_student') }}
                    </th>
                    <th>
                        &nbsp;
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

  <?php  
    $url =  "//{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    $url_ary = explode('/', $url);
    $last_index = count($url_ary) - 1;

    if($url_ary[$last_index] == 'daily' || $url_ary[$last_index] == 'weekly' || $url_ary[$last_index] == 'monthly'){
        $report_type = $url_ary[$last_index];
        $date = '';
    }else if($url_ary[$last_index-1] == 'daily' || $url_ary[$last_index-1] == 'weekly' || $url_ary[$last_index-1] == 'monthly'){
        $report_type = $url_ary[$last_index-1];
        $date = $url_ary[$last_index];
    }else{
        $report_type = 'daily';
        $date = '';
    }
  ?>

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('dealer.dealer-commission-report.' . $report_type, ['date' => $date]) }}",
    
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'user_name', name: 'user.name' },
{ data: 'current_plan_name', name: 'current_plan.name' },
{ data: 'student_count', name: 'student_count' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  let table = $('.datatable-DealerCommissionReport').DataTable(dtOverrideGlobals);
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });  
});

</script>
@endsection