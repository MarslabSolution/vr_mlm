@extends('dealer.layout')
@section('body')

<div class="content ml-0">
    <div class="intro-y flex items-center mt-4">
        <h2 class="text-xl font-semibold truncate mr-5">{{ trans('cruds.agentDownline.title') }}</h2>
    </div>

    <div class="mb-4">
        <select class="form-control select2" name="table_type_select" id="table_type_select">
            <option value="downline">{{ trans('cruds.agentDownline.fields.current_downline') }}</option>
            <option value="pontential">{{ trans('cruds.agentDownline.fields.pontential_downline') }}</option>
        </select>
    </div>

    <div id="downlineContainer">
        <div class="intro-y row">
            <div class="col-md-4 col-lg-6 flex items-center mb-2">
                <ul class="nav nav-pills" id="formTab" role="tablist">
                    @foreach($level_downlines as $index => $level_data)
                        <li class="nav-item">
                            <a class="nav-link {{ $index == 0 ? 'active' : '' }}" id="downline-{{ $index }}Btn" data-toggle="pill" href="#downline-{{ $index }}Tab" role="tab" aria-controls="downline-{{ $index }}Tab" aria-selected="true">
                                {{ $index == 0 ? trans('cruds.agentDownline.fields.all') . ' ' . trans('cruds.agentDownline.fields.level') : trans('cruds.agentDownline.fields.level') . $index }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3 col-lg-3 col-span-12 flex items-center md:justify-end mb-2">
                <h2 class="font-medium text-base mb-01">{{ trans('cruds.agentDownline.fields.date_filter') }}</h2>
            </div>
            <div class="col-md-5 col-lg-3 col-span-12 flex items-center mb-2">
                <select class="form-control select2" name="month_select" id="month_select">
                    @foreach($month_select as $index => $date_range)
                        <option value="{{ $date_range['from'] . '|' . $date_range['to'] }}">{{ $index == 0 ? trans('cruds.agentDownline.fields.all') : substr($date_range['from'], 0, 10) . ' - ' . substr($date_range['to'], 0, 10) }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="tab-content" id="formTabContent">
            @foreach($level_downlines as $level_index => $level_data)
                <div class="tab-pane fade active" style="display: none" id="downline-{{ $level_index }}Tab" role="tabpanel" aria-labelledby="downline-{{ $level_index }}Btn">
                    <div class="intro-y box mt-4 mb-4">
                        <div class="flex flex-row justify-content-between px-6 border-b border-slate-200/60 p-01">
                            <div class="flex flex-col sm:flex-row items-center  ">
                                <h2 class="font-medium text-lg mr-4 mb-01">{{ $level_index == 0 ? trans('cruds.agentDownline.fields.all') . ' ' . trans('cruds.agentDownline.fields.level') : trans('cruds.agentDownline.fields.level') . ' ' . $level_index }} {{ trans('cruds.agentDownline.title_singular') }} {{ trans('global.list') }}</h2>
                            </div>
                            <div class="flex items-center text-end" id="total-commission-{{ $level_index }}">{{ trans('cruds.agentDownline.fields.total_commission') }} {{ trans('global.rm') }} {{ $level_data['commission'] }}</div>
                        </div>

                        <div class="card-body">
                            <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-AgentDownline-{{ $level_index }}">
                                <thead>
                                    <tr>
                                        <th width="10">
                                        </th>
                                        <th>
                                            {{ trans('cruds.fields.id') }}
                                        </th>
                                        <th>
                                            {{ trans('cruds.agentDownline.fields.user') }}
                                        </th>
                                        <th>
                                            {{ trans('cruds.agentDownline.fields.current_plan') }}
                                        </th>
                                        <th>
                                            {{ trans('cruds.agentDownline.fields.total_student') }}
                                        </th>
                                        <th>
                                            {{ trans('cruds.agentDownline.fields.register_at') }}
                                        </th>
                                        <th>
                                            {{ trans('cruds.agentDownline.fields.cumulative_commission') }} {{ trans('global.rm') }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($level_data['downlines'] as $index => $downline)
                                        <tr data-entry-id="{{ $downline->id }}">
                                            <td>
                                            </td>
                                            <td>
                                                {{ $downline->id }}
                                            </td>
                                            <td>
                                                {{ $downline->user_name }}
                                            </td>
                                            <td>
                                                {{ $downline->current_plan_name }}
                                            </td>
                                            <td id="level-{{ $level_index }}-downlineId-{{ $downline->id }}-student">
                                                {{ $downline->student_count }}
                                            </td>
                                            <td>
                                                {{ $downline->register_at }}
                                            </td>
                                            <td id="level-{{ $level_index }}-downlineId-{{ $downline->id }}-commission">
                                                {{ $downline->cumulative_commission }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div id="potentialContainer">
        <div class="intro-y row">
            <div class="col-md-4 col-lg-6 flex items-center mb-2">
                <ul class="nav nav-pills" id="potentialTab" role="tablist">
                    @foreach($potential_level_downlines as $index => $level_data)
                        <li class="nav-item">
                            <a class="nav-link {{ $index == 0 ? 'active' : '' }}" id="potential-{{ $index }}Btn" data-toggle="pill" href="#potential-{{ $index }}Tab" role="tab" aria-controls="potential-{{ $index }}Tab" aria-selected="true">
                                {{ $index == 0 ? trans('cruds.agentDownline.fields.all') . ' ' . trans('cruds.agentDownline.fields.level') : trans('cruds.agentDownline.fields.level') . $index }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="tab-content" id="potentialTabContent">
            @foreach($potential_level_downlines as $level_index => $level_data)
                <div class="tab-pane fade active" style="display: none" id="potential-{{ $level_index }}Tab" role="tabpanel" aria-labelledby="potential-{{ $level_index }}Btn">
                    <div class="intro-y box mt-4 mb-4">
                        <div class="flex flex-row justify-content-between px-6 border-b border-slate-200/60 p-01">
                            <div class="flex flex-col sm:flex-row items-center  ">
                                <h2 class="font-medium text-lg mr-4 mb-01">{{ $level_index == 0 ? trans('cruds.agentDownline.fields.all') . ' ' . trans('cruds.agentDownline.fields.level') : trans('cruds.agentDownline.fields.level') . ' ' . $level_index }} {{ trans('cruds.agentDownline.title_singular') }} {{ trans('global.list') }}</h2>
                            </div>
                            <div class="flex items-center text-end" id="potential-commission-{{ $level_index }}">{{ trans('cruds.agentDownline.fields.total_commission') }} {{ trans('global.rm') }} {{ $level_data['commission'] }}</div>
                        </div>

                        <div class="card-body">
                            <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-PotentialDownline-{{ $level_index }}">
                                <thead>
                                    <tr>
                                        <th width="10">
                                        </th>
                                        <th>
                                            {{ trans('cruds.fields.id') }}
                                        </th>
                                        <th>
                                            {{ trans('cruds.agentDownline.fields.user') }}
                                        </th>
                                        <th>
                                            {{ trans('cruds.agentDownline.fields.current_plan') }}
                                        </th>
                                        <th>
                                            {{ trans('cruds.agentDownline.fields.total_student') }}
                                        </th>
                                        <th>
                                            {{ trans('cruds.agentDownline.fields.register_at') }}
                                        </th>
                                        <th>
                                            {{ trans('cruds.agentDownline.fields.cumulative_commission') }} {{ trans('global.rm') }}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($level_data['downlines'] as $index => $downline)
                                        <tr data-entry-id="{{ $downline->id }}">
                                            <td>
                                            </td>
                                            <td>
                                                {{ $downline->id }}
                                            </td>
                                            <td>
                                                {{ $downline->user_name }}
                                            </td>
                                            <td>
                                                {{ $downline->current_plan_name }}
                                            </td>
                                            <td id="potential-{{ $level_index }}-downlineId-{{ $downline->id }}-student">
                                                {{ $downline->student_count }}
                                            </td>
                                            <td>
                                                {{ $downline->register_at }}
                                            </td>
                                            <td id="potential-{{ $level_index }}-downlineId-{{ $downline->id }}-commission">
                                                {{ $downline->cumulative_commission }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection
@section('scripts')
@parent
<script>
    $(function () {
        // ============================== table control ==============================
        var $table_type_select = $("#table_type_select")

        $table_type_select.on('change', function(){
            if($table_type_select.val() == "downline"){
                $('#downlineContainer').show();
                $('#potentialContainer').hide();
            }else{
                $('#downlineContainer').hide();
                $('#potentialContainer').show();

                $('#potential-0Tab').show();
                $('.datatable-PotentialDownline-0').DataTable().columns.adjust()
            }
        }).trigger('change');

        // ============================== table setup ==============================
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

        let dtOverrideGlobals = {
            buttons: dtButtons,
            processing: true,
            // serverSide: true,
            retrieve: true,
            aaSorting: [],
            scrollX: true,
            columns: [
                { data: 'placeholder', name: 'placeholder' },
                { data: 'id', name: 'id' },
                { data: 'user_name', name: 'user.name' },
                { data: 'current_plan_name', name: 'current_plan.name' },
                { data: 'student_count', name: 'student_count' },
                { data: 'register_at', name: 'register_at' },
                { data: 'cumulative_commission', name: 'cumulative_commission' },
            ],
            orderCellsTop: true,
            order: [[ 1, 'desc' ]],
            pageLength: 100,
        };

        var $tol_table = "{{ count($level_downlines) }}"
        var $potential_tol_table = "{{ count($potential_level_downlines) }}"

        for(var $i = 0; $i <= $tol_table; $i++){
            let table = $('.datatable-AgentDownline-' + $i).DataTable(dtOverrideGlobals);

            if($i == 0) $('#downline-'+$i+'Tab').show();
        }
        for(var $i = 0; $i <= $potential_tol_table; $i++){
            let table = $('.datatable-PotentialDownline-' + $i).DataTable(dtOverrideGlobals);

            if($i == 0) $('#potential-'+$i+'Tab').show();
        }

        $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });

        for(var $i = 0; $i <= $tol_table; $i++){
            $('#downline-'+$i+'Btn').on('click', function(){
                $level_index = $(this).attr('id').replace('downline-', '').replace('Btn', '');

                if($level_index != 0){
                    $('#downline-'+$level_index+'Tab').show();
                    $('.datatable-AgentDownline-' + $level_index).DataTable().columns.adjust()
                }

                $(this).off('click');
            });
        }
        for(var $i = 0; $i <= $potential_tol_table; $i++){
            $('#potential-'+$i+'Btn').on('click', function(){
                $level_index = $(this).attr('id').replace('potential-', '').replace('Btn', '');

                if($level_index != 0){
                    $('#potential-'+$level_index+'Tab').show();
                    $('.datatable-PotentialDownline-' + $level_index).DataTable().columns.adjust()
                }

                $(this).off('click');
            });
        }

        // ============================== for data filter ==============================
        var $month_select = $("#month_select")

        $month_select.on('change', function(){
            $data_range = $month_select.val().split("|")
            $data_from = $data_range[0]
            $data_to = $data_range[1]

            $.ajax({
                headers: {
                    'x-csrf-token': _token
                },
                method: 'POST',
                url: "{{ route('dealer.dealer-downline.updateByDate') }}",
                data: {
                    from: $data_from,
                    to: $data_to,
                }
            })
            .done(function($data) {
                $data.forEach(($value, $level_index) => {
                    $commission = $value['commission']
                    $downlines = $value['downlines']
                    $("#total-commission-"+$level_index).html("{{ trans('cruds.agentDownline.fields.total_commission') }} {{ trans('global.rm') }} " + $commission)

                    $downlines.forEach(($downline) => {
                        $("#level-"+ $level_index +"-downlineId-"+ $downline['id'] +"-student").html($downline['student_count'])
                        $("#level-"+ $level_index +"-downlineId-"+ $downline['id'] +"-commission").html($downline['cumulative_commission'])
                    })
                })
            });
        }).trigger('change');
    });
</script>
@endsection
