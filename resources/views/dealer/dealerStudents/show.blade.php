@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header font-semibold text-lg">
        {{ trans('global.show') }} {{ trans('cruds.agentStudent.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('dealer.dealer-students.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.fields.id') }}
                        </th>
                        <td>
                            {{ $dealerStudent->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.agentStudent.fields.tuition_package_efk') }}
                        </th>
                        <td>
                            {{ $dealerStudent->tuition_package_efk }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.agentStudent.fields.student_id') }}
                        </th>
                        <td>
                            {{ $dealerStudent->student_id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.agentStudent.fields.referral_id') }}
                        </th>
                        <td>
                            {{ $dealerStudent->referral_id }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('dealer.dealer-students.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection