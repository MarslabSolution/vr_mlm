@extends('dealer.layout')
@section('body')
<div class="content ml-0">
    <div class="row">
        <div class="col-md-6">
            <div class="intro-y box mt-4">

                <div class="flex flex-col sm:flex-row p-01 border-b border-slate-200/60">
                    <h2 class="font-semibold text-xl mb-0">{{ trans('global.my_profile') }}</h2>
                </div>

                <div class="card-body">
                    <form class="spacing-bottom-0" method="POST" action="{{ route('dealer.password.updateProfile') }}">
                        @csrf
                        <div class="form-group spacing-top-0">
                            <label class="required" for="name">{{ trans('cruds.fields.name') }}</label>
                            <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text"
                                name="name" id="name" value="{{ old('name', auth()->user()->name) }}" required>
                            @if($errors->has('name'))
                            <div class="invalid-feedback">
                                {{ $errors->first('name') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="required" for="title">{{ trans('cruds.fields.email') }}</label>
                            <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text"
                                name="email" id="email" value="{{ old('email', auth()->user()->email) }}" required>
                            @if($errors->has('email'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group spacing-bottom-0">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="intro-y box mt-4">
                <div class="flex flex-col sm:flex-row p-01 border-b border-slate-200/60">
                    <h2 class="font-semibold text-xl mb-0">{{ trans('global.change_password') }}</h2>
                </div>

                <div class="card-body">
                    <form class="spacing-bottom-0" method="POST" action="{{ route('dealer.password.update') }}">
                        @csrf
                        <div class="form-group spacing-top-0">
                            <label class="required" for="title">New {{ trans('cruds.user.fields.password')
                                }}</label>
                            <input class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" type="password"
                                name="password" id="password" required>
                            @if($errors->has('password'))
                            <div class="invalid-feedback">
                                {{ $errors->first('password') }}
                            </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="required" for="title">Repeat New {{ trans('cruds.user.fields.password')
                                }}</label>
                            <input class="form-control" type="password" name="password_confirmation"
                                id="password_confirmation" required>
                        </div>
                        <div class="form-group spacing-bottom-0">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="intro-y col-md-6 xl:mt-10 lg:mt-10 md:mt-10 sm:mt-6 xs:mt-6 xl:mb-6 lg:mb-6 md:mb-6 sm:mb-0 xs:mb-0">
            <div class="box">
                <div class="flex flex-col sm:flex-row p-01 border-b border-slate-200/60">
                    <h2 class="font-semibold text-xl mb-0">{{ trans('global.delete_account') }}</h2>
                </div>

                <div class="card-body">
                    <form class="spacing-bottom-0" method="POST" action="{{ route('dealer.password.destroyProfile') }}"
                        onsubmit="return prompt('{{ __('global.delete_account_warning') }}') == '{{ auth()->user()->email }}'">
                        @csrf
                        <div class="form-group spacing-top-0 spacing-bottom-0">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.delete') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @if(Route::has('dealer.password.toggleTwoFactor'))
        <div class="intro-y col-md-6 xl:mt-10 lg:mt-10 md:mt-10 sm:mt-6 xs:mt-6 sm:mb-6 xs:mb-6">
            <div class="box">
                <div class="flex flex-col sm:flex-row p-01 border-b border-slate-200/60">
                    <h2 class="font-semibold text-xl mb-0">{{ trans('global.two_factor.title') }}</h2>
                </div>

                <div class="card-body">
                    <form class="spacing-bottom-0" method="POST" action="{{ route('dealer.password.toggleTwoFactor') }}">
                        @csrf
                        <div class="form-group spacing-top-0 spacing-bottom-0">
                            <button class="btn btn-danger" type="submit">
                                {{ auth()->user()->two_factor ? trans('global.two_factor.disable') :
                                trans('global.two_factor.enable') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@endsection
