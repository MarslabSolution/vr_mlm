@extends('dealer.layout')
@section('styles')
<style>
    .px-5{
        padding-left: 0px !important;
        padding-right: 0px !important;
    }
    .mr-52{
        margin-right: 0rem !important;
    }
    .w-9{
        width: 2rem !important;
    }
    .hidden{
        display: block;
    }

</style>
@endsection
@section('body')
<div class="content ml-0">
    <div class="p-1">
        <div class="row px-4">
            <div class="intro-y box col-lg-4 col-md-6 col-sm-12 col-xs-12 xl:mr-3 lg:mr-3 md:mr-3 xl:mb-12 lg:mb-12 md:mb-12 sm:mb-12 xs:mb-12 xl:mt-6 lg:mt-6 md:mt-6 sm:mt-6 xs:mt-6">
                <div class="card-body row align-items-center">
                    <div class="col">
                        <img src="{{ asset('images/sample-user-profile.jpeg') }}" class="col img-rounded img-responsive" alt="User profile image">
                        <div class="btn-edit-profile">
                            <a class="btn btn-primary row my-2 mx-2" href="{{ route('dealer.password.edit') }}">
                                <i class="material-icons nav__icon mr-2">edit</i>
                                <span class="nav__text">{{ trans('cruds.agentProfile.edit_profile') }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro-y box col-lg-8 col-md-6 col-sm-12 col-xs-12 py-4 mb-5 xl:mt-6 lg:mt-6 md:mt-6 sm:mt-0 xs:mt-0">
                <h2 class="font-semibold text-xl mr-4 mb-01">{{ trans('cruds.agentProfile.fields.my_profile') }}</h2>
                <div class="row my-3 align-items-center">
                    <div class="col">
                        {{ trans('cruds.agentProfile.fields.account') }}
                    </div>
                    <div class="col">
                        <input type="text" class="form-control bg-white" value="{{ $login_user->id }}" disabled />
                    </div>
                </div>
                <div class="row my-3">
                    <div class="col">
                        {{ trans('cruds.agentProfile.fields.member_level') }}
                    </div>
                    <div class="col">
                        <input type="text" class="form-control bg-white" value="{{ $roles }}" disabled />
                    </div>
                </div>
                <div class="row my-3">
                    <div class="col">
                        {{ trans('cruds.agentProfile.fields.language') }}
                    </div>
                    <div class="col">
                        <select class="form-control" id="language-select">
                            @foreach(config('panel.available_languages') as $langLocale => $langName)
                                <option value="{{ $langLocale }}" {{ app()->getLocale() ? app()->getLocale() == $langLocale ? 'selected' : '' : '' }}>{{ $langName }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col">
                        {{ trans('cruds.agentProfile.fields.referral_code') }}
                    </div>
                    <div class="col">
                        <input type="text" id="referral_code" class="form-control bg-white" value="{{ $login_user->referral_code }}" disabled />
                        <a id="referral_code_btn" class="text-primary float-right mr-2">
                            share code
                        </a>
                    </div>
                </div>
                <div>
                    @include('admin.settings.general.mainSwitcher')
                </div>

            </div>
        </div>

        <div class="row px-4">
            <div class="intro-y col-lg-6 col-md-6 col-sm-6 spacing-p-0">
                <a id="deposit_btn" class="btn btn-info mb-3 btn-register" href="{{ route('dealer.dealer-deposit.index') }}">
                    {{ trans('cruds.agentProfile.fields.deposit') }}
                </a>
            </div>
            <div class="intro-y col-lg-6 col-md-6 col-sm-6 spacing-p-0">
                <a id="upgrade_plan_btn" class="btn btn-info mb-3 btn-upgrade" href="{{ route('dealer.dealer-upgrade-plan.index') }}">
                    {{ trans('cruds.agentProfile.fields.upgrade_plan') }}
                </a>
            </div>
        </div>

        <div class="spacing-p-1">
            <form id="logoutform" action="{{ route('logout') }}" method="POST" class="mb-3 mt-3">
                @csrf
                <button type="submit" class="btn btn-danger btn-logout ">{{ trans('global.logout') }}</button>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
<script>
    $(function () {
        var $referralCodeBtn = $('#referral_code_btn')

        $referralCodeBtn.on('click', function(e) {
            var $referralCode = $('#referral_code').val()

            navigator.clipboard.writeText($referralCode)

            toastr.success('Copied the text: ' + $referralCode)
        });

        var $language_select = $('#language-select')

        $language_select.on('change', function(){
            window.location.href = "{{ url()->current() }}?change_language=" + $language_select.val();
        });
    });
</script>
@endsection
