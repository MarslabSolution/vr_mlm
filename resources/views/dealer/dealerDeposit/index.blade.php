@extends('dealer.layout')
@section('body')

<div class="content ml-0">
    <div class="intro-y box mt-4">
        <!-- <div class="card-header"> -->
            <div class="flex flex-row justify-content-between px-6 border-b border-slate-200/60 p-01">
                <div class="flex flex-col sm:flex-row items-center  ">
                    <h2 class="font-medium text-lg mb-01">{{ trans('cruds.dealerDeposit.fields.refund_target_detail') }}</h2>
                </div>
                <div class="flex items-center text-end">
                    <a id="referral_code_btn" href="{{ route("dealer.dealer-downline.index") }}">
                        {{ trans('cruds.dealerDeposit.fields.view_more') }}
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table style="width:100%">
                    <thead>
                    <tbody>
                        <tr>
                            <td>{{ trans('cruds.dealerDeposit.fields.target_downline') }}</td>
                            <td><div class="progress">
                                        <div class="progress-bar w-{{ ($downline_count/$min_target) * 100 }}" role="progressbar"></div>
                                    </div>
                                <div class="d-flex justify-content-center">{{ $downline_count . ' / ' . $min_target }}</div>
                            </td>
                        </tr>
                        <tr><td>{{ trans('cruds.dealerDeposit.fields.refund_period_leave')  }}</td><td>: {{ $leave_date->format('%a days') }}</td></tr>
                        <tr><td>{{ trans('cruds.dealerDeposit.fields.refundable_amount')    }}</td><td>: {{ $refundable_amount }}</td></tr>
                    </tbody>
                </table>
            <p></p>

            <div class="flex justify-center">
                <button type="button" id="request_btn" class="btn btn-info text-white mb-3 w-full md:w-300px" {{  $leave_date->format('%a') < 100 ? 'disabled' : '' }}>
                    {{ trans('cruds.dealerDeposit.fields.request_refund') }}
                </button>
            </div>
        <!-- </div> -->
    </div>
</div>
@endsection
@section('scripts')
@parent
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script> -->
<script>
    $(function () {
        var $requestBtn = $('#request_btn')

        $requestBtn.on('click', function(e) {
            var $msg = "{{ trans('cruds.dealerDeposit.fields.pending_msg') }}"

            // toastr.success($msg)
            alert($msg)
        });
    });
</script>
@endsection
