<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateGeneralSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('general.site_name', 'VR MLM');
        $this->migrator->add('general.site_active', true);
        $this->migrator->add('general.color_scheme', 'default');
        $this->migrator->add('api.main_system_domain', 'https://www.vr.marslab.app');
        $this->migrator->add('api.api_version', '/api/v1/');
        $this->migrator->add('api.tuition_packages', 'tuition-packages');
        $this->migrator->add('api.student_details_massDestroy', 'student-details-massDestroy');
        $this->migrator->add('api.student_details_index', 'student-details');
        $this->migrator->add('condition.dealer_commission_level', 1);
        $this->migrator->add('condition.franchiser_commission_level', 2);
        $this->migrator->add('condition.maximum_franchiser_group', 155);
        $this->migrator->add('condition.maximum_franchiser_downline', 5);
        $this->migrator->add('condition.trainner_student_kpi', 100);
    }
}
