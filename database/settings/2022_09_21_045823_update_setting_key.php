<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class updateSettingKey extends SettingsMigration
{
    public function up(): void
    {
        try {
            $this->migrator->add('condition.maximum_franchiser_downline', 5);
            //$this->migrator->rename('condition.maximum_franchise_default_group', 'condition.maximum_franchise_group');
        } catch (\Throwable $th) {}
    }
    public function down(){
        $this->migrator->rename('condition.maximum_franchise_group', 'condition.maximum_franchise_default_group');
    }
}
