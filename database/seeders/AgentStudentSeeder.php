<?php

namespace Database\Seeders;

use App\Models\AgentStudent;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AgentStudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agentStudents = [];
        $local_date = Carbon::parse('2021-11-27 10:00:00');

        for ($i = 0; $i < 20; $i++) {
            $local_date->addDays(9);

            $agentStudents[$i] = [
                'id' => $i + 1,
                'tuition_package_efk' => 1,
                'student_id' => 40 + $i,
                'referral_id' => $i % 2 == 0 ? 30 + $i : 20 + $i,
                'trainer_id' => $i == 0 ? 60 + $i : 60 + (int)($i / 2),
                'created_at' => $local_date->toDateTimeString(),
                'updated_at' => $local_date->toDateTimeString(),
            ];
        }

        AgentStudent::upsert(
            $agentStudents,
            ['id'],
            [
                'tuition_package_efk',
                'student_id',
                'referral_id',
                'trainer_id',
                'created_at',
                'updated_at',
            ],
        );
    }
}
