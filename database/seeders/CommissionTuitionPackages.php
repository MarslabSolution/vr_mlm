<?php

namespace Database\Seeders;

use App\Models\AgentPlan;
use App\Models\Commission;
use App\Models\PackagesCommission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CommissionTuitionPackages extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agentPlans = AgentPlan::all();
        foreach ($agentPlans as $agentPlan) {
            for ($i=1; $i < 3; $i++) { 
                for ($level=0; $level <= $agentPlan->commissionable_level; $level++) {
                    $commission = Commission::updateOrCreate([
                        'agent_plan_id' => $agentPlan->id,
                        'level' => $level,
                        'type' => 'package',
                        'commission'=> '100',
                    ]);
                    PackagesCommission::updateOrCreate([
                        'tuition_package_efk' => $i,
                        'commission_id' => $commission->id,
                    ]);
                }
            }
        }
        Commission::create([
            'agent_plan_id'=>2,
            'type'=>'affiliate',
            'level'=>0,
            'commission'=> '20%',
        ]);
        Commission::create([
            'agent_plan_id'=>2,
            'type'=>'affiliate',
            'level'=>1,
            'commission'=> '3%',
        ]);
        Commission::create([
            'agent_plan_id'=>2,
            'type'=>'affiliate',
            'level'=>2,
            'commission'=> '2%',
        ]);
    }
}
