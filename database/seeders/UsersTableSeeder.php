<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\SsoClient\Entities\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('sso_db.users')->truncate();
        $users = [];
        $users[0] = [
            'id'             => 1,
            'name'           => 'Marslab Development',
            'email'          => 'development@marslab.com.my',
            'password'       => bcrypt('@Marslab111'),
            'remember_token' => null,
        ];
        $users[1] = [
            'id'             => 10,
            'name'           => 'Admin',
            'email'          => 'admin@admin.com',
            'password'       => bcrypt('password'),
            'remember_token' => null,
        ];
        for ($i=0; $i < 10; $i++) {
            $users[20+$i] = [
                'id'             => 20+$i,
                'name'           => 'Franchiser'.$i,
                'email'          => 'franchiser'.$i.'@gmail.com',
                'password'       => bcrypt('password'),
                'remember_token' => null,
            ];
            $users[30+$i] = [
                'id'             => 30+$i,
                'name'           => 'Dealer'.$i,
                'email'          => 'dealer'.$i.'@gmail.com',
                'password'       => bcrypt('password'),
                'remember_token' => null,
            ];
            $users[40+$i] = [
                'id'             => 30+$i,
                'name'           => 'Student'.$i,
                'email'          => 'student'.$i.'@gmail.com',
                'password'       => bcrypt('password'),
                'remember_token' => null,
            ];
            $users[50+$i] = [
                'id'             => 40+$i,
                'name'           => 'Student'.$i,
                'email'          => 'student'.$i.'@gmail.com',
                'password'       => bcrypt('password'),
                'remember_token' => null,
            ];
        }
        User::insert($users);
    }
}
