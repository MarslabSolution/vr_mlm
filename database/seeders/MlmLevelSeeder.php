<?php

namespace Database\Seeders;

use App\Models\MlmLevel;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MlmLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mlmLevels = [];
        $local_date = Carbon::parse('2021-11-27 10:00:00');

        for ($i=0, $upline = 0; $i <= 19; $i++) {
            if($i > 5) $upline++;
            $path = $this->getPath($upline);
            $local_date->addDays(9);
            $mlmLevels[$i] = [
                'id' => $i + 1,
                'position' => $i <= 5 ? null : ($i < 10 ? 1 : 0),
                'path' => $path,
                'level' => $i <= 5 ? 0 : count(explode('/', $path)),
                'children_count' => $i <= 3 ? 1 : 0,
                'user_id' => 20+$i,
                'current_plan_id' => $i < 10 ? 2 : 1,
                'up_line_id' => $i <= 5 ? null : $upline,
                'created_at' => $local_date->toDateTimeString(),
                'updated_at' => $local_date->toDateTimeString(),
            ];
        }

        MlmLevel::upsert($mlmLevels,
            [
                'position',
                'path',
                'level',
                'children_count',
                'user_id',
                'current_plan_id',
                'up_line_id',
                'created_at',
                'updated_at',
            ],
            ['id'],
        );
    }

    private function getPath($upline) {
        $path = [];
        for ($i=0; $i < $upline/6; $upline -= 6) {
            array_push($path, $upline);
        }
        $path = array_reverse($path);
        return implode('/', $path);
    }
}
