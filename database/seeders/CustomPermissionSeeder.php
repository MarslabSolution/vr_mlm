<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\SsoClient\Entities\Permission;

class CustomPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'commission', 'commission_statement', 'commission_type_statement', 
            'agent_plan', 'agent_student', 
            'dealer', 'dealer_downline', 'franchiser', 'agent'
        ];
        $actions = ['access', 'create', 'show', 'edit', 'delete'];
        foreach ($permissions as $permission) {
            foreach ($actions as $action) {
                Permission::firstOrCreate([
                    'title' => $permission . '_' . $action,
                ]);
            }
        }
    }
}
