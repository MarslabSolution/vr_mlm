<?php

namespace Database\Seeders;

use App\Models\AgentPlan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Modules\SsoClient\Entities\Role;

class AgentPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AgentPlan::insert([
            'name' => 'Dealer',
            'price' => '300.00',
            'commissionable_level' => 1,
            'roles_id' => Role::where('title', 'Dealer')->first()->id ?? null,
        ]);
        AgentPlan::insert([
            'name' => 'Franchiser',
            'price' => '8000.00',
            'commissionable_level' => 2,
            'roles_id' => Role::whereIn('title', ['Franchiser', 'Franchiser'])->first()->id ?? null,
        ]);
        AgentPlan::insert([
            'name' => 'Assistant Supervisor',
            'price' => '26000.00',
            'commissionable_level' => 2,
            'roles_id' => Role::where('title', 'Assistant Supervisor')->first()->id ?? null,
        ]);
    }
}
