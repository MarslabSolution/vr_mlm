<?php

// way to use
// - config('constants.dealer.lastest_dealer_register');

return [
    'dealer' => [
        'lastest_dealer_register' => 5,
        'minimum_temp' => 5,
        'deposit_min_temp' => 5,
    ],
    'franchiser' => [
        'group_franchiser_maximum' => 155,
        'franchiser_downline_maximum' => 5,
        'franchiser_downline_found' => 6,
    ],
    'week' => 7,
    'month' => 30,
    'dealer_date_start' => 26,
    'dealer_date_end' => 25,
    'commission_month_of_show_temp' => 6,
    'gradient_relstionship_select' => [
        'Mother'      => 'Mother',
        'Father'      => 'Father',
        'Grandma'     => 'Grandma',
        'Grandfather' => 'Grandfather',
        'Brother'     => 'Brother',
        'Sister'      => 'Sister',
        'Guardian'    => 'Guardian',
    ],
    'gender_select' => [
        'Male'   => 'Male',
        'Female' => 'Female',
        'NotWillingToTell' => 'Not willing to tell',
    ],
];