<?php

return [
    'date_format'         => 'd/m/Y',
    'time_format'         => 'H:i:s',
    'primary_language'    => 'en',
    'available_languages' => [
        'en'      => 'English',
        'ms-my'      => 'Malay',
        'zh-cn' => 'Chinese',
    ],
];
