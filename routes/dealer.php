<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'DealerHomeController@index')->name('home');
Route::get('/home/getLatestMonthCommission', 'DealerHomeController@getLatestMonthCommission')->name('home.getLatestMonthCommission');
Route::get('/home/getLastMonthDealerStudent', 'DealerStudentController@agentStudentInMonth')->name('home.getLastMonthDealerStudent');

// Dealer Student
Route::get('dealer-students', 'DealerStudentController@index')->name('dealer-students.index');
Route::get('dealer-students/in-month', 'DealerStudentController@agentStudentInMonth')->name('dealer-students.agentStudentInMonth');
Route::post('dealer-students/by-date', 'DealerStudentController@agentStudentByDate')->name('dealer-students.agentStudentByDate');

// Dealer Commission
Route::get('dealer-commission-history', 'DealerCommissionHistoryController@index')->name('dealer-commission-history.index');
Route::post('dealer-commission-history/generateTable', 'DealerCommissionHistoryController@generateTable')->name('dealer-commission-history.generateTable');
Route::post('dealer-commission-history/date-filter', 'DealerCommissionHistoryController@update')->name('dealer-commission-history.update');

// Dealer Downline
Route::get('dealer-downline', 'DealerDownlineController@index')->name('dealer-downline.index');
Route::post('dealer-downline/date-filter', 'DealerDownlineController@updateByDate')->name('dealer-downline.updateByDate');
Route::post('dealer-downline/potential-downline', 'DealerDownlineController@potentialDownline')->name('dealer-downline.potentialDownline');

// Dealer Profile
Route::get('dealer-profile', 'DealerProfileController@index')->name('dealer-profile.index');
Route::get('password', 'DealerProfileController@edit')->name('password.edit');
Route::post('password', 'DealerProfileController@update')->name('password.update');
Route::post('profile', 'DealerProfileController@updateProfile')->name('password.updateProfile');
Route::post('profile/destroy', 'DealerProfileController@destroy')->name('password.destroyProfile');
Route::post('profile/two-factor', 'DealerProfileController@toggleTwoFactor')->name('password.toggleTwoFactor');

// Dealer Deposit
Route::get('dealer-deposit', 'DealerDepositController@index')->name('dealer-deposit.index');

// Dealer Upgrade Plan
Route::get('dealer-upgrade-plan', 'DealerUpgradePlanController@index')->name('dealer-upgrade-plan.index');

// Dealer Commission
Route::get('dealer-commission-report/daily/{date?}', 'DealerCommissionReportController@daily')->name('dealer-commission-report.daily');
Route::get('dealer-commission-report/weekly/{date?}', 'DealerCommissionReportController@weekly')->name('dealer-commission-report.weekly');
Route::get('dealer-commission-report/monthly/{date?}', 'DealerCommissionReportController@monthly')->name('dealer-commission-report.monthly');
Route::resource('dealer-commission-report', 'DealerCommissionReportController', ['except' => ['create', 'store', 'edit', 'update', 'show', 'destroy']]);

// Route::get('system-calendar', 'SystemCalendarController@index')->name('systemCalendar');

// Route::get('global-search', 'GlobalSearchController@search')->name('globalSearch');
