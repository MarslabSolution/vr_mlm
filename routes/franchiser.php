<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'FranchiserHomeController@index')->name('home');
Route::post('/home/getCommission', 'FranchiserHomeController@getCommission')->name('home.getCommission');
Route::post('/home/getLatestMonthCommission', 'FranchiserHomeController@getLatestMonthCommission')->name('home.getLatestMonthCommission');

// Franchiser Student
Route::get('franchiser-students', 'FranchiserStudentController@index')->name('franchiser-students.index');
Route::get('franchiser-students/in-month', 'FranchiserStudentController@agentStudentInMonth')->name('franchiser-students.agentStudentInMonth');
Route::post('franchiser-students/by-date', 'FranchiserStudentController@agentStudentByDate')->name('franchiser-students.agentStudentByDate');

// Franchiser Commission
Route::get('franchiser-commission-history', 'FranchiserCommissionHistoryController@index')->name('franchiser-commission-history.index');
Route::post('franchiser-commission-history/generateTable', 'FranchiserCommissionHistoryController@generateTable')->name('franchiser-commission-history.generateTable');
Route::post('franchiser-commission-history/date-filter', 'FranchiserCommissionHistoryController@update')->name('franchiser-commission-history.update');

// Franchiser Downline
Route::get('franchiser-downline', 'FranchiserDownlineController@index')->name('franchiser-downline.index');
Route::post('franchiser-downline/date-filter', 'FranchiserDownlineController@update')->name('franchiser-downline.update');

// Franchiser Agent
Route::get('franchiser-agent/create', 'FranchiserAgentController@create')->name('franchiser-agent.create');
Route::post('franchiser-agent/store', 'FranchiserAgentController@store')->name('franchiser-agent.store');
Route::get('franchiser-agent/edit', 'FranchiserAgentController@edit')->name('franchiser-agent.edit');
Route::put('franchiser-agent/update', 'FranchiserAgentController@update')->name('franchiser-agent.update');
Route::post('franchiser-agent/searchDownline', 'FranchiserAgentController@searchDownline')->name('franchiser-agent.searchDownline');

// Franchiser Profile
Route::get('franchiser-profile', 'FranchiserProfileController@index')->name('franchiser-profile.index');
Route::get('password', 'FranchiserProfileController@edit')->name('password.edit');
Route::post('password', 'FranchiserProfileController@update')->name('password.update');
Route::post('profile', 'FranchiserProfileController@updateProfile')->name('password.updateProfile');
Route::post('profile/destroy', 'FranchiserProfileController@destroy')->name('password.destroyProfile');
Route::post('profile/two-factor', 'FranchiserProfileController@toggleTwoFactor')->name('password.toggleTwoFactor');
