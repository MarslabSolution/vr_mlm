<?php

use Illuminate\Support\Facades\Route;
// Settings
//Route::resource('/{type?}', 'SettingsController');
Route::group(['prefix' => 'settings', 'as' => 'settings.', 'middleware' => ['can:setting_access']], function () {
    Route::delete('destroy', 'SettingsController@massDestroy')->name('massDestroy');
    Route::prefix('{setting}/{group?}')->group(function () {
        Route::get('edit', 'SettingsController@edit')->name('edit');
        Route::get('show', 'SettingsController@show')->name('show');
        Route::match(['put', 'patch'], 'update', 'SettingsController@update')->name('update');
        Route::delete('delete', 'SettingsController@destroy')->name('destroy');
    });
    Route::prefix('{group?}')->group(function () {
        Route::get('/', 'SettingsController@index')->name('index');
        Route::get('create', 'SettingsController@create')->name('create');
        Route::post('store', 'SettingsController@store')->name('store');
    });
    Route::get('{condition?}/clear-cache', 'SettingsController@clearCache')->name('clear-cache');
    Route::post('{condition?}/save-settings', 'SettingsController@saveConditionSettings')->name('save-settings');
});

// Agent
Route::group(['prefix' => 'agents/{type}', 'as' => 'agents.'], function () {
    Route::get('/', 'AgentsController@index')->name('index');
    Route::get('create', 'AgentsController@create')->name('create');
    Route::post('store', 'AgentsController@store')->name('store');
    /* Route::prefix('{user}')->group(function () {
        Route::get('edit', 'AgentsController@edit')->name('edit');
        Route::match(['put', 'patch'], 'update', 'AgentsController@update')->name('update');
    }); */
});
Route::group(['prefix' => 'agents/{user}', 'as' => 'agents.'], function () {
    Route::prefix('{type?}')->group(function () {
        Route::get('edit', 'AgentsController@edit')->name('edit');
        Route::match(['put', 'patch'], 'update', 'AgentsController@update')->name('update');
    });
});
// Commission
Route::group(['prefix' => 'commissions', 'as' => 'commissions.', 'middleware' => ['can:commission_access']], function () {
    Route::delete('destroy', 'CommissionController@massDestroy')->name('massDestroy');
    Route::post('parse-csv-import', 'CommissionController@parseCsvImport')->name('parseCsvImport');
    Route::post('process-csv-import', 'CommissionController@processCsvImport')->name('processCsvImport');
    Route::post('getCommissionFields', 'CommissionController@returnCommissionFields')->name('returnCommissionFields');
    Route::prefix('{commission}/{type?}')->group(function () {
        Route::get('edit', 'CommissionController@edit')->name('edit');
        Route::get('show', 'CommissionController@show')->name('show');
        Route::match(['put', 'patch'], 'update', 'CommissionController@update')->name('update');
        Route::delete('delete', 'CommissionController@destroy')->name('destroy');
    });
    Route::prefix('{type?}')->group(function () {
        Route::get('/', 'CommissionController@index')->name('index');
        Route::get('create', 'CommissionController@create')->name('create');
        Route::post('store', 'CommissionController@store')->name('store');
    });
}); //Route::resource('commissions/{type?}', 'CommissionController');

// Commission Statement
Route::group(['prefix' => 'commission-statements/{type}', 'as' => 'commission-statements.'], function () {
    Route::get('/', 'CommissionStatementController@index')->name('index');
});