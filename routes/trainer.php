<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'TrainerController@index')->name('home');

Route::get('students', 'StudentsController@create')->name('students.create');
Route::get('students/studentType={student_type}/{mlm_detail}', 'StudentsController@postMlmDetail')->name('students.postMlmDetail');
Route::get('students/studentType={student_type}/{mlm_detail}/{student_detail}', 'StudentsController@postStudentDetail')->name('students.postStudentDetail');
Route::post('students/store', 'StudentsController@store')->name('students.store');
Route::post('students/search-student-detail', 'StudentsController@searchStudentDetail')->name('students.search-student-detail');


Route::get('profile', 'ChangePasswordController@edit')->name('profile.edit');
Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
Route::post('password', 'ChangePasswordController@update')->name('password.update');
Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
Route::post('profile/two-factor', 'ChangePasswordController@toggleTwoFactor')->name('password.toggleTwoFactor');
