<?php

namespace App\Repositories\Settings;

use App\Models\ThemeSettings;
use Illuminate\Database\Eloquent\Builder;
use Spatie\LaravelSettings\SettingsRepositories\DatabaseSettingsRepository as DefaultSettingRepository;

class ThemeSettingsRepository extends DefaultSettingRepository
{
    /** @var class-string<\Illuminate\Database\Eloquent\Model> */
    protected string $propertyModel;

    protected ?string $connection;

    protected ?string $table;

    public function __construct(array $config)
    {
        $this->propertyModel = ThemeSettings::class;
        $this->connection = $config['connection'] ?? null;
        $this->table = $config['table'] ?? null;
    }
}
