<?php

namespace App\Listeners;

use App\Events\AutoUpgradeFranchiser;
use App\Models\MlmLevel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class TriggerUpgradeFranchiser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\AutoUpgradeFranchiser  $event
     * @return void
     */
    public function handle(AutoUpgradeFranchiser $event)
    {
        //Log::debug('TriggerUpgradeFranchiser'.MlmLevel::where('path', 'LIKE', '%'.$event->franchiser->id.'%'));
    }
}
