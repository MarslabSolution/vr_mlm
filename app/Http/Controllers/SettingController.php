<?php

namespace App\Http\Controllers;

use App\Http\Requests\GeneralSettingsRequest;
use App\Models\GeneralSettings;
use Closure;

class SettingController extends Controller
{
    public function __invoke(GeneralSettings $settings, GeneralSettingsRequest $request){
        $settings->site_name = $request->input('site_name');
        $settings->site_active = $request->boolean('site_active');
        
        $settings->save();
        
        return back();
    }
    public function handle($request, Closure $next)
    {
        $request->merge([
            "payload"=> json_encode($request->input('value', '')),
        ]);
        unset($request['value']);

        return $next($request);
    }
}