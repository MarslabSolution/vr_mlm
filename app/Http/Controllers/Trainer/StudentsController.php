<?php

namespace App\Http\Controllers\Trainer;

use App\Events\TuitionPackageCommission;
use App\Http\Controllers\Controller;
use App\Http\Requests\Trainer\StoreStudentRequest;
use App\Models\Address;
use App\Models\AgentPlan;
use App\Models\AgentStudent;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Modules\SsoClient\Entities\Role;
use Modules\SsoClient\Entities\User;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('trainer.student.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $students = Role::where('title', 'Student')->first()->users->pluck('email')->toArray();
        $agents_ary = Role::where('title', 'Dealer')->orWhere('title', 'Franchiser')->get();

        $agents = [];

        foreach($agents_ary as $index => $agent){
            foreach($agent->users->pluck('email', 'id') as $id => $email){
                $agents[$id] = $email;
            }
        }

        $students = array_filter($students);
        return view(
            'trainer.student.create',
            compact('students', 'agents')
        );
    }

    public function postMlmDetail(Request $request, $student_type, $mlm_detail, $back_value = null){
        try {
            // ------------------------------ validation ------------------------------
            $errors = [];

            if ($student_type != 'newStudent' && $student_type != 'oldStudent'){
                $errors['student_type'] = trans('validation.invalid_student_type');
            }

            if($mlm_detail == null){
                $errors['mlm_detail'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.mlm_detail'));
            }else{
                $mlm_detail_decode = json_decode($mlm_detail, true);

                if (!array_key_exists('tuition_package_id', $mlm_detail_decode) || $mlm_detail_decode['tuition_package_id'] == null){
                    $errors['tuition_package'] = sprintf(trans('validation.data_are_required'), trans('cruds.packagesCommission.fields.tuition_package_efk'));
                }

                if (!array_key_exists('referral_id', $mlm_detail_decode) || $mlm_detail_decode['referral_id'] == null){
                    $errors['referral'] = sprintf(trans('validation.data_are_required'), trans('cruds.student.fields.referral_id'));
                }
            }

            if(count($errors) > 0){
                return back()->withErrors($errors);
            }

            if($back_value != null){
                $home_address = $back_value['home_address'] ? new Address($back_value['home_address']) : null;
                $mail_address = $back_value['mail_address'] ? new Address($back_value['mail_address']) : null;
                $student_detail = $back_value['student_detail'];
                $back_errors = $back_value['errors'];

                return view('trainer.student.studentDetail.create', compact('student_type', 'mlm_detail', 'home_address', 'mail_address', 'student_detail'))->withErrors($back_errors);
            }

            return view('trainer.student.studentDetail.create', compact('student_type', 'mlm_detail'));

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public function postStudentDetail(Request $request, $student_type, $mlm_detail, $student_detail, $back_value = null){
        try {
            // ------------------------------ validation ------------------------------
            $errors = [];
            
            if ($student_type != 'newStudent' && $student_type != 'oldStudent'){
                $errors['student_type'] = trans('validation.invalid_student_type');
            }

            if($mlm_detail == null){
                $errors['mlm_detail'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.mlm_detail'));
            }else{
                $mlm_detail_decode = json_decode($mlm_detail, true);

                if (!array_key_exists('tuition_package_id', $mlm_detail_decode)){
                    $errors['tuition_package'] = sprintf(trans('validation.data_are_required'), trans('cruds.packagesCommission.fields.tuition_package_efk'));
                }

                if (!array_key_exists('referral_id', $mlm_detail_decode)){
                    $errors['referral'] = sprintf(trans('validation.data_are_required'), trans('cruds.student.fields.referral_id'));
                }
            }

            if($student_detail == null){
                $errors['student_detail'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.student_detail'));
            }else{
                $student_detail_decode = json_decode($student_detail, true);

                $student_array_map = $student_detail_decode['student_detail'];
                $home_address_array_map = $student_detail_decode['home_address'];
                $mail_address_array_map = $student_detail_decode['mail_address'];

                // student detail validation
                if (
                    !array_key_exists('student_name', $student_array_map) || trim($student_array_map['student_name']) == ''){
                    $errors['student_name'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.english_name'));
                }
    
                if (!array_key_exists('chinese_name', $student_array_map) || trim($student_array_map['chinese_name']) == ''){
                    $errors['chinese_name'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.chinese_name'));
                }
    
                if (!array_key_exists('student_email', $student_array_map) || trim($student_array_map['student_email']) == ''){
                    $errors['student_email'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.student_email'));
                
                }else if(preg_match('/^\w([\.-]?\w)+@\w+([\.]?\w)+(\.\w{2,3})$/i', $student_array_map['student_email']) == 0){
                    $errors['student_email'] = sprintf(trans('validation.data_invalid_format'), trans('cruds.studentDetail.fields.student_email'));
                
                }else{
                    // create student user account
                    $student_role = Role::where('title', 'Student')->first();

                    $student_account_query = User::where('email', $student_array_map['student_email']);
                    $student_account = $student_account_query->first();

                    // check user account
                    if($student_account != null){
                        $student_account_found = $student_account_query->whereHas('roles', function($roles) use ($student_role) {
                            $roles->where('id', $student_role->id);
                        })->first();
                
                        if($student_account_found != null){
                            $errors['student_email'] = trans('validation.student_account_already_exists');
                        }
                    }
                }
    
                if (!array_key_exists('nric_no', $student_array_map) || trim($student_array_map['nric_no']) == ''){
                    $errors['nric_no'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.nric_no'));
                
                }else if(preg_match('/^([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])-([0-9]{2})-([0-9]{4})$/i', $student_array_map['nric_no']) == 0){
                    $errors['nric_no'] = sprintf(trans('validation.data_invalid_format'), trans('cruds.studentDetail.fields.nric_no'));
                }
    
                if (!array_key_exists('age', $student_array_map) || trim($student_array_map['age']) == ''){
                    $errors['age'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.age'));
                
                }else if(preg_match('/^[0-9]*$/i', $student_array_map['age']) == 0){
                    $errors['age'] = sprintf(trans('validation.data_invalid_format'), trans('cruds.studentDetail.fields.age'));
                }
    
                if (!array_key_exists('school_name', $student_array_map) || trim($student_array_map['school_name']) == ''){
                    $errors['school_name'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.school_name'));
                }
    
                if (!array_key_exists('class_name', $student_array_map) || trim($student_array_map['class_name']) == ''){
                    $errors['class_name'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.class_name'));
                }
    
                if (!array_key_exists('gender', $student_array_map) || trim($student_array_map['gender']) == ''){
                    $errors['gender'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.gender'));
                }

                if (!array_key_exists('student_password', $student_array_map) || trim($student_array_map['student_password']) == ''){
                    $errors['password'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.password'));
                }else if(strlen($student_array_map['student_password']) < 8){
                    $errors['password'] = sprintf(trans('validation.password_length'), '8');
                }
        
                if (!array_key_exists('student_con_password', $student_array_map) || trim($student_array_map['student_con_password']) == ''){
                    $errors['con_password'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.con_password'));
                }

                if (
                    array_key_exists('student_password', $student_array_map) &&
                    array_key_exists('student_con_password', $student_array_map) &&
                    $student_array_map['student_password'] != $student_array_map['student_con_password']
                ){
                    $errors['con_password'] = trans('validation.con_password_not_same');
                }

                // home address validation
                if (!array_key_exists('address_line_1', $home_address_array_map) || trim($home_address_array_map['address_line_1']) == ''){
                    $errors['home_address_line_1'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.address_line_1'));
                }
    
                if (!array_key_exists('city', $home_address_array_map) || trim($home_address_array_map['city']) == ''){
                    $errors['home_city'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.city'));
                }
    
                if (!array_key_exists('state', $home_address_array_map) || trim($home_address_array_map['state']) == ''){
                    $errors['home_state'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.state'));
                }
    
                if (!array_key_exists('postal_code', $home_address_array_map) || trim($home_address_array_map['postal_code']) == ''){
                    $errors['home_postal_code'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.postal_code'));
                
                }else if(preg_match('/^[0-9]{5}$/i', $home_address_array_map['postal_code']) == 0){
                    $errors['home_postal_code'] = sprintf(trans('validation.data_invalid_format'), trans('cruds.address.fields.postal_code'));
                }

                if (!array_key_exists('country', $home_address_array_map) || trim($home_address_array_map['country']) == ''){
                    $errors['home_country'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.country'));
                }

                if (!array_key_exists('phone', $home_address_array_map) || trim($home_address_array_map['phone']) == ''){
                    $errors['home_phone'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.phone'));
                
                }else if(preg_match('/^(01[0-9])-[0-9]{7,8}$/i', $home_address_array_map['phone']) == 0){
                    $errors['home_phone'] = sprintf(trans('validation.data_invalid_format'), trans('cruds.address.fields.phone'));
                }

                // main address validation
                if($mail_address_array_map != null){
                    if (!array_key_exists('address_line_1', $mail_address_array_map) || trim($mail_address_array_map['address_line_1']) == ''){
                        $errors['mail_address_line_1'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.address_line_1'));
                    }
        
                    if (!array_key_exists('city', $mail_address_array_map) || trim($mail_address_array_map['city']) == ''){
                        $errors['mail_city'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.city'));
                    }
        
                    if (!array_key_exists('state', $mail_address_array_map) || trim($mail_address_array_map['state']) == ''){
                        $errors['mail_state'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.state'));
                    }
        
                    if (!array_key_exists('postal_code', $mail_address_array_map) || trim($mail_address_array_map['postal_code']) == ''){
                        $errors['mail_postal_code'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.postal_code'));
                    
                    }else if(preg_match('/^[0-9]{5}$/i', $mail_address_array_map['postal_code']) == 0){
                        $errors['mail_postal_code'] = sprintf(trans('validation.data_invalid_format'), trans('cruds.address.fields.postal_code'));
                    }
    
                    if (!array_key_exists('country', $mail_address_array_map) || trim($mail_address_array_map['country']) == ''){
                        $errors['mail_country'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.country'));
                    }
    
                    if (!array_key_exists('phone', $mail_address_array_map) || trim($mail_address_array_map['phone']) == ''){
                        $errors['mail_phone'] = sprintf(trans('validation.data_are_required'), trans('cruds.address.fields.phone'));
                    
                    }else if(preg_match('/^(01[0-9])-[0-9]{7,8}$/i', $mail_address_array_map['phone']) == 0){
                        $errors['mail_phone'] = sprintf(trans('validation.data_invalid_format'), trans('cruds.address.fields.phone'));
                    }
                }
            }

            if(count($errors) > 0){
                $student_detail_decode = json_decode($student_detail, true);
                $request_home_address = $student_detail_decode['home_address'];
                $request_mail_address = $student_detail_decode['mail_address'];
                $request_student_detail = $student_detail_decode['student_detail'];

                return $this->postMlmDetail($request, $student_type, $mlm_detail, [
                    'home_address' => $request_home_address, 
                    'mail_address' => $request_mail_address, 
                    'student_detail' => $request_student_detail,
                    'errors' => $errors
                ]);
            }

            if($back_value != null){
                $guardian_detail = $back_value['guardian_detail'] ?? null;
                $back_errors = $back_value['errors'];

                return view('trainer.student.guardian.create', compact('student_type', 'mlm_detail', 'student_detail', 'guardian_detail'))->withErrors($back_errors);
            }

            return view('trainer.student.guardian.create', compact('student_type', 'mlm_detail', 'student_detail'));

        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    public function searchStudentDetail(Request $request){
        try{
            $request_data = $request->all();

            $email_query = $request_data['search'];
            $page_query = $request_data['page'];
            $page_query = (int) $page_query;

            $results = [];

            $match_students = User::where('email', 'like', '%' . $email_query . '%')->whereHas('roles', function ($query) {
                $query->where('title', 'Student');
            })->limit($page_query * 10)->get();

            foreach($match_students as $student){
                $results[] = [
                    'id' => $student->id,
                    'text' => $student->email,
                ];
            }

            return [
                "results" => $results,
                "pagination"=> [
                    "more"=> true,
                ]
            ];
        }catch(Exception $e){
            throw $e;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Http\Requests\Trainer\StoreStudentRequest $request
     */
    public function store(Request $request)
    {
        try {
            $request_data = $request->all();
            
            $student_type = $request_data['student_type'];
            $mlm_detail = $request_data['mlm_detail'];

            $student_id = null;

            if($student_type == "oldStudent"){
                $student_id = $request_data['student_detail_id'];
            }else if($student_type == "newStudent"){
                $student_detail = $request_data['student_detail'];
                $guardian_address = [];

                // ------------------------------ validation ------------------------------
                $errors = [];

                if ($mlm_detail == null){
                    $errors['mlm_detail'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.mlm_detail'));
                }

                if ($student_detail == null){
                    $errors['student_detail'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.student_detail'));
                }

                for($i = 0; $i < count($request_data['total_guardian']); $i++){
                    $guardian_address['parent_'. $i .'_address'] = new Address();

                    if(!array_key_exists('parent_name_'. $i, $request_data) || trim($request_data['parent_name_'. $i]) == ''){
                        $errors['parent_name_'. $i] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.studentDetail.fields.english_name'));
                    }
                    
                    if(!array_key_exists('parent_chinese_name_'. $i, $request_data) || trim($request_data['parent_chinese_name_'. $i]) == ''){
                        $errors['parent_chinese_name_'. $i] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.studentDetail.fields.chinese_name'));
                    }

                    if (!array_key_exists('parent_email_'. $i, $request_data) || trim($request_data['parent_email_'. $i]) == ''){
                        $errors['parent_email_'. $i] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.studentDetail.fields.student_email'));
                    
                    }else if(preg_match('/^\w([\.-]?\w)+@\w+([\.]?\w)+(\.\w{2,3})$/i', $request_data['parent_email_'. $i]) == 0){
                        $errors['parent_email_'. $i] = sprintf(trans('validation.data_invalid_format'), trans('cruds.studentDetail.fields.student_email'));
                    }

                    if(!array_key_exists('parent_nric_no_'. $i, $request_data) || trim($request_data['parent_nric_no_'. $i]) == ''){
                        $errors['parent_nric_no_'. $i] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.studentDetail.fields.nric_no'));
                    
                    }else if(preg_match('/^([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])-([0-9]{2})-([0-9]{4})$/i', $request_data['parent_nric_no_'. $i]) == 0){
                        $errors['parent_nric_no_'. $i] = sprintf(trans('validation.data_invalid_format'), trans('cruds.studentDetail.fields.nric_no'));
                    }
                    
                    if(!array_key_exists('parent_occupation_'. $i, $request_data) || trim($request_data['parent_occupation_'. $i]) == ''){
                        $errors['parent_occupation_'. $i] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.studentDetail.fields.occupation'));
                    }

                    if(!array_key_exists('parent_relationship_'. $i, $request_data) || trim($request_data['parent_relationship_'. $i]) == ''){
                        $errors['parent_relationship_'. $i] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.studentDetail.fields.relationship'));
                    }

                    if(!array_key_exists('parent_'. $i .'_address_line_1', $request_data) || trim($request_data['parent_'. $i .'_address_line_1']) == ''){
                        $errors['parent_'. $i .'_address_line_1'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.address.fields.address_line_1'));
                    }

                    if(!array_key_exists('parent_'. $i .'_city', $request_data) || trim($request_data['parent_'. $i .'_city']) == ''){
                        $errors['parent_'. $i .'_city'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.address.fields.city'));
                    }

                    if(!array_key_exists('parent_'. $i .'_state', $request_data) || trim($request_data['parent_'. $i .'_state']) == ''){
                        $errors['parent_'. $i .'_state'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.address.fields.state'));
                    }

                    if(!array_key_exists('parent_'. $i .'_postal_code', $request_data) || trim($request_data['parent_'. $i .'_postal_code']) == ''){
                        $errors['parent_'. $i .'_postal_code'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.address.fields.postal_code'));
                    
                    }else if(preg_match('/^[0-9]{5}$/i', $request_data['parent_'. $i .'_postal_code']) == 0){
                        $errors['parent_'. $i .'_postal_code'] = sprintf(trans('validation.data_invalid_format'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.address.fields.postal_code'));
                    }

                    if(!array_key_exists('parent_'. $i .'_country', $request_data) || trim($request_data['parent_'. $i .'_country']) == ''){
                        $errors['parent_'. $i .'_country'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.address.fields.country'));
                    }

                    if(!array_key_exists('parent_'. $i .'_phone', $request_data) || trim($request_data['parent_'. $i .'_phone']) == ''){
                        $errors['parent_'. $i .'_phone'] = sprintf(trans('validation.data_are_required'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.address.fields.phone'));
                    
                    }else if(preg_match('/^(01[0-9])-[0-9]{7,8}$/i', $request_data['parent_'. $i .'_phone']) == 0){
                        $errors['parent_'. $i .'_phone'] = sprintf(trans('validation.data_invalid_format'), trans('cruds.studentDetail.fields.guardian').' '. $i+1 .' '.trans('cruds.address.fields.phone'));
                    }

                    $guardian_address['parent_'. $i .'_address']->address_line_1 = $request_data['parent_'. $i .'_address_line_1'];
                    $guardian_address['parent_'. $i .'_address']->address_line_2 = $request_data['parent_'. $i .'_address_line_2'];
                    $guardian_address['parent_'. $i .'_address']->city = $request_data['parent_'. $i .'_city'];
                    $guardian_address['parent_'. $i .'_address']->state = $request_data['parent_'. $i .'_state'];
                    $guardian_address['parent_'. $i .'_address']->postal_code = $request_data['parent_'. $i .'_postal_code'];
                    $guardian_address['parent_'. $i .'_address']->country = $request_data['parent_'. $i .'_country'];
                    $guardian_address['parent_'. $i .'_address']->phone = $request_data['parent_'. $i .'_phone'];
                }

                if(count($errors) > 0){
                    return $this->postStudentDetail($request, $student_type, $mlm_detail, $student_detail, [
                        'guardian_detail' => [
                            ...$request_data,
                            ...$guardian_address,
                        ],
                        'errors' => $errors
                    ]);
                }
                
                $student_detail = json_decode($student_detail, true);
                $mlm_detail = json_decode($mlm_detail, true);

                $api_url = settings('main_system_domain', '', 'api') . settings('api_version', '', 'api');

                $response = Http::withToken(access_token())
                    ->withHeaders([
                        'Accept' => 'application/json',
                        'Content-Type' => 'application/json',
                        'X-VRDRUM-USER' => auth()->user()->email,
                    ])
                    ->post($api_url . 'student-details', [
                        'student_detail' => $student_detail,
                        'guardian_detail' => [
                            ...$request_data,
                            ...$guardian_address,
                        ],
                        'referral_id' => $mlm_detail['referral_id'],
                    ]);

                if ($response->status() != 201) {
                    return $this->postStudentDetail($request, $student_type, $mlm_detail, $student_detail, [
                        'guardian_detail' => [
                            ...$request_data,
                            ...$guardian_address,
                        ],
                        'errors' => [
                            'Error creating student' => $response->json()['message'],
                        ]
                    ]);
                }

                $response_data = $response->json()['data'];
                $student_id = $response_data['user_id'];
            }

            DB::beginTransaction();

            $agent_student = new AgentStudent();
            $agent_student->tuition_package_efk =  $mlm_detail['tuition_package_id'];
            $agent_student->student_id = $student_id;
            $agent_student->referral_id = $mlm_detail['referral_id'];
            $agent_student->trainer_id = auth()->user()->id;
            $agent_student->save();

            DB::commit();
            event(new TuitionPackageCommission($mlm_detail['referral_id'], $request->tuition_package_price));
            session()->flash('message', 'Student created successfully');

            return redirect()->route('trainer.students.create');

        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('trainer.students.show');
    }
}
