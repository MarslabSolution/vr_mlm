<?php

namespace App\Http\Controllers\Trainer;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\FranchiserServiceTrait;
use App\Http\Controllers\Traits\MlmLevelDownlineTrait;
use App\Http\Controllers\Traits\MonthRangeTrait;
use App\Models\AgentStudent;
class TrainerController extends Controller
{   
    use MlmLevelDownlineTrait;
    use MonthRangeTrait;
    use FranchiserServiceTrait;
    
    public function index()
    {   
        $current_month_api = AgentStudent::where('trainer_id', auth()->user()->id)->count();

        return view('trainer.home', compact('current_month_api'));
    }

}
