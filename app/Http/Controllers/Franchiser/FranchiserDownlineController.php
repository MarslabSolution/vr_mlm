<?php

namespace App\Http\Controllers\Franchiser;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\FranchiserServiceTrait;
use App\Http\Controllers\Traits\MlmLevelDownlineTrait;
use App\Http\Controllers\Traits\MonthRangeTrait;
use App\Models\AgentStudent;
use App\Models\MlmLevel;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FranchiserDownlineController extends Controller
{
    use MlmLevelDownlineTrait;
    use MonthRangeTrait;
    use FranchiserServiceTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $login_user = auth()->user();
        $user_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        $level_downlines = [];
        $tol_level = $this->getCommissionableLevel();

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date_now = Carbon::now();
        $date_oldest = Carbon::now();

        $commissions = $this->getFranchiseCommissions();

        for($i = 0; $i <= $tol_level; $i++){
            $downlines = collect();

            if($user_level != null){
                $downlines = $this->getDownlineByLevel($user_level->id, $tol_level, $i == 0 ? null : $i);
            }

            $tol_commission = 0;
            
            foreach($downlines as $index => $value){
                $students = AgentStudent::where('referral_id', $value->user->id)->get();

                $student_count = 0;

                if(count($students) > 0){
                    $student_count = count($students);
                }

                $downlines[$index]->id = $value->id ? $value->id : '';
                $downlines[$index]->user_name = $value->user ? $value->user->name : '';
                $downlines[$index]->current_plan_name = $value->current_plan ? $value->current_plan->name : '';
                $downlines[$index]->student_count = $student_count;
                $downlines[$index]->register_at = $value->created_at ? $value->created_at->format('Y-m-d') : '';
                $downlines[$index]->cumulative_commission = $student_count * $commissions[$value->level - $user_level->level];

                $tol_commission += $student_count * $commissions[$value->level - $user_level->level];

                if($date_oldest->toDateTimeString() > $value->created_at->format('Y-m-d H:i:s')){
                    $date_oldest = Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at->format('Y-m-d H:i:s'));
                }
            }

            $level_downlines[$i] = [
                'downlines' => $downlines,
                'commission' => $tol_commission,
            ];
        }

        $date_range = $this->getDateRange($date_now, config('constants.dealer_date_start'), config('constants.dealer_date_end'));
        $date_from = $date_range['from'];
        $date_to = $date_range['to'];
        
        $month_select = [];
        array_push($month_select, [
            'from' => $this->getDateRange($date_oldest, config('constants.dealer_date_start'), config('constants.dealer_date_end'))['from']->toDateTimeString(),
            'to'   => $this->getDateRange($date_now, config('constants.dealer_date_start'), config('constants.dealer_date_end'))['to']->toDateTimeString(),
        ]);
        array_push($month_select, [
            'from' => $date_from->toDateTimeString(),
            'to'   => $date_to->toDateTimeString(),
        ]);
        
        while(!($date_range['from']->toDateTimeString() <= $date_oldest->toDateTimeString() && $date_oldest->toDateTimeString() <= $date_range['to']->toDateTimeString())){
            $date_from->subMonth();
            $date_to->subMonth();
            
            array_push($month_select, [
                'from' => $date_from->toDateTimeString(),
                'to'   => $date_to->toDateTimeString(),
            ]);
        }
        
        return view('franchiser.franchiserDownline.index', compact('month_select', 'level_downlines'));
    }

    public function update(Request $request)
    {
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $date_from = $request['from'];
        $date_to = $request['to'];

        if($date_from == null || $date_to == null){
            return;
        }

        $login_user = auth()->user();
        $user_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        $level_downlines = [];
        $tol_level = $this->getCommissionableLevel();

        $commissions = $this->getFranchiseCommissions();

        for($i = 0; $i <= $tol_level; $i++){
            $downlines = collect();

            if($user_level != null){
                $downlines = $this->getDownlineByLevel($user_level->id, $tol_level, $i == 0 ? null : $i);
            }
            
            $tol_commission = 0;
            
            foreach($downlines as $index => $value){
                $students = AgentStudent::where([
                    ['referral_id', '=', $value->user->id],
                    ['created_at', '>=', $date_from],
                    ['created_at', '<', $date_to],
                ])->get();

                $student_count = 0;

                if(count($students) > 0){
                    $student_count = count($students);
                }

                $downlines[$index]->id = $value->id ? $value->id : '';
                $downlines[$index]->user_name = $value->user ? $value->user->name : '';
                $downlines[$index]->current_plan_name = $value->current_plan ? $value->current_plan->name : '';
                $downlines[$index]->student_count = $student_count;
                $downlines[$index]->register_at = $value->created_at ? $value->created_at->format('Y-m-d') : '';
                $downlines[$index]->cumulative_commission = $student_count * $commissions[$downlines[$index]->level - $user_level->level];

                $tol_commission += $student_count * $commissions[$downlines[$index]->level - $user_level->level];
            }

            $level_downlines[$i] = [
                'downlines' => $downlines,
                'commission' => $tol_commission,
            ];
        }

        return $level_downlines;
    }
}
