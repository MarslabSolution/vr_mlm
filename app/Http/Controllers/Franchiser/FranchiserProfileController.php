<?php

namespace App\Http\Controllers\Franchiser;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Modules\SsoClient\Http\Requests\UpdatePasswordRequest;
use Modules\SsoClient\Http\Requests\UpdateProfileRequest;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FranchiserProfileController extends Controller
{
    public function index(Request $request)
    {
        $login_user = auth()->user()->load(['roles']);

        $roles = [];
        foreach($login_user->roles as $index => $value){
            $roles[$index] = $value->title;
        }
        $roles = implode(', ', $roles);

        $language = 'en';

        return view('franchiser.franchiserProfile.index', compact('login_user', 'roles', 'language'));
    }

    public function edit()
    {
        abort_if(Gate::denies('profile_password_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('franchiser.franchiserProfile.edit');
    }

    public function update(UpdatePasswordRequest $request)
    {
        auth()->user()->update($request->validated());

        return redirect()->route('franchiser.password.edit')->with('message', __('global.change_password_success'));
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $user = auth()->user();

        $user->update($request->validated());

        return redirect()->route('franchiser.password.edit')->with('message', __('global.update_profile_success'));
    }

    public function destroy()
    {
        $user = auth()->user();

        $user->update([
            'email' => time() . '_' . $user->email,
        ]);

        $user->delete();

        return redirect()->route('login')->with('message', __('global.delete_account_success'));
    }

    public function toggleTwoFactor(Request $request)
    {
        $user = auth()->user();

        if ($user->two_factor) {
            $message = __('global.two_factor.disabled');
        } else {
            $message = __('global.two_factor.enabled');
        }

        $user->two_factor = !$user->two_factor;

        $user->save();

        return redirect()->route('franchiser.password.edit')->with('message', $message);
    }
}
