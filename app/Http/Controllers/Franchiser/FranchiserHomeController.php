<?php

namespace App\Http\Controllers\Franchiser;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\FranchiserServiceTrait;
use App\Http\Controllers\Traits\MlmLevelDownlineTrait;
use App\Http\Controllers\Traits\MonthRangeTrait;
use App\Models\AgentStudent;
use App\Models\MlmLevel;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FranchiserHomeController extends Controller
{
    use MlmLevelDownlineTrait;
    use MonthRangeTrait;
    use FranchiserServiceTrait;

    public function index(Request $request){
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $login_user = auth()->user()->load(['roles']);

        $user_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        $tol_level = $this->getCommissionableLevel();

        $franchiser_8k = collect();

        $user_downlines = collect();

        if($user_level != null){
            $user_downlines = $this->getDownlineByLevel($user_level->id, $tol_level);
        }
        
        foreach($user_downlines as $index => $value){
            if($value->current_plan->name == 'Franchiser'){
                $franchiser_8k = $franchiser_8k->push($value);
            }
        }

        $months = [];
        for($i = config('constants.commission_month_of_show_temp'); $i > 0; $i--){
            $months[$i] = $i;
        }

        $levels = [ 0 => 'ALL' ];
        for($i = 1; $i <= $tol_level; $i++){
            $levels[$i] = $i;
        }

        return view('franchiser.home', compact('login_user', 'franchiser_8k', 'months', 'levels'));
    }

    public function getCommission(Request $request){
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $request_month = $request['month'];
        $request_level = $request['level'];

        if($request_month == null || $request_level == null){
            return;
        }

        date_default_timezone_set("Asia/Kuala_Lumpur");

        $login_user = auth()->user()->load(['roles']);
        $user_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        $user_downlines = collect();

        if($user_level != null){
            $user_downlines = $this->getDownlineByLevel($user_level->id,  $user_level->current_plan->commissionable_level, $request_level != 'ALL' ? $request_level : null);
        }

        $date_range = $this->getDateRange(Carbon::now(), config('constants.dealer_date_start'), config('constants.dealer_date_end'));
        $date_from = $date_range['from'];
        $date_to = $date_range['to'];
        
        $date_from->subMonths($request_month);

        $downline_commission = 0;
        $user_commission = 0;
        $join_commission = 0;
        $downline_student = 0;
        $user_student = 0;

        $student_commissions = $this->getFranchiseCommissions();
        $join_commissions = $this->getJoinCommissions();

        // to get login user downline student
        foreach($user_downlines as $index => $value){
            $downline_students = AgentStudent::where([
                ['referral_id', '=', $value->user_id],
                ['created_at', '>=', $date_from->toDateTimeString()],
                ['created_at', '<', $date_to->toDateTimeString()],
            ])->with(['student'])->get();
            
            if (count($downline_students) > 0){
                $downline_commission += count($downline_students) * $student_commissions[$value->level - $user_level->level];
                $downline_student += count($downline_students);
            }

            if($value->current_plan->name == 'Franchiser'){
                $join_commission += $join_commissions[$value->level - $user_level->level];
            }
        }
        
        // to get login user student
        $user_students = AgentStudent::where([
            ['referral_id', '=', $login_user->id],
            ['created_at', '>=', $date_from->toDateTimeString()],
            ['created_at', '<', $date_to->toDateTimeString()],
        ])->with(['student'])->get();

        if (count($user_students) > 0){
            $user_commission += count($user_students) * $student_commissions[0];
            $user_student += count($user_students);
        }

        return [
            'downline_commission' => $downline_commission,
            'user_commission' => $user_commission,
            'join_commission' => $join_commission,
            'downline_student' => $downline_student,
            'user_student' => $user_student,
        ];
    }

    public function getLatestMonthCommission(Request $request){
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $request_month = $request['month'];
        $request_level = $request['level'];

        if($request_month == null || $request_level == null){
            return;
        }

        date_default_timezone_set("Asia/Kuala_Lumpur");

        $login_user = auth()->user();
        $user_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        $user_downlines = collect();

        if($user_level != null){
            $user_downlines = $this->getDownlineByLevel($user_level->id, $this->getCommissionableLevel(), $request_level != 'ALL' ? $request_level : null);
        }
        
        $date_range = $this->getDateRange(Carbon::now(), config('constants.dealer_date_start'), config('constants.dealer_date_end'));
        $date_from = $date_range['from'];
        $date_to = $date_range['to'];

        // prepare for login user student & downline student
        $total_month = $request_month;

        $half_year_user_students = null;
        $half_year_downline_students = null;
        
        $student_commissions = $this->getFranchiseCommissions();
        $join_commissions = $this->getJoinCommissions();
        
        do{
            // to get login user student
            $user_students = AgentStudent::where([
                ['referral_id', '=', $login_user->id],
                ['created_at', '>=', $date_from->toDateTimeString()],
                ['created_at', '<', $date_to->toDateTimeString()],
            ])->with(['student'])->get();

            if ($half_year_user_students == null){
                $half_year_user_students = $user_students;
            }else{
                $half_year_user_students = $half_year_user_students->merge($user_students);
            }

            // to get login user downline student
            foreach($user_downlines as $index => $value){
                $downline_students = AgentStudent::where([
                    ['referral_id', '=', $value->user_id],
                    ['created_at', '>=', $date_from->toDateTimeString()],
                    ['created_at', '<', $date_to->toDateTimeString()],
                ])->with(['student'])->get();
                
                foreach($downline_students as $index => $downline_student){
                    $downline_students[$index]->in_level = $value->level;
                }

                if ($half_year_downline_students == null){
                    $half_year_downline_students = $downline_students;
                }else{
                    $half_year_downline_students = $half_year_downline_students->merge($downline_students);
                }
            }


            $date_from->subMonth();
            $date_to->subMonth();

            $total_month -= 1;

        } while($total_month >= 0);

        // set data for the chart use
        $total_month = $request_month;
        $chartData = [];
        
        do{
            $date_from->addMonth();
            $date_to->addMonth();
            
            $user_commission = 0;
            $downline_commission = 0;
            $join_commission = 0;

            foreach($half_year_user_students as $index => $value){
                if($date_from->toDateTimeString() <= $value->created_at && $value->created_at < $date_to->toDateTimeString()){
                    $user_commission += $student_commissions[0];
                    unset($half_year_user_students[$index]);
                }
            }
            
            if($half_year_downline_students != null){
                foreach($half_year_downline_students as $index => $value){
                    if($date_from->toDateTimeString() <= $value->created_at && $value->created_at < $date_to->toDateTimeString()){
                        $downline_commission += $student_commissions[$value->in_level - $user_level->level];;
                        unset($half_year_downline_students[$index]);
                    }
                }
            }

            foreach($user_downlines as $index => $value){
                if($value->current_plan->name == 'Franchiser' &&
                    $date_from->toDateTimeString() <= $value->created_at && 
                    $value->created_at < $date_to->toDateTimeString()
                ){
                    $join_commission += $join_commissions[$value->level - $user_level->level];
                    unset($user_downlines[$index]);
                }
            }

            $total_month -= 1;

            if($total_month < 0){
                $date_to = Carbon::now();
            }

            array_push($chartData, [
                'date'   => $date_to->year . '-' . $date_to->month . '-' . $date_to->day,
                'user'     => $user_commission,
                'downline' => $downline_commission,
                'join'     => $join_commission,
            ]);

        }while($total_month >= 0);

        return $chartData;
    }
}
