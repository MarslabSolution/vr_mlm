<?php

namespace App\Http\Controllers\Franchiser;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Controllers\Traits\FranchiserServiceTrait;
use App\Http\Controllers\Traits\MlmLevelDownlineTrait;
use App\Http\Requests\StoreAgentRequest;
use App\Http\Requests\UpdateAgentRequest;
use App\Models\AgentPlan;
use App\Models\MlmLevel;
use Modules\SsoClient\Entities\User;
use Modules\SsoClient\Entities\Role;
use Carbon\Carbon;
use Exception;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Symfony\Component\HttpFoundation\Response;

class FranchiserAgentController extends Controller
{
    use CsvImportTrait;
    use FranchiserServiceTrait;
    use MlmLevelDownlineTrait;

    public function create()
    {
        abort_if(Gate::denies('agent_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $id_collections = Role::where('title', 'Dealer')->orWhere('title', 'Franchiser')->pluck('id');

        $agent_plans = AgentPlan::whereIn('roles_id', $id_collections)->get();

        $user_mlm = MlmLevel::where('user_id', auth()->user()->id)->get();
        
        $downlines = [];

        if($user_mlm->count() > 0){
            $downlines_found = $this->getDownlineByLevel($user_mlm[0]->id, config('constants.franchiser.franchiser_downline_found'));
        
            foreach($agent_plans as $plan_index => $agent_plan){
                $downlines[$agent_plan->id] = [];

                foreach($downlines_found as $downline_index => $downline){
                    if($downline->current_plan_id == $agent_plan->id || $agent_plan->name != 'Franchiser'){
                        $downlines[$agent_plan->id] = [
                            ...$downlines[$agent_plan->id],
                            $downline,
                        ];
                    }
                }
            }
        }

        return view('franchiser.franchiserAgent.create', compact('agent_plans', 'downlines'));
    }

    public function store(StoreAgentRequest $request)
    {
        $upline = MlmLevel::find($request->all()['upline']);

        $url = URL::temporarySignedRoute(
            'franchiser-agent.verify', now()->addMinutes(60), ['dataSource' => urlencode( json_encode([
                ...$request->all(),
                'upline' => $upline->user_id,
            ]) )]
        );

        send_html_email(
            $request->all()['email'], 
            trans('cruds.franchiserAgent.message.agent_verify'), 
            'franchiser.franchiserAgent.emailBody',
            [ 'url' => $url ],
        );

        return redirect()->route('franchiser.franchiser-profile.index')->with('success', trans('cruds.franchiserAgent.message.create_success'));
    }

    public function edit()
    {
        abort_if(Gate::denies('agent_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $upgrade_plans = AgentPlan::where('name', 'Franchiser')->get();

        return view('franchiser.franchiserAgent.edit', compact('upgrade_plans'));
    }

    public function update(UpdateAgentRequest $request)
    {
        $requet_data = $request->all();

        try{
            DB::beginTransaction();

            $downline_id = explode('|', $requet_data['downline'])[0];
            $upgrade_plan_id = $requet_data['upgrade_plan'];

            date_default_timezone_set("Asia/Kuala_Lumpur");
            $date_now = Carbon::now();

            $mlm_account = MlmLevel::find($downline_id);

            $user_level = MlmLevel::where('user_id', auth()->user()->id)->first();

            $free_agent_id = $this->getFreeFranchiserId($user_level->id, $upgrade_plan_id);
            $new_upline = MlmLevel::find($free_agent_id);

            $downline_count = MlmLevel::where([
                ['up_line_id', '=', $new_upline->id],
                ['current_plan_id', '=', $upgrade_plan_id],
            ])->count();

            $position = $downline_count + 1;
            $new_path = $new_upline->path ? $new_upline->path . '/' . $new_upline->id : $new_upline->id;

            if(auth()->user()->id != $free_agent_id){
                $old_downline_path = $mlm_account->path ? $mlm_account->path . '/' . $mlm_account->id : $mlm_account->id;
                $new_downline_path = $new_path . '/' . $mlm_account->id;

                if(count(explode('/', $old_downline_path)) >= count(explode('/', $new_downline_path))){
                    MlmLevel::query()->lockForUpdate()
                    ->where('path', 'LIKE', '%' . $old_downline_path . '%')
                    ->decrement('level', count(explode('/', $old_downline_path)) - count(explode('/', $new_downline_path)), [
                        'path' => DB::raw(sprintf('REPLACE(path, \'%s\', \'%s\')', $old_downline_path, $new_downline_path))
                    ]);

                }else{
                    MlmLevel::query()->lockForUpdate()
                    ->where('path', 'LIKE', '%' . $old_downline_path . '%')
                    ->increment('level', count(explode('/', $new_downline_path)) - count(explode('/', $old_downline_path)), [
                        'path' => DB::raw(sprintf('REPLACE(path, \'%s\', \'%s\')', $old_downline_path, $new_downline_path))
                    ]);
                }
            }

            MlmLevel::create($mlm_account->toArray())->delete();

            $mlm_account->update([
                'up_line_id' => $new_upline->id,
                'current_plan_id' => $upgrade_plan_id,
                'position' => $position,
                'path' => $new_path,
                'level' => $new_upline->level + 1,
                'children_count' => 0,
                'created_at' => $date_now,
                'updated_at' => $date_now,
            ]);

            DB::commit();

            return redirect()->route('franchiser.franchiser-profile.index')->with('success', trans('cruds.franchiserAgent.message.upgrade_success'));
            
        } catch (Exception $e) {
            DB::rollBack();
            return back()->withErrors(['error' => $e->getMessage()]);
        }
    }
    
    public function searchDownline(Request $request)
    {
        $request_data = $request->all();

        $email_query = $request_data['search'];
        $page_query = $request_data['page'];
        $page_query = (int) $page_query;

        $results = [];
        
        $login_user = auth()->user();
        $user_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        if($email_query != '' && $user_level != null){
            $path_query = $user_level->id . '%';
            for($i = 0; $i < $user_level->level; $i++){
                $path_query = '_/' . $path_query;
            }

            $match_user_ids = User::where('email', 'like', '%' . $email_query . '%')->pluck('id');
            $match_downline = MlmLevel::whereIn('user_id', $match_user_ids)->where([
                ['path', 'like', $path_query],
                ['current_plan_id', '=', '1'],
            ])->limit($page_query * 10)->with(['user'])->get();

            foreach($match_downline as $downline){
                $results[] = [
                    'id' => $downline->id . '|' . $downline->user->name,
                    'text' => $downline->user->email,
                ];
            }
        }

        return [
            "results" => $results,
            "pagination"=> [
                "more"=> true,
            ]
        ];
    }

    public function verify(Request $request, $dataSource){
        try {
            DB::beginTransaction();

            $request_data = (array)json_decode($dataSource);

            // ------------------------------ validation ------------------------------
            $errors = [];

            if($request_data == null || !is_array($request_data)){
                $errors['dataSource'] = trans('validation.invalid_data_source');
            
            }else{
                if (!array_key_exists('name', $request_data) || trim($request_data['name']) == ''){
                    $errors['name'] = sprintf(trans('validation.data_are_required'), trans('cruds.franchiserAgent.fields.name'));
                }
    
                if (!array_key_exists('email', $request_data) || trim($request_data['email']) == ''){
                    $errors['email'] = sprintf(trans('validation.data_are_required'), trans('cruds.franchiserAgent.fields.email'));
                }
    
                if (!array_key_exists('password', $request_data) || trim($request_data['password']) == ''){
                    $errors['password'] = sprintf(trans('validation.data_are_required'), trans('cruds.franchiserAgent.fields.password'));
                }
    
                if (!array_key_exists('agent_plan', $request_data) || trim($request_data['agent_plan']) == ''){
                    $errors['agent_plan'] = sprintf(trans('validation.data_are_required'), trans('cruds.franchiserAgent.fields.agent_plan'));
                }
    
                if (!array_key_exists('upline', $request_data) || trim($request_data['upline']) == ''){
                    $errors['upline'] = sprintf(trans('validation.data_are_required'), trans('cruds.franchiserAgent.fields.upline'));
                }
            }
            
            if(count($errors) > 0){
                return view('franchiser.franchiserAgent.verify', compact('errors'));
            }

            $expires = Carbon::createFromTimestamp($request->all()['expires']);
            $data_now = Carbon::now();

            if (!$request->hasValidSignature() || $expires->lt($data_now)) {
                $isExpires = true;
                return view('franchiser.franchiserAgent.verify', compact('isExpires'));
            }

            $upline = User::find($request_data['upline']);

            // abort_if(Gate::denies('agent_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

            $agent_plan = AgentPlan::find($request_data['agent_plan']);

            $role = Role::where('title', $agent_plan->name)->first();

            $request_data['approved'] = true;

            $user_query = User::where('email', $request_data['email']);
            $user = $user_query->first();

            if($user != null){
                $roles_ids = AgentPlan::groupBy('roles_id')->pluck('roles_id');

                $user_found = $user_query->whereHas('roles', function($roles) use ($roles_ids) {
                    $roles->whereIn('id', $roles_ids);
                })->first();

                if($user_found != null){
                    $errors['email'] = trans('validation.agent_account_already_exists');

                    return view('franchiser.franchiserAgent.verify', compact('errors'));
                }else{
                    $user->roles()->sync([
                        ...$user->roles->pluck('id'),
                        $role->id,
                    ]);
                }
            }else{
                // create user account and roles
                $user = User::create($request_data);
                $user->roles()->sync($role->id);
            }

            // create mlm level
            $upline_level = MlmLevel::where('user_id', $upline->id)->get()->first();
            $position = 0;

            // if create franchiser
            if($agent_plan->name == 'Franchiser'){
                $free_agent_id = $this->getFreeFranchiserId($upline_level->id, $agent_plan->id);
                $upline_level = MlmLevel::find($free_agent_id);

                $downline_count = MlmLevel::where([
                    ['up_line_id', '=', $upline_level->id],
                    ['current_plan_id', '=', $agent_plan->id],
                ])->count();

                $position = $downline_count + 1;
            }

            $mlm_level = [
                'user_id' => $user->id,
                'current_plan_id' => $agent_plan->id,
                'up_line_id' => $upline_level->id,
                'position' => $position,
                'path' => $upline_level->path ? $upline_level->path . '/' . $upline_level->id : $upline_level->id,
                'level' => $upline_level->level + 1,
                'children_count' => 0,
            ];

            MlmLevel::create($mlm_level);

            // if create franchiser
            if($agent_plan->name == 'Franchiser'){
                $upline_level->children_count = $upline_level->children_count + 1;
                $upline_level->save();
            }

            DB::commit();

            $isExpires = false;

            return view('franchiser.franchiserAgent.verify', compact('isExpires'));

        } catch (Exception $e) {
            DB::rollBack();
            $errors = [ 'errors' => $e->getMessage() ];
            return view('franchiser.franchiserAgent.verify', compact('errors'));
        }
    }
}
