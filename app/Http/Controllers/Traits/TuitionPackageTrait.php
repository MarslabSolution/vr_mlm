<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Support\Facades\DB;

trait TuitionPackageTrait
{
    private function getTuitionPackage(int $tuition_id, string $only_field = null){
        $result = DB::connection('mainsql')?->table('tuition_packages')?->where('id',$tuition_id)?->first();

        if($result != null && $only_field != null){
            $result = $result->{$only_field};
        }

        return $result;
    }
}
