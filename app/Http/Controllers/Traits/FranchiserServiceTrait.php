<?php

namespace App\Http\Controllers\Traits;

use App\Models\AgentPlan;
use App\Models\Commission;
use App\Models\MlmLevel;

trait FranchiserServiceTrait
{
    private static $planName= 'Franchiser';

    private static function getCommissionableLevel()
    {
        return settings('franchiser_commission_level', 0, 'condition');
    }
    
    private static function getFranchiseCommission(int $level, string $type = 'package')
    {
        $dealerPlan = AgentPlan::where('name', self::$planName)->first();
        $commission = Commission::where([
            ['agent_plan_id', '=', $dealerPlan->id],
            ['level', '=', $level],
            ['type', '=', $type],
        ])->first();

        if($commission != null){
            return $commission->commission;
        }

        return null;
    }

    private static function getFranchiseCommissions(string $type = 'package')
    {
        $dealerPlan = AgentPlan::where('name', self::$planName)->first();
        $commissions = Commission::where([
            ['agent_plan_id', '=', $dealerPlan->id],
            ['type', '=', $type],
        ])->get();

        if($commissions != null){
            $commissionFormated = [];

            foreach($commissions as $commission){
                $commissionFormated[$commission->level] = $commission->commission;
            }

            return $commissionFormated;
        }

        return null;
    }

    private static function getJoinCommissions(string $type = 'affiliate')
    {
        $dealerPlan = AgentPlan::where('name', self::$planName)->first();
        $commissions = Commission::where([
            ['agent_plan_id', '=', $dealerPlan->id],
            ['type', '=', $type],
        ])->get();

        if($commissions != null){
            $commissionFormated = [];

            foreach($commissions as $commission){
                if(strpos($commission->commission, '%') !== false){
                    $baseCommission = str_replace('%', '', $commission->commission);

                    $commissionFormated[$commission->level] = $baseCommission * $dealerPlan->price / 100;
                }else{
                    $commissionFormated[$commission->level] = $commission->commission;
                }
            }

            return $commissionFormated;
        }

        return null;
    }

    private static function getFreeFranchiserId(string $user_id, string $agent_plan_id){
        $isFound = false;
        $finalist_ids = [$user_id];

        while(!$isFound && count($finalist_ids) > 0){
            $downline_read_ids = MlmLevel::where([
                ['up_line_id', '=', $finalist_ids[0]],
                ['current_plan_id', '=', $agent_plan_id],
            ])->pluck('id');

            if(count($downline_read_ids) < settings('maximum_franchiser_downline', config('constants.franchiser.franchiser_downline_maximum'), 'condition')){
                $isFound = true;
                return $finalist_ids[0];
            }else{
                array_shift($finalist_ids);
                array_push($finalist_ids, ...$downline_read_ids);
            }
        }
    }
}
