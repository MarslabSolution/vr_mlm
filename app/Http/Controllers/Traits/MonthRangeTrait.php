<?php

namespace App\Http\Controllers\Traits;

use Carbon\Carbon;

trait MonthRangeTrait
{
    private function getDateRange(Carbon $date, int $start, int $end){
        $date_range = [
            'from' => null,
            'to' => Carbon::createFromFormat('Y-m-d H:i:s', $date->toDateTimeString()),
        ];
        
        if($date->day >= $start){
            $date_range['from'] = Carbon::createFromFormat('d-m-Y H:i:s', 
                $start . '-' .
                $date->month . '-' .
                $date->year . ' 00:00:00'
            );
            $date_range['to']->day($end);
            $date_range['to']->addMonth();
        }else{
            $date_range['from'] = Carbon::createFromFormat('d-m-Y H:i:s', 
                $start . '-' .
                $date->month - 1 . '-' .
                $date->year . ' 00:00:00'
            );
            $date_range['to']->day($end);
        }

        $date_range['to']->hour(23);
        $date_range['to']->minute(59);
        $date_range['to']->second(59);

        return $date_range;
    }
}
