<?php

namespace App\Http\Controllers\Traits;

use App\Models\MlmLevel;

trait MlmLevelDownlineTrait
{
    private function getDownlineByLevel(int $mlm_level_id, int $commission_level, int $only_level = null, int $current_level = 1){
        $mlm_level = collect();

        if($commission_level >= $current_level){
            $level_read = MlmLevel::where('up_line_id', $mlm_level_id)->with(['user', 'current_plan', 'up_line'])->get();

            if($only_level == null || $only_level == $current_level){
                $mlm_level = $level_read;
            }

            if($commission_level >= $current_level + 1){
                foreach($level_read as $index => $value){
                    $mlm_level = $mlm_level->merge($this->getDownlineByLevel($value->id, $commission_level, $only_level, $current_level + 1));
                }
            }
        }

        return $mlm_level;
    }
}
