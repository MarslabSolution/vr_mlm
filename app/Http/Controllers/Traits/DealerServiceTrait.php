<?php

namespace App\Http\Controllers\Traits;

use App\Models\AgentPlan;
use App\Models\Commission;

trait DealerServiceTrait
{
    private static $planName= 'Dealer';
    private static $potentialPlanName= 'Franchiser';

    private static function getCommissionableLevel()
    {
        return settings('dealer_commission_level', 0, 'condition');
    }
    
    private static function getPotentialCommissionableLevel()
    {
        return settings('franchiser_commission_level', 0, 'condition');
    }

    private static function getDealerCommission(int $level, string $type = 'package')
    {
        $dealerPlan = AgentPlan::where('name', self::$planName)->first();
        $commission = Commission::where([
            ['agent_plan_id', '=', $dealerPlan->id],
            ['level', '=', $level],
            ['type', '=', $type],
        ])->first();

        if($commission != null){
            return $commission->commission;
        }

        return null;
    }

    private static function getDealerCommissions(string $type = 'package')
    {
        $dealerPlan = AgentPlan::where('name', self::$planName)->first();
        $commissions = Commission::where([
            ['agent_plan_id', '=', $dealerPlan->id],
            ['type', '=', $type],
        ])->get();

        if($commissions != null){
            $commissionFormated = [];

            foreach($commissions as $index => $commission){
                $commissionFormated[$commission->level] = $commission->commission;
            }

            return $commissionFormated;
        }

        return null;
    }

    private static function getPotentialDealerCommissions(string $type = 'package')
    {
        $potentialPlan = AgentPlan::where('name', self::$potentialPlanName)->first();
        $commissions = Commission::where([
            ['agent_plan_id', '=', $potentialPlan->id],
            ['type', '=', $type],
        ])->get();

        if($commissions != null){
            $commissionFormated = [];

            foreach($commissions as $index => $commission){
                $commissionFormated[$commission->level] = $commission->commission;
            }

            return $commissionFormated;
        }

        return null;
    }
}
