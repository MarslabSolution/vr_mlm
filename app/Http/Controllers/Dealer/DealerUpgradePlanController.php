<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use Modules\SsoClient\Entities\User;
use Modules\SsoClient\Entities\Role;
use App\Models\AgentStudent;
use App\Models\AgentPlan;
use App\Models\MlmLevel;
use Gate;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class DealerUpgradePlanController
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $login_user = auth()->user();

        $mlm_user = MlmLevel::where('user_id', $login_user->id)->with(['user', 'current_plan', 'up_line'])->get()->first();

        $agent_plans = AgentPlan::where('price', '>', $mlm_user->current_plan->price)->get();

        $plan_upline_users = [];

        if ($agent_plans != null && $mlm_user->up_line_id != null){
            foreach($agent_plans as $index => $plan){
                $upline_user = null;
                if(!MlmLevel::where('id', $mlm_user->up_line_id)->exists()) {
                    return response("Up line doesn't exist.", Response::HTTP_NO_CONTENT);
                }
                do {
                    $upline = MlmLevel::find($mlm_user->up_line_id)->load(['user', 'current_plan', 'up_line']);
    
                    if($upline == null){
                        break;

                    }else if($upline->current_plan->name == $plan->name){
                        $upline_user = User::find($upline->user_id);
                        break;
                    }else{
                        $mlm_user = $upline;
                    }
    
                } while ($upline_user == null);

                $plan_upline_users[$plan->id] = $upline_user;
            }
        }

        return view('dealer.dealerUpgradePlan.index', compact('login_user', 'agent_plans', 'plan_upline_users'));
    }
}
