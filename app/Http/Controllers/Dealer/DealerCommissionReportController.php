<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use Modules\SsoClient\Entities\Role;
use App\Models\AgentStudent;
use App\Models\MlmLevel;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class DealerCommissionReportController
{
    public function index(Request $request){
        return $this->daily($request, null);
    }

    public function daily(Request $request, $date = ''){
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $report_type = 'daily';

        if ($request->ajax()) {
            date_default_timezone_set("Asia/Kuala_Lumpur");
            $date_from = Carbon::now();
            $date_to = Carbon::now();
            
            if(preg_match("/^(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[0-2])-(19[0-9][0-9]|20[0-9][0-9])$/", $date)){
                $date_from = Carbon::createFromFormat('d-m-Y', $date);
                $date_to = Carbon::createFromFormat('d-m-Y', $date);
            }
            
            return $this->generateTable($date_from, $date_to);
        }

        return view('dealer.dealerCommissionReport.index');
    }

    public function weekly(Request $request, $date = ''){
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $report_type = 'weekly';

        if ($request->ajax()) {
            date_default_timezone_set("Asia/Kuala_Lumpur");
            $date_from = Carbon::now();
            $date_to = Carbon::now();

            if(preg_match("/^(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[0-2])-(19[0-9][0-9]|20[0-9][0-9])$/", $date)){
                $date_from = Carbon::createFromFormat('d-m-Y', $date);
                $date_to = Carbon::createFromFormat('d-m-Y', $date);
            }

            $date_from->subDays(config('constants.week'));

            return $this->generateTable($date_from, $date_to);
        }

        return view('dealer.dealerCommissionReport.index');
    }

    public function monthly(Request $request, $date = ''){
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $report_type = 'monthly';

        if ($request->ajax()) {
            date_default_timezone_set("Asia/Kuala_Lumpur");
            $date_from = Carbon::now();
            $date_to = Carbon::now();

            if(preg_match("/^(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[0-2])-(19[0-9][0-9]|20[0-9][0-9])$/", $date)){
                $date_from = Carbon::createFromFormat('d-m-Y', $date);
                $date_to = Carbon::createFromFormat('d-m-Y', $date);
            }

            $date_from->subDays(config('constants.month'));

            return $this->generateTable($date_from, $date_to);
        }

        return view('dealer.dealerCommissionReport.index');
    }

    private function generateTable($date_from, $date_to){
        $login_user = auth()->user();

        $date_from->hour(0);
        $date_from->minute(0);
        $date_from->second(0);
        $date_from = $date_from->toDateTimeString();

        $date_to->hour(23);
        $date_to->minute(59);
        $date_to->second(59);
        $date_to = $date_to->toDateTimeString();

        $user_mlm_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        $query = MlmLevel::where([
            ['user_id', '=', $login_user->id],
            ['created_at', '>=', $date_from],
            ['created_at', '<', $date_to],
        ])->orWhere([
            ['up_line_id', '=', $user_mlm_level->id],
            ['created_at', '>=', $date_from],
            ['created_at', '<', $date_to],
        ])->with(['user', 'current_plan', 'up_line']);
        
        $table = Datatables::of($query);

        $table->addColumn('placeholder', '&nbsp;');
        $table->addColumn('actions', '&nbsp;');

        $table->editColumn('actions', function ($row) {
            $viewGate = 'mlm_level_show';
            $editGate = 'mlm_level_edit';
            $deleteGate = 'mlm_level_delete';
            $crudRoutePart = 'mlm-levels';

            return view('partials.datatablesActions', compact(
                'viewGate',
                'editGate',
                'deleteGate',
                'crudRoutePart',
                'row'
            ));
        });

        $table->editColumn('id', function ($row) {
            return $row->id ? $row->id : '';
        });
        $table->addColumn('user_name', function ($row) {
            $login_user = auth()->user();
            
            if($row->user->id == $login_user->id){
                return trans('cruds.dealerCommissionReport.fields.own_self');
            }

            return $row->user ? $row->user->name : '';
        });

        $table->addColumn('current_plan_name', function ($row) {
            return $row->current_plan ? $row->current_plan->name : '';
        });

        $table->editColumn('student_count', function ($row) {
            $students = AgentStudent::where('referral_id', $row->user->id)->get();

            $student_count = 0;

            if(count($students) > 0){
                $student_count = count($students);
            }

            return $student_count;
        });

        $table->rawColumns(['actions', 'placeholder', 'user', 'current_plan']);

        return $table->make(true);
    }
}
