<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Traits\DealerServiceTrait;
use App\Http\Controllers\Traits\MlmLevelDownlineTrait;
use App\Http\Controllers\Traits\MonthRangeTrait;
use App\Models\AgentStudent;
use App\Models\MlmLevel;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class DealerCommissionHistoryController
{
    use MlmLevelDownlineTrait;
    use MonthRangeTrait;
    use DealerServiceTrait;

    public function index(Request $request){
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $login_user = auth()->user();
        $user_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        $all_level_downlines = [];
        $tol_level = $this->getCommissionableLevel();

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date_now = Carbon::now();
        $date_oldest = Carbon::now();

        for($i = 0; $i <= $tol_level; $i++){
            $downlines = $this->getDownlineByLevel($user_level->id, $tol_level, $i == 0 ? null : $i);
            $tol_commission = 0;
            
            $level_downlines = collect();

            $commissions = $this->getDealerCommissions();

            foreach($downlines as $index => $value){
                $students = AgentStudent::where('referral_id', $value->user->id)->get();
                
                $student_count = 0;

                if(count($students) > 0){
                    $student_count = count($students);
                    
                    $tol_commission += $student_count * $commissions[$value->level - $user_level->level];

                    if($date_oldest->toDateTimeString() > $value->created_at->format('Y-m-d H:i:s')){
                        $date_oldest = Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at->format('Y-m-d H:i:s'));
                    }
                }
            }

            $all_level_downlines[$i] = [
                'downlines' => null,
                'commission' => $tol_commission,
            ];
        }

        $date_range = $this->getDateRange($date_now, config('constants.dealer_date_start'), config('constants.dealer_date_end'));
        $date_from = $date_range['from'];
        $date_to = $date_range['to'];
        
        $month_select = [];
        array_push($month_select, [
            'from' => $this->getDateRange($date_oldest, config('constants.dealer_date_start'), config('constants.dealer_date_end'))['from']->toDateTimeString(),
            'to'   => $this->getDateRange($date_now, config('constants.dealer_date_start'), config('constants.dealer_date_end'))['to']->toDateTimeString(),
        ]);
        array_push($month_select, [
            'from' => $date_from->toDateTimeString(),
            'to'   => $date_to->toDateTimeString(),
        ]);
        
        while(!($date_range['from']->toDateTimeString() <= $date_oldest->toDateTimeString() && $date_oldest->toDateTimeString() <= $date_range['to']->toDateTimeString())){
            $date_from->subMonth();
            $date_to->subMonth();
            
            array_push($month_select, [
                'from' => $date_from->toDateTimeString(),
                'to'   => $date_to->toDateTimeString(),
            ]);
        }

        return view('dealer.dealerCommissionHistory.index', compact('month_select', 'all_level_downlines'));
    }

    public function update(Request $request)
    {
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $request->validate([
            'from' => 'required',
            'to' => 'required',
        ]);

        $date_from = $request['from'];
        $date_to = $request['to'];

        $login_user = auth()->user();
        $user_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        $all_level_downlines = [];
        $tol_level = $this->getCommissionableLevel();

        $commissions = $this->getDealerCommissions();
        
        for($i = 0; $i <= $tol_level; $i++){
            $downlines = $this->getDownlineByLevel($user_level->id, $tol_level, $i == 0 ? null : $i);
            $tol_commission = 0;
            
            $level_downlines = [];

            foreach($downlines as $index => $value){
                $students = AgentStudent::where([
                    ['referral_id', '=', $value->user->id],
                    ['created_at', '>=', $date_from],
                    ['created_at', '<', $date_to],
                ])->get();

                $student_count = 0;

                if(count($students) > 0){
                    $student_count = count($students);

                    $base_commission = $commissions[$downlines[$index]->level - $user_level->level];

                    $downlines[$index]->student_count = $student_count;
                    $downlines[$index]->cumulative_commission = $student_count * $base_commission;

                    array_push($level_downlines, $downlines[$index]);

                    $tol_commission += $student_count * $base_commission;
                }
            }

            $all_level_downlines[$i] = [
                'downlines' => $level_downlines,
                'commission' => $tol_commission,
            ];
        }

        return $all_level_downlines;
    }

    public function generateTable(Request $request){
        $request->validate([
            'collection' => 'required',
        ]);

        $collection = json_decode($request['collection'], true);

        // table generate
        $table = Datatables::of($collection);

        $table->addColumn('placeholder', '&nbsp;');

        $table->editColumn('id', function ($row) {
            return $row['id'] ? $row['id'] : '';
        });
        $table->editColumn('user_name', function ($row) {
            return $row['user'] ? $row['user']['name'] : '';
        });
        $table->editColumn('current_plan_name', function ($row) {
            return $row['current_plan'] ? $row['current_plan']['name'] : '';
        });
        $table->editColumn('student_count', function ($row) {
            return $row['student_count'] ? $row['student_count'] : '';
        });
        $table->editColumn('register_at', function ($row) {
            return $row['created_at'] ? substr($row['created_at'], 0, 10) : '';
        });
        $table->editColumn('cumulative_commission', function ($row) {
            return $row['cumulative_commission'] ? $row['cumulative_commission'] : '';
        });

        $table->rawColumns(['placeholder']);

        return $table->make(true);
    }
}
