<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Controllers\Traits\MonthRangeTrait;
use App\Models\AgentStudent;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class DealerStudentController extends Controller
{
    use CsvImportTrait;
    use MonthRangeTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('agent_student_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $login_user = auth()->user();
        
        $query = $this->getThisMonthQuery();
        $total_student = count($query->get());

        $oldest_student = AgentStudent::where('referral_id', $login_user->id)->orderBy('created_at')->get()->first();

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date_now = Carbon::now();
        $date_oldest = Carbon::now();
        
        if($oldest_student != null){
            $date_oldest = Carbon::createFromFormat('Y-m-d H:i:s', $oldest_student->created_at->format('Y-m-d H:i:s'));
        }
        
        $date_range = $this->getDateRange($date_now, config('constants.dealer_date_start'), config('constants.dealer_date_end'));
        $date_from = $date_range['from'];
        $date_to = $date_range['to'];
        
        $month_select = [];
        array_push($month_select, [
            'from' => $this->getDateRange($date_oldest, config('constants.dealer_date_start'), config('constants.dealer_date_end'))['from']->toDateTimeString(),
            'to'   => $this->getDateRange($date_now, config('constants.dealer_date_start'), config('constants.dealer_date_end'))['to']->toDateTimeString(),
        ]);
        array_push($month_select, [
            'from' => $date_from->toDateTimeString(),
            'to'   => $date_to->toDateTimeString(),
        ]);
        
        while(!($date_range['from']->toDateTimeString() <= $date_oldest->toDateTimeString() && $date_oldest->toDateTimeString() <= $date_range['to']->toDateTimeString())){
            $date_from->subMonth();
            $date_to->subMonth();
            
            array_push($month_select, [
                'from' => $date_from->toDateTimeString(),
                'to'   => $date_to->toDateTimeString(),
            ]);
        }

        return view('dealer.dealerStudents.index', compact('total_student', 'month_select'));
    }

    public function agentStudentInMonth(Request $request)
    {
        abort_if(Gate::denies('agent_student_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $table = Datatables::of($this->getThisMonthQuery());

        $table->addColumn('placeholder', '&nbsp;');

        $table->editColumn('id', function ($row) {
            return $row->id ? $row->id : '';
        });
        $table->editColumn('tuition_package_efk', function ($row) {
            return $row->tuition_package_efk ? $row->tuition_package_efk : '';
        });
        $table->editColumn('student_id', function ($row) {
            return $row->student ? $row->student->name : '';
        });
        $table->editColumn('register_at', function ($row) {
            return $row->created_at ? $row->created_at->format('Y-m-d') : '';
        });

        $table->rawColumns(['placeholder']);

        return $table->make(true);
    }

    private function getThisMonthQuery(){
        $login_user = auth()->user();

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date_to = Carbon::now();
        $date_from = null;

        if($date_to->day >= config('constants.dealer_date_start')){
            $date_from = Carbon::createFromFormat('d-m-Y H:i:s', 
                config('constants.dealer_date_start') . '-' .
                $date_to->month . '-' .
                $date_to->year . ' 00:00:00'
            );
        }else{
            $date_from = Carbon::createFromFormat('d-m-Y H:i:s', 
                config('constants.dealer_date_start') . '-' .
                $date_to->month - 1 . '-' .
                $date_to->year . ' 00:00:00'
            );
        }

        $date_from = $date_from->toDateTimeString();
        $date_to = $date_to->toDateTimeString();

        $query = AgentStudent::where([
            ['referral_id', '=', $login_user->id],
            ['created_at', '>=', $date_from],
            ['created_at', '<', $date_to],
        ])->with(['student']);

        return $query;
    }

    public function agentStudentByDate(Request $request){
        abort_if(Gate::denies('agent_student_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $date_from = $request['from'];
        $date_to = $request['to'];

        if($date_from == null || $date_to == null){
            return;
        }

        $login_user = auth()->user();

        $query = AgentStudent::where([
            ['referral_id', '=', $login_user->id],
            ['created_at', '>=', $date_from],
            ['created_at', '<', $date_to],
        ])->with(['student']);

        $table = Datatables::of($query);

        $table->addColumn('placeholder', '&nbsp;');

        $table->editColumn('id', function ($row) {
            return $row->id ? $row->id : '';
        });
        $table->editColumn('tuition_package_efk', function ($row) {
            return $row->tuition_package_efk ? $row->getTuitionPackagesName($row->tuition_package_efk) : '';
        });
        $table->editColumn('student_id', function ($row) {
            return $row->student ? $row->student->name : '';
        });
        $table->editColumn('register_at', function ($row) {
            return $row->created_at ? $row->created_at->format('Y-m-d') : '';
        });

        $table->rawColumns(['placeholder']);

        return $table->make(true);
    }
}
