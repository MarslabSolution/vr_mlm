<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Controller;
use Modules\SsoClient\Entities\Role;
use App\Models\MlmLevel;
use DateTime;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class DealerDepositController
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $login_user = auth()->user()->load(['roles']);

        $user_mlm_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        $downline_count = MlmLevel::where('up_line_id', $user_mlm_level->id)->with(['user', 'current_plan', 'up_line'])->count();
        
        $min_target = settings('package_refund_target_'.$user_mlm_level->current_plan_id,config('constants.dealer.deposit_min_temp'),'condition');
        $refundable_amount = settings('max_refundable_amount_'.$user_mlm_level->current_plan_id, 0, 'condition');
        $today = new DateTime();
        $refundable_period = settings('package_refund_period_'.$user_mlm_level->current_plan_id, 0, 'condition');
        $refundable_day = $user_mlm_level->created_at->modify('+'.$refundable_period.' months');
        $leave_date = $today->diff($refundable_day);

        return view('dealer.dealerDeposit.index', compact('login_user', 'downline_count', 'min_target', 'refundable_amount', 'leave_date'));
    }
}
