<?php

namespace App\Http\Controllers\Dealer;

use App\Http\Controllers\Traits\DealerServiceTrait;
use App\Http\Controllers\Traits\MlmLevelDownlineTrait;
use App\Models\AgentStudent;
use App\Models\MlmLevel;
use Carbon\Carbon;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DealerHomeController
{
    use MlmLevelDownlineTrait;
    use DealerServiceTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('agent_downline_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        $login_user = auth()->user()->load(['roles']);

        $cumulative_commission = 0;
        $user_student = 0;
        $downline_student = 0;

        $user_level = MlmLevel::where('user_id', $login_user->id)->get()->first();
        
        $mlm_levels = collect();

        $user_downlines = collect();

        if($user_level != null){
            $user_downline = $this->getDownlineByLevel($user_level->id, $this->getCommissionableLevel());
            $mlm_levels = $user_downline->push($user_level);
        }

        $commissions = $this->getDealerCommissions();

        foreach($mlm_levels as $index => $value){
            $dealer_students = AgentStudent::where('referral_id', $value->user_id)->get();

            $student_count = 0;

            if(count($dealer_students) > 0){
                $student_count = count($dealer_students);
            }

            $commission = $student_count * $commissions[$value->level - $user_level->level];

            $cumulative_commission += $commission;

            if ($value->user_id == $login_user->id){
                $user_student += $student_count;
            }else{
                $downline_student += $student_count;
            }
        }


        return view('dealer.home', compact('login_user', 'mlm_levels', 'cumulative_commission', 'user_student', 'downline_student'));
    }

    public function getLatestMonthCommission(Request $request){
        $login_user = auth()->user();

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date_to = Carbon::now();
        $date_from = null;

        if($date_to->day >= config('constants.dealer_date_start')){
            $date_from = Carbon::createFromFormat('d-m-Y H:i:s', 
                config('constants.dealer_date_start') . '-' .
                $date_to->month . '-' .
                $date_to->year . ' 00:00:00'
            );
            $date_to->addMonth();
            $date_to->day(config('constants.dealer_date_end'));
        }else{
            $date_from = Carbon::createFromFormat('d-m-Y H:i:s', 
                config('constants.dealer_date_start') . '-' .
                $date_to->month - 1 . '-' .
                $date_to->year . ' 00:00:00'
            );
            $date_to->day(config('constants.dealer_date_end'));
        }

        // prepare for login user student
        $half_year_user_students = null;
        $total_month = config('constants.commission_month_of_show_temp');

        // prepare for login user downline student
        $user_level = MlmLevel::where('user_id', $login_user->id)->get()->first();

        $user_downlines = collect();

        if($user_level != null){
            $user_downlines = $this->getDownlineByLevel($user_level->id, $this->getCommissionableLevel());
        }

        $half_year_downline_students = null;

        do{
            // to get login user student
            $user_students = AgentStudent::where([
                ['referral_id', '=', $login_user->id],
                ['created_at', '>=', $date_from->toDateTimeString()],
                ['created_at', '<', $date_to->toDateTimeString()],
            ])->with(['student'])->get();

            if ($half_year_user_students == null){
                $half_year_user_students = $user_students;
            }else{
                $half_year_user_students = $half_year_user_students->merge($user_students);
            }

            // to get login user downline student
            foreach($user_downlines as $index => $value){
                $downline_students = AgentStudent::where([
                    ['referral_id', '=', $value->user_id],
                    ['created_at', '>=', $date_from->toDateTimeString()],
                    ['created_at', '<', $date_to->toDateTimeString()],
                ])->with(['student'])->get();
                
                foreach($downline_students as $index => $downline_student){
                    $downline_students[$index]->in_level = $value->level;
                }

                if ($half_year_downline_students == null){
                    $half_year_downline_students = $downline_students;
                }else{
                    $half_year_downline_students = $half_year_downline_students->merge($downline_students);
                }
            }


            $date_from->subMonth(1);
            $date_to->subMonth(1);

            $total_month -= 1;

        } while($total_month >= 0);


        // set data for the chart use
        $total_month = config('constants.commission_month_of_show_temp');
        $chartData = [];

        do{
            $date_from->addMonth();
            $date_to->addMonth();
            
            $user_commission = 0;
            $downline_commission = 0;

            $commissions = $this->getDealerCommissions();

            foreach($half_year_user_students as $index => $value){
                if($date_from->toDateTimeString() <= $value->created_at && $value->created_at < $date_to->toDateTimeString()){
                    $user_commission += $commissions[0];
                    unset($half_year_user_students[$index]);
                }
            }
            if($half_year_downline_students != null){
                foreach($half_year_downline_students as $index => $value){
                    if($date_from->toDateTimeString() <= $value->created_at && $value->created_at < $date_to->toDateTimeString()){
                        $downline_commission += $commissions[$value->in_level - $user_level->level];
                        unset($half_year_downline_students[$index]);
                    }
                }
            }

            $total_month -= 1;

            if($total_month < 0){
                $date_to = Carbon::now();
            }

            array_push($chartData, [
                'date'   => $date_to->year . '-' . $date_to->month . '-' . $date_to->day,
                'user'     => $user_commission,
                'downline' => $downline_commission,
            ]);            

        }while($total_month >= 0);


        return $chartData;
    }
}
