<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Models\AgentStudent;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class AgentStudentController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('agent_student_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = AgentStudent::with(['student', 'referral']);
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate = 'null';
                $editGate = 'null';
                $deleteGate = 'null';
                $crudRoutePart = 'agent-students';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : '';
            });
            $table->editColumn('tuition_package_efk', function ($row) {
                return $row->tuition_package_efk ? $row->getTuitionPackagesName($row->tuition_package_efk) : '';
            });
            $table->editColumn('student_id', function ($row) {
                return $row->student ? $row->student->name : '';
            });
            $table->editColumn('referral_id', function ($row) {
                return $row->referral ? $row->referral->name : '';
            });
            $table->editColumn('trainer_id', function ($row) {
                return $row->trainer ? $row->trainer->name : '';
            });

            $table->rawColumns(['actions', 'placeholder']);

            return $table->make(true);
        }

        return view('admin.agentStudents.index');
    }
}
