<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\StoreDealerRequest;
use App\Http\Requests\UpdateDealerRequest;
use App\Models\AgentPlan;
use App\Models\MlmLevel;
use Modules\SsoClient\Entities\User;
use Modules\SsoClient\Entities\Role;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class DealerController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('dealer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $collection = Role::where('title', 'Dealer')->with('users')->get()->first()->users;

            $table = Datatables::collection($collection);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate = 'null';
                $editGate = 'dealer_edit';
                $deleteGate = 'null';
                $crudRoutePart = 'dealers';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : '';
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : '';
            });
            $table->editColumn('email', function ($row) {
                return $row->email ? $row->email : '';
            });
            $table->editColumn('approved', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->approved ? 'checked' : null) . '>';
            });
            $table->editColumn('verified', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->verified ? 'checked' : null) . '>';
            });
            $table->editColumn('two_factor', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->two_factor ? 'checked' : null) . '>';
            });

            $table->rawColumns(['actions', 'placeholder', 'approved', 'verified', 'two_factor', 'roles']);

            return $table->make(true);
        }

        return view('admin.dealers.index');
    }

    public function create()
    {
        abort_if(Gate::denies('dealer_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $uplines = [ '' => trans('global.pleaseSelect') ];
        $mlm_levels = MlmLevel::with(['user', 'current_plan', 'up_line'])->get();

        foreach ($mlm_levels as $index => $value){
            $uplines[$value->id] = $value->user_id . ' - ' . $value->user->name;
        }

        return view('admin.dealers.create', compact('uplines'));
    }

    public function store(StoreDealerRequest $request)
    {
        $request_data = $request->all();

        $role = Role::where('title', 'Dealer')->get()->first();
        $agent_plan = AgentPlan::where('name', 'Dealer')->get()->first();
        $upline_level = MlmLevel::find($request_data['upline']);

        $user = User::create($request_data);
        $user->roles()->sync($role->id);

        $mlm_level = [
            'user_id' => $user->id,
            'current_plan_id' => $agent_plan->id,
            'up_line_id' => $request_data['upline'],
            'position' => 0,
            'path' => $upline_level->path . '/' . $upline_level->id,
            'level' => $upline_level->level + 1,
            'children_count' => 0,
        ];

        MlmLevel::create($mlm_level);

        return redirect()->route('admin.dealers.index');
    }

    public function edit(User $user)
    {
        abort_if(Gate::denies('dealer_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $dealer_level = MlmLevel::where('user_id', $user->id)->get()->first();

        $dealer_upline = null;

        if($dealer_level != null){
            $dealer_upline = MlmLevel::find($dealer_level->up_line_id)->load('user');
        }

        return view('admin.dealers.edit', compact('user', 'dealer_upline'));
    }

    public function update(UpdateDealerRequest $request, User $user)
    {
        $user_found = User::where('email', $request->email)->first();

        if($user_found != null){
            back()->withErrors(['error' => trans('validation.email_used')]);
        }

        $user->update($request->all());
        return redirect()->route('admin.dealers.index');
    }
}
