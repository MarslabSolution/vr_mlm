<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Models\AgentPlan;
use App\Models\CommissionStatement;
use DateInterval;
use DatePeriod;
use DateTime;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class CommissionStatementController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request, $type = '')
    {
        abort_if(Gate::denies('commission_statement_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $agentPlanID = AgentPlan::where('name', ucfirst($type))?->pluck('id')[0] ?? '';
        $period = new DatePeriod(
            new DateTime('2022-01-01'),
            DateInterval::createFromDateString('1 month'),
            new DateTime()
        );
        $dates = [];
        foreach ($period as $date) {
            $dates[$date->format("Y-m")] = $date->format("m/Y");
        }
        if ($request->ajax()) {
            $query = CommissionStatement::whereHas('agent', function($agent) use ($agentPlanID) {
                $agent->where('current_plan_id', $agentPlanID);
            })
            ->select(sprintf('%s.*', (new CommissionStatement())->table))
            ->with(['agent', 'commission_groups', 'user']);
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate = 'commission_statement_show';
                $editGate = 'commission_statement_edit';
                $deleteGate = 'commission_statement_delete';
                $viewGate = $editGate = $deleteGate = 'null';
                $crudRoutePart = 'commission-statements';

                return view('partials.datatablesActions', compact(
                'viewGate',
                'editGate',
                'deleteGate',
                'crudRoutePart',
                'row'
            ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : '';
            });
            $table->addColumn('user.name', function ($row) {
                return $row->user ? $row->user->name : '';
            });

            $table->editColumn('user.email', function ($row) {
                return $row->user ? $row->user->email : '';
            });
            $table->editColumn('total', function ($row) {
                return $row->total ? $row->total : '';
            });
            $table->editColumn('created_at', function ($row) {
                return $row->created_at->format('m/Y');
            });
            $table->editColumn('commission_group', function ($row) {
                $labels = [];
                foreach ($row->commission_groups as $commission_group) {
                    $labels[] = sprintf('<span class="label label-info label-many">%s</span>', $commission_group->type);
                }

                return implode(' ', $labels);
            });

            $table->rawColumns(['actions', 'placeholder', 'agent', 'commission_group']);

            return $table->make(true);
        }

        return view('admin.commissionStatements.index', ['type'=> $type, 'dates' => $dates]);
    }
}
