<?php

namespace App\Http\Controllers\Admin;

use App\Events\AutoUpgradeFranchiser;
use App\Events\FranchiserStatement;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\StoreDealerRequest;
use App\Http\Requests\UpdateDealerRequest;
use App\Models\AgentPlan;
use App\Models\AgentStudent;
use App\Models\Commission;
use App\Models\MlmLevel;
use Modules\SsoClient\Entities\User;
use Modules\SsoClient\Entities\Role;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class AgentsController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request, String $type = 'dealer')
    {
        abort_if(Gate::denies($type . '_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $collections = User::whereHas('roles', function ($query) use ($type) {
                $query->where('title', ucfirst($type));
            })->get();
            $table = Datatables::of($collections);
            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', function ($row) use ($type) {
                $viewGate = $deleteGate = 'null';
                $editGate = $type . '_edit';
                $crudRoutePart = 'agents';
                $parameter = [
                    $row->id,
                    'type' => $type,
                ];
                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row',
                    'parameter'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : '';
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : '';
            });
            $table->editColumn('email', function ($row) {
                return $row->email ? $row->email : '';
            });
            $table->editColumn('total_referral_student', function ($row) {
                return AgentStudent::where('referral_id', $row->id)->count() ?? 0;
            });
            $table->editColumn('users.approved', function ($row) {
                return '<input type="checkbox" disabled ' . (!($row->approved) ? 'checked' : null) . '>';
            });
            /* $table->editColumn('verified', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->verified ? 'checked' : null) . '>';
            });
            $table->editColumn('two_factor', function ($row) {
                return '<input type="checkbox" disabled ' . ($row->two_factor ? 'checked' : null) . '>';
            }); */

            $table->rawColumns(['actions', 'placeholder', 'users.approved', 'roles']);
            return $table->make(true);
        }

        return view('admin.' . $type . 's.index');
    }

    public function create(String $type = 'dealer')
    {
        abort_if(Gate::denies($type . '_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $uplines = ['' => trans('global.pleaseSelect')];

        if ($type == 'dealer') {
            $id_collections = User::whereHas('roles', function ($query) use ($type) {
                $query->where('title', ucfirst($type))->whereOr('title', 'franchiser');
            })->pluck('id');

            $mlm_levels = MlmLevel::whereIn('user_id', $id_collections)->with(['user', 'current_plan', 'up_line'])->get();

        } else if ($type == 'franchiser') {
            $id_collections = User::whereHas('roles', function ($query) use ($type) {
                $query->where('title', ucfirst($type));
            })->pluck('id');
            
            $full_mlm_ids = MlmLevel::groupBy('up_line_id')
                                    ->select('up_line_id', DB::raw('count(*) as total'))
                                    ->where('up_line_id', '!=', null)
                                    ->havingRaw(DB::raw('total >= 5'))
                                    ->pluck('up_line_id');

            $mlm_levels = MlmLevel::whereIn('user_id', $id_collections)->whereNotIn('id', $full_mlm_ids)->with(['user', 'current_plan', 'up_line'])->get();
        }


        foreach ($mlm_levels as $index => $value) {
            $uplines[$value->id] = $value->user_id . ' - ' . $value->user->name;
        }

        return view('admin.' . $type . 's.create', compact('uplines'));
    }

    public function store(StoreDealerRequest $request, String $type = 'dealer')
    {
        try {
            DB::beginTransaction();

            $request_data = $request->validated();

            $role = Role::where('title', ucfirst($type))?->first(); // get role
            $agent_plan = AgentPlan::where('name', ucfirst($type))?->first(); // get agent plan
            $upline_level = MlmLevel::find($request_data['upline']); // get upline id

            // check for existing user
            $user_query = User::where('email', $request_data['email']);
            $user = $user_query->first();

            if($user != null){
                $roles_ids = AgentPlan::groupBy('roles_id')->pluck('roles_id');

                $user_found = $user_query->whereHas('roles', function($roles) use ($roles_ids) {
                    $roles->whereIn('id', $roles_ids);
                })->first();

                if($user_found != null){
                    return back()->withInput()->withErrors([
                        'email' => trans('validation.agent_account_already_exists'),
                    ]);
                }else{
                    $user->roles()->sync([
                        ...$user->roles->pluck('id'),
                        $role->id,
                    ]);
                }
            }else{
                // create user account and roles
                $user = User::create($request_data);
                $user->roles()->sync($role->id);
            }
            
            // sound event if upline already have 155 downline.
            event(new AutoUpgradeFranchiser($upline_level));

            $commissionableLevel = settings(
                $agent_plan->id,
                $type == 'franchiser' ? Commission::where([['type', 'affiliate'], ['agent_plan_id', $agent_plan->id]])->count() : 1,
                'agent_plans_affiliate_level'
            );

            $commissionableLevel = $type == 'franchiser'
                ? Commission::where([
                    ['type', 'affiliate'],
                    ['agent_plan_id', $agent_plan->id]
                ])->count() : 1;

            // deconstruct the strings of uplines id to array.
            $uplines = !empty($upline_level->path) ? explode('/', $upline_level->path) : [];
            $user_position = 0;

            if ($type == 'franchiser') {
                $commissionableUplines = $uplines;
                $count = count($uplines);

                // check if current parent has the uplines more than the commissionable level.
                if (count($uplines) >= $commissionableLevel) {
                    // only the last 3 uplines (dynamically changed by commissionableLevel) are commissionable.
                    $commissionableUplines = array_slice($uplines, (count($uplines) - $commissionableLevel));
                    $count = $commissionableLevel;
                }

                $downline_number = MlmLevel::where([
                    ['up_line_id', '=', $upline_level->id],
                    ['current_plan_id', '=', $agent_plan->id],
                ])->count();
    
                $user_position = $downline_number + 1;

                for ($level = 0; $level < $count; $level++) {
                    $commission = 0;

                    $commissionableValue = Commission::where([
                        ['level', $level],
                        ['type', 'affiliate'],
                        ['agent_plan_id', $agent_plan->id],
                    ])?->first()->commission ?? 0;

                    // $commission = $agent_plan->price * (floatval(str_replace('%', '', $commissionableValue)) / 100);

                    if (ctype_digit(strval($commissionableValue))) {
                        $commission = $commissionableValue;
                    } else if (str_contains($commissionableValue, '%')) {
                        $commission = $agent_plan->price * (floatval(str_replace('%', '', $commissionableValue)) / 100);
                    } else {
                        $commission = $agent_plan->price * $commissionableValue;
                    }

                    event(new FranchiserStatement(
                        MlmLevel::find($commissionableUplines[$level]),
                        $type . '_recruitment',
                        $commission,
                    ));
                }
            }

            // prepare to save mlm level
            $mlm_level = new MlmLevel();
            $mlm_level->user_id = $user->id;
            $mlm_level->current_plan_id = $agent_plan->id;
            $mlm_level->up_line_id = $request_data['upline'];
            $mlm_level->position = $user_position;
            $mlm_level->level = $upline_level->level + 1;
            $mlm_level->children_count = 0;

            array_push($uplines, $upline_level->id);
            $mlm_level->path = implode('/', $uplines);
            $mlm_level->save();

            $upline_level->update([
                'children_count' => $upline_level->children_count + 1
            ]);

            DB::commit();

            return redirect()->route('admin.agents.index', ['type' => $type]);
        } catch (\Throwable $th) {
            DB::rollBack();
            throw $th;
        }
    }

    public function edit(User $user, String $type = 'dealer')
    {
        abort_if(Gate::denies('dealer_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $dealer_level = MlmLevel::where('user_id', $user->id)->get()->first();
        $agent_upline = null;
        if ($agent_level = MlmLevel::where('user_id', $user->id)?->first()) {
            $agent_upline = !empty($agent_level?->up_line_id) ? MlmLevel::find($agent_level->up_line_id)->load('user') : null;
        }
        return view('admin.' . $type . 's.edit', compact('user', 'agent_upline'));
    }

    public function update(UpdateDealerRequest $request, User $user, String $type = 'dealer')
    {
        $user_found = User::where('email', $request->email)->first();
        
        if($user_found != null){
            back()->withErrors(['error' => trans('validation.email_used')]);
        }

        $user->update($request->all());
        return redirect()->route('admin.agents.index', ['type' => $type]);
    }
}
