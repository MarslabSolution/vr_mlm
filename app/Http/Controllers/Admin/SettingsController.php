<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\SettingController as ControllersSettingsController;
use App\Models\AgentPlan;
use Gate;
use App\Models\AppSettings as Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;


class SettingsController extends ControllersSettingsController
{
    public function index($group = 'general')
    {
        abort_if(Gate::denies('setting_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $settings = Setting::where('group', $group)->select('id', 'name', 'payload')->get() ?? [];
        return view("admin.settings.$group.index", compact('settings'));
    }

    public function create($group = 'general')
    {
        abort_if(Gate::denies('setting_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view("admin.settings.$group.create");
    }

    public function store(Request $request, $group = 'general')
    {
        $request->merge([
            "payload" => json_encode($request->input('value', '')),
            "group" => $group,
            "locked" => 0,
        ]);
        $setting = Setting::create($request->except(['value']));

        return redirect()->route('admin.settings.index', ['type' => $group]);
    }

    public function edit(Setting $setting, $group = 'general')
    {
        abort_if(Gate::denies('setting_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view("admin.settings.$group.edit", compact('setting'));
    }

    public function update(Request $request, Setting $setting, $group = 'general')
    {
        $request->merge([
            "payload" => json_encode($request->input('value', '')),
        ]);
        $setting->update($request->except(['value']));
        return redirect()->route('admin.settings.index', ['type' => $group]);
    }

    public function show(Setting $setting, $group = 'general')
    {
        abort_if(Gate::denies('setting_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view("admin.settings.$group.show", compact('setting'));
    }

    public function destroy(Setting $setting)
    {
        abort_if(Gate::denies('setting_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $setting->delete();
        return back();
    }

    public function massDestroy(Request $request)
    {
        Setting::whereIn('id', request('ids'))->delete();
        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function saveConditionSettings(Request $request, $condition = null)
    {
        $keyList = [
            'min_deposit_amount',
            'refund_period',
            'refund_target',
        ];
        if(isset($condition) && $condition != null) {
            self::saveSettings($request, $condition);
        } else {
            foreach ($keyList as $value) {
                self::saveSettings($request, $value);
            }
        }
        return back();
    }
    private function saveSettings(Request $request, string $key) {
        $inputValues = $request->input($key, []);
        foreach ($inputValues as $id => $value) {
            $setting = Setting::where('name', 'package_'.$key.'_'.$id)->first();
            if(!isset($setting) || $setting == null) {
                Setting::create([
                    "name"=> "package_".$key."_".$id,
                    "payload" => json_encode($value),
                    "group" => 'condition',
                    "locked" => 0,
                ]);
            }else {
                $setting->update([
                    "payload" => json_encode($value),
                ]);
            };
        }
    }

    public function clearCache($specificSetting = null)
    {
        if (isset($specificSetting)) {
            switch (strtolower($specificSetting)) {
                case 'route':
                    Artisan::call('route:clear');
                    break;
                case 'view':
                    Artisan::call('view:clear');
                    break;
                case 'config':
                    Artisan::call('config:clear');
                    break;
                case 'cache':
                    Artisan::call('cache:clear');
                    break;
                default:
                    Artisan::call('optimize:clear');
                    break;
            }
        } else {
            Artisan::call('optimize:clear');
        }
        return redirect()->back();
    }
}
