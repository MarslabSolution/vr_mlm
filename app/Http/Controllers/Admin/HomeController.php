<?php

namespace App\Http\Controllers\Admin;

use App\Models\AgentPlan;
use Modules\SsoClient\Entities\User;

class HomeController
{
    public function index()
    {
        $franchiser = AgentPlan::where('name', 'Franchiser')->first();
        $dealer = AgentPlan::where('name', 'Dealer')->first();
        $total_dealers = $dealer == null ? 0 : User::whereHas('roles', function ($query) use ($dealer) {
            $query?->where('id',$dealer->roles_id);
        })->count();
        $total_franchiser = $franchiser == null ? 0 : User::whereHas('roles', function ($query) use ($franchiser) {
            $query?->where('id',$franchiser->roles_id);
            //$query->find($franchiser->roles_id);
        })->count();
        $total_franchiser_price = $franchiser?->price ?? 0;
        $total_dealer_price =  $dealer?->price ?? 0;
        return view('home', compact([
            'total_dealers',
            'total_franchiser',
            'total_franchiser_price',
            'total_dealer_price',
        ]));
    }
}
