<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyCommissionRequest;
use App\Http\Requests\StoreCommissionRequest;
use App\Http\Requests\UpdateCommissionRequest;
use App\Models\AgentPlan;
use App\Models\AppSettings;
use App\Models\Commission;
use App\Models\PackagesCommission;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class CommissionController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request, $type = 'affiliate')
    {
        abort_if(Gate::denies('commission_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $collections = Commission::where('type', $type)->with(['agent_plan'])->select(sprintf('%s.*', (new Commission())->table))->get();
            $table = Datatables::of($collections);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) use ($type) {
                $viewGate = 'commission_show';
                $editGate = 'commission_edit';
                $deleteGate = 'null';
                $crudRoutePart = 'commissions';
                $parameter = [
                    'commission' => $row,
                    'type' => $type,
                ];
                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row',
                    'parameter'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : '';
            });
            $table->editColumn('tuition_package', function ($row) {
                return sizeof($row->packagesCommissions) > 0
                    ? $row->tuitionPackagesName($row->packagesCommissions[0]->tuition_package_efk)
                    : '';
                //return $row->type ? Commission::TYPE_SELECT[$row->type] : '';
            });
            $table->addColumn('agent_plan_name', function ($row) {
                return $row->agent_plan ? $row->agent_plan->name : '';
            });
            $table->editColumn('agent_plan.price', function ($row) {
                return $row->agent_plan ? (is_string($row->agent_plan) ? $row->agent_plan : $row->agent_plan->price) : '';
            });
            $table->editColumn('level', function ($row) {
                return $row->level >= 0 ? $row->level : '';
            });
            $table->editColumn('commission', function ($row) {
                return $row->commission ? $row->commission : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'agent_plan']);

            return $table->make(true);
        }

        return view("admin.commissions.$type.index");
    }

    public function create($type = 'affiliate')
    {
        abort_if(Gate::denies('commission_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $agent_plans = AgentPlan::select('name', 'id', 'commissionable_level')->get();
        $tuition_commissions = [];
        $package_commissions = PackagesCommission::with(['commission'])->select('commission_id', 'tuition_package_efk')->get();

        foreach ($package_commissions as $package_commission) {
            if (array_key_exists($package_commission->tuition_package_efk, $tuition_commissions)) {
                if (!in_array($package_commission->commission->agent_plan_id, $tuition_commissions[$package_commission->tuition_package_efk])) {

                    $tuition_commissions[$package_commission->tuition_package_efk] = [
                        $package_commission->commission->agent_plan_id, ...$tuition_commissions[$package_commission->tuition_package_efk]
                    ];
                }
            } else {
                $tuition_commissions[$package_commission->tuition_package_efk] = [$package_commission->commission->agent_plan_id];
            }
        }

        return view("admin.commissions.$type.create", compact('agent_plans', 'tuition_commissions'));
    }

    public function store(StoreCommissionRequest $request, $type = 'affiliate')
    {
        if (isset($request->tuition_package_efk)) {
            $type = 'package';
        } else {
            $setting = AppSettings::firstOrCreate([
                'group' => 'agent_plans_affiliate_level',
                'name' => $request->agent_plan_id,
            ]);
            $setting->update([
                'payload' => $request->commissionable_level,
            ]);
        }
        foreach ($request->commissions as $key => $value) {
            $commission = Commission::updateOrCreate([
                'agent_plan_id' => $request->agent_plan_id,
                'level' => $key,
                'type' => $type
            ], [
                'commission' => $value,
            ]);
            if ($type === 'package') {
                PackagesCommission::updateOrCreate([
                    'tuition_package_efk' => $request->tuition_package_efk,
                    'commission_id' => $commission->id,
                ]);
            }
        }
        return redirect()->route('admin.commissions.index', ['type' => $type]);
    }

    public function edit(Commission $commission, $type = 'affiliate')
    {
        abort_if(Gate::denies('commission_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $agent_plans = AgentPlan::select('name', 'id', 'commissionable_level')->get();
        // $agent_plans = AgentPlan::pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        $commissionableLevel = $commission->agent_plan->commissionable_level;;
        if ($type == 'affiliate') {
            $commissionableLevel =
                settings($commission->agent_plan_id, $commissionableLevel, 'agent_plans_affiliate_level');
            /* AppSettings::where([
                'group' => 'agent_plans_affiliate_level',
                'name'=> $commission->agent_plan_id,
            ])?->pluck('payload')[0] ?? $commissionableLevel; */
            $commission->load('agent_plan');
        } else {
            $commission->load('agent_plan', 'packagesCommissions');
        }

        return view("admin.commissions.$type.edit", compact('agent_plans', 'commission', 'commissionableLevel'));
    }

    public function update(UpdateCommissionRequest $request, Commission $commission, $type = 'affiliate')
    {
        if (isset($request->tuition_package_efk)) {
            $type = 'package';
        } else {
            AppSettings::updateOrCreate([
                'group' => 'agent_plans_affiliate_level',
                'name' => $request->agent_plan_id,
            ], [
                'payload' => $request->commissionable_level,
            ]);
        }
        foreach ($request->commissions as $key => $value) {
            $commission = Commission::updateOrCreate([
                'agent_plan_id' => $request->agent_plan_id,
                'level' => $key,
                'type' => $type
            ], [
                'commission' => $value,
            ]);
            if ($type === 'package') {
                PackagesCommission::updateOrCreate([
                    'tuition_package_efk' => $request->tuition_package_efk,
                    'commission_id' => $commission->id,
                ]);
            }
        } //$commission->update($request->all());
        return redirect()->route('admin.commissions.index', ['type' => $type]);
    }

    public function show(Commission $commission, $type = 'affiliate')
    {
        abort_if(Gate::denies('commission_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $commission->load('agent_plan');
        return view("admin.commissions.$type.show", compact('commission'));
    }

    public function destroy(Commission $commission)
    {
        abort_if(Gate::denies('commission_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $commission->delete();

        return back();
    }

    public function massDestroy(MassDestroyCommissionRequest $request)
    {
        Commission::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function returnCommissionFields(Request $request)
    {
        if (!isset($request->number) || !isset($request->agentPlanId)) {
            return '';
        }
        $commissionable_level = $request->number ?? AgentPlan::find($request->agentPlanId)->commissionable_level;
        $commissionsIds = Commission::where([
            'agent_plan_id' => $request->agentPlanId,
            'type' => $request->type ?? 'package',
        ]);
        if (isset($request->tuitionPackageEfk)) {
            $commissionsIds = $commissionsIds->whereHas('packagesCommissions', function ($q) use ($request) {
                $q->where('tuition_package_efk', $request->tuitionPackageEfk);
            })->pluck('id')->toArray();
        } else {
            $commissionsIds = $commissionsIds->pluck('id')->toArray();
        }
        $html = '';

        $upline = $request->upline;
        for ($i = 0; $i <= $commissionable_level; $i++) {
            $commissionValue = '';
            $level = $i;
            if (!empty($commissionsIds)) {
                $commission = Commission::whereIn('id', $commissionsIds)->where('level', $i)?->first();
                $commissionValue = $commission?->commission;
            };
            $html .= view(
                'components.commissions.fieldsCommission',
                compact('commissionValue', 'level', 'commissionsIds', 'upline')
            )->render();
        }
        $html .= '<div class="form-group"><label>';
        $commissionable_level++;
        if ($upline) {
            $html .= sprintf(trans('cruds.commission.fields.commission_up_x_level'), $commissionable_level . '++');
        } else {
            $html .= sprintf(trans('cruds.commission.fields.commission_down_x_level'), $commissionable_level . '++');
        }
        $html .= '</label><input class="form-control" type="text" value="0" disabled></div>';
        return response()->json(['data' => $html]);
    }
}
