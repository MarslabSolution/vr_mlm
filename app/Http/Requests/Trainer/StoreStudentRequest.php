<?php

namespace App\Http\Requests\Trainer;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('student_create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'tuition_package_efk' => [
                'required',
            ],
            'referral_id' => [
                'required',
            ],
            'student_name' => [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    if (trim($value) === '') {
                        $fail(trans('cruds.studentDetail.fields.required'));
                    }
                },
            ],
            'chinese_name' => [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    if (trim($value) === '') {
                        $fail(trans('cruds.studentDetail.fields.required'));
                    }
                },
            ],
            'student_email' => [
                'required',
                'string',
                'regex:/^\w([\.-]?\w)+@\w+([\.]?\w)+(\.\w{2,3})$/i',
            ],
            'nric_no' => [
                'nullable',
                'string',
                'regex:/^([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|3[01])-([0-9]{2})-([0-9]{4})$/i',
            ],
            'age' => [
                'nullable',
                'integer',
            ],
            'school_name' => [
                'nullable',
                'string',
            ],
            'class_name' => [
                'nullable',
                'string',
            ],
            'gender' => [
                'nullable',
                'string',
            ],
            'guardians.*' => [
                'integer',
            ],
            'guardians' => [
                'array',
            ],
        ];
    }
}
