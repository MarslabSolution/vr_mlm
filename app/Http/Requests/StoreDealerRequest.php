<?php

namespace App\Http\Requests;

use Gate;
use Illuminate\Foundation\Http\FormRequest;

class StoreDealerRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('dealer_create');
    }

    public function rules()
    {
        return [
            'upline' => [
                'required',
            ],
            'name' => [
                'string',
                'required',
            ],
            'email' => [
                'required',
                // 'unique:sso_db.users',
            ],
            'password' => [
                'required',
                'min:8',
            ],
        ];
    }
}
