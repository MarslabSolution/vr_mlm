<?php

namespace App\Http\Requests;

use Gate;
use Illuminate\Foundation\Http\FormRequest;

class StoreAgentRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('agent_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
            'email' => [
                'required',
                // 'unique:sso_db.users',
            ],
            'password' => [
                'required',
                'min:8',
            ],
            'upline' => [
                'required',
            ],
        ];
    }
}
