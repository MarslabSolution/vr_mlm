<?php

namespace App\Http\Requests;

use App\Models\User;
use Closure;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class GeneralSettingsRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('setting_access');
    }
    public function handle($request, Closure $next)
    {
        $request->merge([
            "payload"=> json_encode($request->input('value', '')),
        ]);
        unset($request['value']);

        return $next($request);
    }
    public function rules()
    {
        return [];
    }
}
