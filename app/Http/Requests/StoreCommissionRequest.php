<?php

namespace App\Http\Requests;

use App\Models\Commission;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCommissionRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('commission_create');
    }

    public function rules()
    {
        return [
            'commissions' => [
                'required',
                'array',
            ],
            'commissions.*' => [
                'regex:/^((\d{1,2}(\.\d{1,4})?)%|(0\.\d{1,2})|(\d+(\.\d{1,2})?))$/i',
            ],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'commission.not_regex' => trans('validation.commission_regex'),
            'commission.regex' => trans('validation.commission_regex'),
        ];
    }
}
