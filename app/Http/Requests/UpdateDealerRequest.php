<?php

namespace App\Http\Requests;

use Gate;
use Illuminate\Foundation\Http\FormRequest;

class UpdateDealerRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('dealer_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
            'email' => [
                'required',
                // 'unique:sso_db.users,email,' . request()->route('user')->id,
            ],
        ];
    }
}
