<?php

namespace App\Http\Requests;

use Gate;
use Illuminate\Foundation\Http\FormRequest;

class UpdateAgentRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('agent_edit');
    }

    public function rules()
    {
        return [
            'downline' => [
                'required',
                'string',
            ],
            'upgrade_plan' => [
                'required',
                'string',
            ],
        ];
    }
}
