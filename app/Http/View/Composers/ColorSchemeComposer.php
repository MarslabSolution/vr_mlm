<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;

class ColorSchemeComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $color_scheme = session()->has('color_scheme') ? session('color_scheme') : "default";

        $view->with(compact('color_scheme'));
    }
}
