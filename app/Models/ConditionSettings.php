<?php

namespace App\Models;


use Spatie\LaravelSettings\Settings;

class ConditionSettings extends Settings
{
    public int $dealer_commission_level;
    public int $franchiser_commission_level;
    public int $maximum_franchiser_group = 155;
    public int $maximum_franchiser_downline = 2;
    public int $trainner_student_kpi;
    
    public static function group(): string
    {
        return 'condition';
    }

}