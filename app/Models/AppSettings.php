<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppSettings extends Model
{
    protected $table = 'settings';

    protected $guarded = [];

    protected $casts = [
        'locked' => 'boolean',
    ];

    public static function get(string $name, string $group)
    {
        $setting = self::query()
            ->where('group', $group)
            ->where('name', $name)
            ->first('payload');
            
        if(isset($setting)) {
            return json_decode($setting->getAttribute('payload') ?? '');
        }
        return null;
    }
}
