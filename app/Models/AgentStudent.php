<?php

namespace App\Models;

use App\Http\Controllers\Traits\TuitionPackageTrait;
use \DateTimeInterface;
use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\SsoClient\Entities\User;

class AgentStudent extends Model
{
    use SoftDeletes;
    use Auditable;
    use HasFactory;
    use TuitionPackageTrait;

    public $table = 'agent_students';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'tuition_package_efk',
        'student_id',
        'referral_id',
        'trainer_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function getTuitionPackagesName(int $tuition_id){
        return $this->getTuitionPackage($tuition_id, 'name');
    }

    public function student()
    {
        return $this->belongsTo(User::class, 'student_id');
    }

    public function referral()
    {
        return $this->belongsTo(User::class, 'referral_id');
    }
    
    public function trainer()
    {
        return $this->belongsTo(User::class, 'trainer_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
