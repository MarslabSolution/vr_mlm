<?php

namespace App\Models;


use Spatie\LaravelSettings\Settings;

class GeneralSettings extends Settings
{
    public string $site_name;
    public bool $site_active;
    public string $color_scheme;
     
    public static function group(): string
    {
        return 'general';
    }
}