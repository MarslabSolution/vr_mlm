<?php

namespace App\Models;


use Spatie\LaravelSettings\Settings;

class ApiSettings extends Settings
{
    public string $main_system_domain;
    public string $api_version;
    public string $tuition_packages;
    public string $student_details_massDestroy;
    public string $student_details_index;
    
    public static function group(): string
    {
        return 'api';
    }

}