<?php

/**
 * File name: helper.php
 * Last modified: 2022.05.17 
 * Author: ℬᵒᵒⁿ ℑᵘⁿ (文俊),
 * Company: Marslab Solution Sdn. Bhd.
 *
 */

use App\Models\ApiSettings;
use App\Models\AppSettings;
use App\Models\ConditionSettings;
use App\Models\GeneralSettings;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

if (!function_exists('settings')) {
    function settings($key, $default = null, $group = 'general')
    {
        switch ($group) {
            case ConditionSettings::group():
                return app(ConditionSettings::class)->{$key} ?? $default;
                break;
            case ApiSettings::group():
                return app(ApiSettings::class)->{$key} ?? $default;
                break;
            case GeneralSettings::group():
                return app(GeneralSettings::class)->{$key} ?? $default;
                break;
            default:
                return AppSettings::get($key, $group) ?? $default;
        }
    }
}

if (!function_exists('has_role')) {
    function has_role($role_title)
    {
        $role = \Modules\SsoClient\Entities\Role::where('title', $role_title)->first();
        return auth()->user()->roles->contains($role);
    }
}

if (!function_exists('access_token')) {
    function access_token()
    {
        return Storage::get(md5(auth()->user()->email));
    }
}

if (!function_exists('send_html_email')) {
    function send_html_email(string $to, string $subject, $view_url, array $view_data = [])
    {
        Mail::send($view_url, $view_data, function ($message) use ($to, $subject) {
            $message->to($to);
            $message->subject($subject);
        });
    }
}
