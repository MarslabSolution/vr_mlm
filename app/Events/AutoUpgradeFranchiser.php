<?php

namespace App\Events;

use App\Models\AgentPlan;
use App\Models\MlmLevel;
use Illuminate\Broadcasting\InteractsWithSockets;
/* use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast; */
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AutoUpgradeFranchiser
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($franchiser)
    {
        // check if franchiser already have total 155 downlines.
        if (MlmLevel::where([
            ['path', 'LIKE', '%' . $franchiser->id . '%'],
            ['current_plan_id', '!=', '1'],
        ])->count() > settings('maximum_franchiser_group', config('constants.franchiser.group_franchiser_maximum'), 'condition')) {
            $uplineFranchiser = MlmLevel::find($franchiser->id);

            // current_plan_id == 2 means the user is franchiser.
            // check if the agent_plan_id is exist.
            if ($uplineFranchiser->current_plan_id == 2 && AgentPlan::find($uplineFranchiser->current_plan_id)->exists()) {
                $uplineFranchiser->update([
                    'current_plan_id' => $uplineFranchiser->current_plan_id + 1,
                ]);
            }
        }
    }

    /*
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        // return new PrivateChannel('channel-name');
    }
}
