<?php

namespace App\Events;

use App\Models\CommissionStatement;
use App\Models\CommissionTypeStatement;
use DateTime;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class FranchiserStatement
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($franchiser, $type = '', $commission = 0)
    {
        if ($franchiser == null)
            return;

        $today = new DateTime();
        $commissionStatement = CommissionStatement::whereYear('created_at',  $today->format('Y'))
            ->whereMonth('created_at', $today->format('m'))
            ->firstOrNew(['user_id' => $franchiser->user_id]);

        //$commissionStatement->updated_at = \Carbon\Carbon::parse($today); //no work
        $commissionStatement->month = $today->format('d') > 26 ? $today->modify('+1 months')->format('m') : $today->format('m');
        $commissionStatement->user_id = $franchiser->user_id;
        $commissionStatement->total += $commission;
        $commissionStatement->save();

        $items = CommissionTypeStatement::create([
            'type' => $type,
            'sub_total' => $commission,
        ]);
        $commissionStatement->commission_groups()->attach($items->id);
    }
}
