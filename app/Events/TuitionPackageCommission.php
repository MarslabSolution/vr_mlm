<?php

namespace App\Events;

use App\Models\AgentPlan;
use App\Models\Commission;
use App\Models\CommissionStatement;
use App\Models\CommissionTypeStatement;
use App\Models\MlmLevel;
use DateTime;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TuitionPackageCommission
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($referral_id = '', $price = 0)
    {
        if(empty($referral_id)) return;

        $today = new DateTime();
        try {
            DB::beginTransaction();
            $referral_agent = MlmLevel::where('user_id', $referral_id)->first();
            $max_level_commission = AgentPlan::max('commissionable_level');
            $upLineAgentsIds = explode('/', substr($referral_agent->path ??'', -($max_level_commission * 2))) ?? [];
            $upLineAgentsIds[] = strval($referral_agent->id);
            $upLineAgentsIds = array_reverse($upLineAgentsIds);
            for ($i=0; $i < count($upLineAgentsIds); $i++) { 
                $agent = MlmLevel::find($upLineAgentsIds[$i]);
                $commissionable_level = AgentPlan::find($agent->current_plan_id)->commissionable_level ?? 0;
                $commissionableValue = 0;
                if($i <= $commissionable_level) {
                    $commissionableValue = Commission::where([
                        'agent_plan_id' => $agent->current_plan_id,
                        'type' => 'package',
                        'level'=>$i
                    ])->value('commission') ?? 0;
                    if (ctype_digit(strval($commissionableValue))) {
                        $commission = $commissionableValue;
                    } else if (str_contains($commissionableValue, '%')) {
                        $commission = $price * (floatval(str_replace('%', '', $commissionableValue)) / 100);
                    } else {
                        $commission = $price * $commissionableValue;
                    }
                    if($commission > 0) {
                        $commissionStatement = CommissionStatement::whereYear('created_at',  $today->format('Y'))
                        ->whereMonth('created_at', $today->format('m'))
                        ->firstOrNew(['user_id' => $agent->user_id]);
                        $commissionStatement->month = $today->format('d') > 26 ? $today->modify('+1 months')->format('m') : $today->format('m');
                        $commissionStatement->user_id = $agent->user_id;
                        $commissionStatement->total += $commission;
                        $commissionStatement->save();
                
                        $items = CommissionTypeStatement::create([
                            'type' => 'tuition_package',
                            'sub_total' => $commission,
                        ]);
                        $commissionStatement->commission_groups()->attach($items->id);
                    }
                }
            }
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    /* public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    } */
}
